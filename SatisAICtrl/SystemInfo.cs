using System;
using System.Diagnostics;
using System.IO;
using Hardware.Info;
using Microsoft.Win32;

namespace SatisAICtrl
{
    public class SystemInfo
    {
        IHardwareInfo HwInfo { get; set; }

        /// <summary>
        /// Convert bytes to Mb
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public string ToMb(ulong number)
        {
            ulong mb = number / (1024 * 1024);
            return mb.ToString() + " MB ";
        }

        public SystemInfo()
        {
            HwInfo = new HardwareInfo();
            //HwInfo.RefreshAll();
        }

        public string GetInfo()
        {
            string text = "";
            text += "==========SYSTEM INFO========\n";
            text += GetCPUInfo() + "\n";
            text += GetMemoryInfo() + "\n";
            text += GetMainboardInfo() + "\n";
            text += GetDriveInfo() + "\n";
            text += GetGPUInfo() + "\n";
            text += GetDotNetInfo() + "\n";
            text +=
                "Working Directory: " + Directory.GetCurrentDirectory() + "\n";
            text += "=============================\n\n";
            return text;
        }

        public string GetCPUInfo(bool includePercentProcessorTime = false)
        {
            HwInfo.RefreshCPUList (includePercentProcessorTime);
            string text = "";
            foreach (var cpu in HwInfo.CpuList)
            {
                text += (cpu.Name + "\t");
                foreach (var cpuCore in cpu.CpuCoreList)
                text += (cpuCore.ToString() + "\t");
            }
            return text;
        }

        public string GetMemoryInfo()
        {
            string text = "";
            HwInfo.RefreshMemoryList();
            HwInfo.RefreshMemoryStatus();
            var status = HwInfo.MemoryStatus;
            foreach (var hardware in HwInfo.MemoryList)
            text +=
                hardware.Manufacturer + " - " + ToMb(hardware.Capacity) + "\t";
            text += "Free: " + ToMb(status.AvailablePhysical);
            return text;
        }

        public string GetMainboardInfo()
        {
            HwInfo.RefreshMotherboardList();
            string text = "";
            foreach (var hardware in HwInfo.MotherboardList)
            text += hardware.Manufacturer + " " + hardware.Product;
            return text;
        }

        public string GetDriveInfo()
        {
            string text = "";
            HwInfo.RefreshDriveList();
            foreach (var drive in HwInfo.DriveList)
            {
                //text+= drive +"\t";
                foreach (var partition in drive.PartitionList)
                {
                    //text+= partition+"\n";
                    foreach (var volume in partition.VolumeList)
                    text +=
                        volume.Name +
                        " Free: " +
                        ToMb(volume.FreeSpace) +
                        " Hw: " +
                        drive.Model +
                        "\n";
                }
            }
            return text;
        }

        public string GetGPUInfo()
        {
            try
            {
                string command =
                    "--query-gpu=timestamp,name,pci.bus_id,driver_version,pstate,pcie.link.gen.max,pcie.link.gen.current,temperature.gpu,utilization.gpu,utilization.memory,memory.total,memory.free,memory.used --format=csv";

                Process cmd = new Process();
                cmd.StartInfo.FileName = "nvidia-smi";
                cmd.StartInfo.Arguments = command;
                cmd.StartInfo.RedirectStandardInput = true;
                cmd.StartInfo.RedirectStandardOutput = true;
                cmd.StartInfo.CreateNoWindow = true;
                cmd.StartInfo.UseShellExecute = false;
                cmd.Start();

                cmd.StandardInput.Flush();
                cmd.StandardInput.Close();
                cmd.WaitForExit();
                return cmd.StandardOutput.ReadToEnd().Trim();
            }
            catch (Exception ex)
            {
                return ex.Message.Trim();
            }
        }

        // Checking the version using >= will enable forward compatibility,
        // however you should always compile your code on newer versions of
        // the framework to ensure your app works the same.
        private static string CheckFor45DotVersion(int releaseKey)
        {
            if (releaseKey >= 528040)
            {
                return "4.8 or later";
            }
            if (releaseKey >= 461808)
            {
                return "4.7.2 or later";
            }
            if (releaseKey >= 461308)
            {
                return "4.7.1 or later";
            }
            if (releaseKey >= 460798)
            {
                return "4.7 or later";
            }
            if (releaseKey >= 394802)
            {
                return "4.6.2 or later";
            }
            if (releaseKey >= 394254)
            {
                return "4.6.1 or later";
            }
            if (releaseKey >= 393295)
            {
                return "4.6 or later";
            }
            if (releaseKey >= 393273)
            {
                return "4.6 RC or later";
            }
            if ((releaseKey >= 379893))
            {
                return "4.5.2 or later";
            }
            if ((releaseKey >= 378675))
            {
                return "4.5.1 or later";
            }
            if ((releaseKey >= 378389))
            {
                return "4.5 or later";
            }

            // This line should never execute. A non-null release key should mean
            // that 4.5 or later is installed.
            return "No 4.5 or later version detected";
        }

        public string GetDotNetInfo()
        {
            string text = "";
            using (
                RegistryKey ndpKey =
                    RegistryKey
                        .OpenBaseKey(RegistryHive.LocalMachine,
                        RegistryView.Registry32)
                        .OpenSubKey("SOFTWARE\\Microsoft\\NET Framework Setup\\NDP\\v4\\Full\\")
            )
            {
                int releaseKey = Convert.ToInt32(ndpKey.GetValue("Release"));
                if (true)
                {
                    text +=
                        "Dotnet Version: " +
                        CheckFor45DotVersion(releaseKey) +
                        " releaseKey" +
                        releaseKey;
                }
            }
            return text;
        }
    }
}
