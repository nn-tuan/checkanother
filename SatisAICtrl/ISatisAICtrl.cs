﻿using SatisAICtrl.Files;
using SatisAICtrl.Models;


namespace SatisAICtrl
{
    public enum LogType
    {
        NG = 0,
        All,
        None,
    }
    public interface ISatisAICtrl
    {
       
        #region Properties
        public string ResultOutputDirPath { get; set; }
        public int GpuId { get; set; }
        public string ShowMode { get; set; }
        public bool InspectMode { get; set; }
        public LogType _LogType { get; set; }
        public Logger Logger { get; set; } 
        public PcbBoard PcbBoard { get; set; }
        public CloudSynch Syncher { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// 配置情報ファイルの読み込みを実行します。
        /// 1. 上位からFile取得
        /// 2. SequenceEngine側に変更
        /// </summary>
        /// <param name="filePath"></param>
        void LoadSAIPlaceFile(string filePath, bool ignoreLack);

        /// <summary>
        /// 閉じる処理
        /// </summary>
        void CloseApp();
        
        void StartStopCam(bool isStart);
        void StartStopInspect(bool isStart);
        void ToggleNGShowMode();

        /// <summary>
        /// Open Specific Camera
        /// </summary>
        /// <param name="type">camera type: flir/video/webcam/rtsp</param>
        /// <param name="index">camera index </param>
        /// <param name="path">camera path (video, rtsp)</param>
        void SetCamera(string type, int index, string path);
        #region Register PCB functions
        void UpdateCheckBox();
        /// <summary>
        /// Disable Part update while saving
        /// </summary>
        void DisanableUpdate();
        /// <summary>
        /// Enable Part update
        /// </summary>
        void EnableUpdate();
        /// <summary>
        /// Start Register PCB - Pause camera and run detect part + rotation
        /// </summary>
        /// <returns>success</returns>
        bool StartRegisterPcb();
        /// <summary>
        /// Stop Register PCB - Resume camera, clear detections result
        /// </summary>
        /// <returns></returns>
        bool StopRegisterPcb();
        /// <summary>
        /// Register a PCB
        /// </summary>
        /// <param name="OutputPartTxt"> Ouput Dir</param>
        /// <param name="NewProjectName"></param>
        /// <param name="NewBoardName"></param>
        /// <param name="ModelNumber"></param>
        /// <param name="RotationTol"></param>
        /// <param name="DistanceTol"></param>
        /// <returns>Path to Saiplace file</returns>
        string RegisterPcb(string OutputPartTxt, string NewProjectName, string NewBoardName, string ModelNumber, int RotationTol, int DistanceTol);
        /// <summary>
        /// Toggle colorful background rectangle - use when hovering mouse
        /// </summary>
        /// <param name="index"></param>
        void TogglePcbPartBackground(int index);
        /// <summary>
        /// Enable/Disable Part for saving
        /// </summary>
        /// <param name="index"></param>
        void TogglePcbPart(int index);
        void RaiseStartPartRegisterEvent();
        void RaiseStopPartRegisterEvent();
        void RaiseStartPcbRegisterEvent();
        void RaiseStopPcbRegisterEvent();
        void RaiseRegisterPcbRegisterEvent();
        #endregion
        #region Register Part functions
        /// <summary>
        /// Record video - Register PCB Mode
        /// </summary>
        /// <param name="videoPath"></param>
        /// <returns>Record is success</returns>
        bool StartRecord(string videoPath);
        /// <summary>
        /// Stop Record - Register PCB Mode
        /// </summary>
        /// <returns>Stop is success</returns>
        bool StopRecord();
        /// <summary>
        /// Update Parts Checkbox - Register PCB Mode, Checked Parts will be saved later
        /// </summary>
        #endregion

        #endregion

        #region Event Actions
        /// <summary>
        /// ロードされた配置情報ファイルの変更イベント
        /// </summary>
        event Action<PcbBoard>? SAIPlaceFileChangeEvent;

        event Action<string>? SAILogChangeEvent;
        /// <summary>
        /// 画像描画開始イベント
        /// </summary>
        event Action<Uri>? ViewImageOpenEvent;
        /// <summary>
        /// 動画描画開始イベント
        /// </summary>
        event Action<Uri>? ViewMovieOpenEvent;
        /// <summary>
        /// 結果更新イベント
        /// </summary>
        event Action<string>? UpdateInspectionResultEvent;
        event Action<List<PartItem>>? NGPartItemsEvent;
        /// <summary>
        /// Closeイベント
        /// </summary>
        event Action? CloseAppEvent;


        event Action? StartCameraEvent;
        event Action? StopCameraEvent;
        event Action? StartInspectEvent;
        event Action? StopInspectEvent;
        event Action<byte[], int, int, int>? ImageDataUpdateEvent;
        event Action<PcbBoard>? PcbUpdateEvent;



        /// <summary>
        /// Part registration screen
        /// </summary>
        event Action? StartPartRegisterEvent;
        event Action? StopPartRegisterEvent;
        #endregion
    }
}