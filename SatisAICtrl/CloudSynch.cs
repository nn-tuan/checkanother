﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FireSharp.Interfaces;
using FireSharp.Config;
using FireSharp;
using FireSharp.Response;
using Newtonsoft.Json.Linq;

namespace SatisAICtrl
{
    public class Session
    {
        public string? Name;
        public string? Date;
        public string? Time;
        public string? Log;
    }

    public class CloudSynch
    {
        IFirebaseClient Client;
        string? SessionPath;
        public bool Online { get; set; } = false;

        /// <summary>
        /// Create Firebase object for cloud synching
        /// </summary>
        public CloudSynch()
        {
            IFirebaseConfig config = new FirebaseConfig
            {
                AuthSecret = "nDErbcSqFO1eghyPb3iLp5qTrRNOFyvZOHxZO3Tt",
                BasePath = "https://jit-pcb-default-rtdb.asia-southeast1.firebasedatabase.app"
            };
            Client = new FirebaseClient(config);
            string dateTime = DateTime.Today.ToLongDateString() + " " + DateTime.Now.ToLongTimeString();
            SessionPath = "session/" + dateTime;
            try
            {
                SetResponse response = Client.Set(SessionPath, "");
                Online = true;
            }
            catch (Exception)
            {
                Online = false;
            }
        }

        /// <summary>
        /// Log object to cloud given object path
        /// </summary>
        public async void Log<T>(string path, T obj)
        {
            try
            {
                if (Online && SessionPath != null)
                {
                    string cloudPath = SessionPath + "/" + path;
                    await Client.SetAsync(cloudPath, obj);
                }
            }
            catch (Exception)
            {

            }
        }

        /// <summary>
        /// Log object to cloud in array mode given object path
        /// </summary>
        public async void LogPush<T>(string path, T obj)
        {
            try
            {
                string dateTime = DateTime.Now.ToLongTimeString();
                if (Online && SessionPath != null)
                {
                    string cloudPath = SessionPath + "/" + path+"/"+dateTime;
                    await Client.SetAsync(cloudPath, obj);
                }
            }
            catch (Exception)
            {

            }
        }
    }
}
