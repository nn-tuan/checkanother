using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SatisAICtrl.Files
{
    public class SAIPlace
    {
        private string _PCBName = "";
        /// <summary>
        /// PCB名
        /// </summary>
        public string PCBName
        {
            get { return _PCBName; }
            set
            {
                if (_PCBName == value) return;
                _PCBName = value;
            }
        }

        private ObservableCollection<PartItem> _partsList = new ObservableCollection<PartItem>();
        public ObservableCollection<PartItem> PartsList
        {
            get { return _partsList; }
            set
            {
                _partsList = value;
            }
        }

        public class PartItem
        {
            public string? ID { get; set; }
            public string? Name { get; set; }
        }
    }
}
