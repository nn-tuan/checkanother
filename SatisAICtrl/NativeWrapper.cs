﻿using System.Runtime.InteropServices;
using System.Drawing;
using System.Diagnostics;
using SatisAICtrl.Models;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;


namespace SatisAICtrl
{
    
    public class NativeWrapper
    {
        #region Native Functions


        [DllImport(@"SatisAICore.dll")]
        static private extern int GetAppCode();
        [DllImport(@"SatisAICore.dll")]
        static private extern void FreeBuffer(IntPtr myBuffer);
        [DllImport(@"SatisAICore.dll")]
        static private extern IntPtr CreateClass();
        [DllImport(@"SatisAICore.dll")]
        static private extern void RemoveClass(IntPtr pObject);
        [DllImport(@"SatisAICore.dll")]
        static private extern bool CheckGPU();
        [DllImport(@"SatisAICore.dll")]
        static private extern void ResizeImage(IntPtr image, int w, int h);
        [DllImport(@"SatisAICore.dll")]
        static private extern void DrawLine(IntPtr image, int x0, int y0, int x1, int y1, string color, int lineWidth);
        [DllImport(@"SatisAICore.dll")]
        static private extern void DrawRectangle(IntPtr image, int x0, int y0, int x1, int y1, string color, int lineWidth);
        [DllImport(@"SatisAICore.dll")]
        static private extern void DrawArrow(IntPtr image, int x, int y, int angle, int len, string color, int thickness);
        [DllImport(@"SatisAICore.dll")]
        static private extern void PutText(IntPtr image, string label, int x, int y, string color, float fontScale, int thickness);
        [DllImport(@"SatisAICore.dll")]
        static private extern IntPtr MatToBytes(IntPtr image);
        [DllImport(@"SatisAICore.dll")]
        static private extern bool SetCam(IntPtr pObject, string cameraType, int index, string cameraPath);
        [DllImport(@"SatisAICore.dll")]
        static private extern void SetLabels(IntPtr pObject, string labelsStr);
        [DllImport(@"SatisAICore.dll")]
        static private extern void LoadPartsModel(IntPtr pObject, string path, int device_id);
        [DllImport(@"SatisAICore.dll")]
        static private extern void LoadAngleModel(IntPtr pObject, string path, string partName, int device_id);
        [DllImport(@"SatisAICore.dll", CallingConvention = CallingConvention.Cdecl)]
        static private extern IntPtr BenchmarkPartsModel(IntPtr pObject);
        [DllImport(@"SatisAICore.dll", CallingConvention = CallingConvention.Cdecl)]
        static private extern IntPtr BenchmarkAngleModel(IntPtr pObject);
        [DllImport(@"SatisAICore.dll", CallingConvention = CallingConvention.Cdecl)]
        static private extern IntPtr Detect(IntPtr pObject, int frameId, float threshold, float nms);
        [DllImport(@"SatisAICore.dll")]
        static private extern bool LoadTemplate(IntPtr pObject, string path);
        [DllImport(@"SatisAICore.dll")]
        static private extern IntPtr StitchBox(IntPtr pObject, int frameId);
        [DllImport(@"SatisAICore.dll")]
        static private extern IntPtr MapPointFromStitch(IntPtr pObject, int x, int y);
        [DllImport(@"SatisAICore.dll")]
        
        static private extern int DetectRotation(IntPtr pObject, int frameId, int boxId, float conf);
        [DllImport(@"SatisAICore.dll")]
        static private extern void FreeImageDataBuffer(IntPtr pObject);
        [DllImport(@"SatisAICore.dll")]
        static private extern int UpdateFrame(IntPtr pObject);
        [DllImport(@"SatisAICore.dll")]
        static private extern IntPtr GetFrame(IntPtr pObject, int frameId);
        [DllImport(@"SatisAICore.dll")]
        static private extern int GetWidth(IntPtr image);
        [DllImport(@"SatisAICore.dll")]
        static private extern int GetHeight(IntPtr image);
        [DllImport(@"SatisAICore.dll")]
        static private extern bool SaveFrame(IntPtr image, string filePath);
        [DllImport(@"SatisAICore.dll")]
        static private extern bool SavePart(IntPtr image, int x, int y, int w, int h, string filePath);
        [DllImport(@"SatisAICore.dll")]
        static private extern bool StartRecord(IntPtr pObject, string filePath);
        [DllImport(@"SatisAICore.dll")]
        static private extern bool StopRecord(IntPtr pObject);

        #endregion

        #region Const variable
        int APP_CODE = 1;
        // Detector
        float DETECT_CONF = 0.4f;
        float NMS_CONF = 0.1f;
        float ROTATION_CONF = 0.01f;
        float NUM_FRAMES = 1; // NUM FRAMES TO CALCULATE MEDIAN
        int DELAY_TIME = 5000;
        int AVERAGING_TIME = 0; // TIME FOR AVERAGING THE RESULTS, 0: disable
        //Drawing
        int THICKNESS = 6;
        string COLOR_PASS = "#00FF00";
        string COLOR_NG = "#FF0000";
        string COLOR_PENDING = "#AAFFAA";


        #endregion

        private readonly object _InspectFrameLock = new object();

        #region Properties
        SatisAICtrl SatisAICtrl;
        private IntPtr _DetectorWraper, _InspectFrameOriginal, _InspectFrame, _InspectFrameDraw;
        public event Action<byte[], int, int, int>? ImageDataUpdateEvent;
        public event Action<List<BoundingBox>, int>? InspectEvent;
        List<string> Labels = new List<string>();
        bool PcbInPlace = false; //PCB is in center -> Inspect
        public bool InspectMode { get; set; } = false;
        bool _Inspecting = false;
        DateTime _InspectTime = DateTime.Now;
        public int DeviceId { get; set; } = -1;
        public bool Capturing { get; set; } = false;
        private bool _TemplateLoaded = false;
        private PcbBoard _Board = new PcbBoard();
        private List<string> _InspectLabels = new List<string>();
        public PcbBoard Board { get {
                return _Board;
            
            } set {
                _Board = value;
                _InspectLabels.Clear();
                foreach(var item in _Board.Parts)
                {
                    string label = item.Name!;
                    if (!_InspectLabels.Contains(label)) _InspectLabels.Add(label);
                }

            } }

        int _CaptureFrameId { get; set; } = 0;
        int _DetectFrameId { get; set; } = 0;
        int _LastDisplayFrameId { get; set; } = -1;
        float _CaptureFPS { get; set; } = 0;
        float _DetectFPS { get; set; } = 0;
        public int _Width = 0, _Height = 0; //Frame width, height
        int NumFramesDelay { get; set; } = 0;
        bool Loaded { get;set; } = false;
        FrameDataQueue FrameQueue = new FrameDataQueue();
        /// <summary>
        /// Use for calculating fps -> log file
        /// </summary>
        DateTime _LastDateTimeCapture { get; set; } = DateTime.Now;
        DateTime _LastDateTimeDetect { get; set; } = DateTime.Now;

        #endregion


        #region Utils
        /// <summary>
        /// Check whether or not GPU is installed properly (Nvidia driver + CUDA are installed)
        /// </summary>
        /// <returns></returns>
        public bool CheckGPUAvailable()
        {
            return CheckGPU();
        }
        #endregion
        /// <summary>
        /// Check SatisAICore.dll version, if fail show error
        /// </summary>
        private void CheckNativeVersion()
        {
            if (GetAppCode() != APP_CODE)
            {
                SatisAICtrl.Logger.Log("ERROR: Wrong native version");
                SatisAICtrl.RaiseSAILogChangeEvent("ERROR: Wrong native version");
            }
        }
        #region Init

        /// <summary>
        /// Initialize native class
        /// </summary>
        /// <param name="inspectMode">inspection mode</param>
        /// <param name="satisAICtrl"></param>
        public NativeWrapper(bool inspectMode, SatisAICtrl satisAICtrl)
        {
           
            SatisAICtrl = satisAICtrl;
            Board = new PcbBoard();
            InspectMode = inspectMode;
            CheckNativeVersion();
        }

        /// <summary>
        /// Initialize native class
        /// </summary>
        /// <param name="inspectMode">inspection mode</param>
        /// <param name="satisAICtrl"></param>
        /// <param name="partPaths">deep learning model paths for rotation detection</param>
        /// <param name="device_id">cpu/gpu id: cpu:-1, gpu:0,1,2,3,..</param>
        public NativeWrapper(bool inspectMode, SatisAICtrl satisAICtrl, string partPaths, int deviceId)
        {
            InspectMode = inspectMode;
            SatisAICtrl = satisAICtrl;
            Board = new PcbBoard();
            DeviceId = deviceId;
            _DetectorWraper = CreateClass();

            string labelsConcat = "pcb_anchor capacitor header connector1 connector2 ceramic_capacitor inductor"; 
            Labels = labelsConcat.Split(' ').ToList<string>();
            SetLabels(_DetectorWraper, labelsConcat);
            SatisAICtrl.Logger.Log("NativeWrapper Loaded");
            LoadPartsModelInThread(partPaths, DeviceId);

            InspectMode = inspectMode;
            CheckNativeVersion();
        }

        /// <summary>
        /// Load Part Model In Thread - Reduce Startup time
        /// </summary>
        public void LoadPartsModelInThread(string partPaths, int deviceId)
        {
            Thread th = new Thread(() =>
            {
                SatisAICtrl.Logger.Log("Load detection model: " + partPaths + " device ID: " + DeviceId);
                DateTime t1 = DateTime.Now;
                LoadPartsModel(_DetectorWraper, partPaths, deviceId);
                DateTime t2 = DateTime.Now;
                SatisAICtrl.Logger.Log("Loaded, time to load: " + (t2 - t1).TotalMilliseconds + " ms");
                string benchmarkLog = Utils.ConvertPtrToString(BenchmarkPartsModel(_DetectorWraper))!;
                SatisAICtrl.Logger.Log("Benchmark model: " + benchmarkLog);
                Loaded = true;
            });
            th.IsBackground = true;
            th.Start();
        }

        /// <summary>
        /// Preload part rotation detector models
        /// </summary>
        /// <param name="path">Looking directory</param>
        /// <param name="partName">part name</param>
        public void LoadRotationModel(string path, string partName, bool benchmark)
        {
            SatisAICtrl.Logger.Log("Load rotation model: " + path + " " + partName);
            DateTime t1 = DateTime.Now;
            LoadAngleModel(_DetectorWraper, path, partName, DeviceId);
            DateTime t2 = DateTime.Now;
            SatisAICtrl.Logger.Log("Loaded, time to load: " + (t2-t1).TotalMilliseconds + " ms");
            if (benchmark)
            {
                string benchmarkLog = Utils.ConvertPtrToString(BenchmarkAngleModel(_DetectorWraper))!;
                SatisAICtrl.Logger.Log("Benchmark model: " + benchmarkLog);
            }
        }
        /// <summary>
        /// Open Webcam using OpenCV
        /// </summary>
        /// <param name="index">camera index</param>
        /// <returns>opened</returns>
        /// 
        public bool SetupCam(string cameraType, int index, string cameraPath)
        {
            bool opened = SetCam(_DetectorWraper,cameraType, index, cameraPath);
            if (opened)
                StartCaptureAndDetect();
            return opened;
        }

        /// <summary>
        /// Load template image associate with saiplace for inspecting
        /// </summary>
        /// <param name="path"></param>
        /// <returns>success</returns>
        public bool LoadTemplateImage(string path)
        {
            _TemplateLoaded = LoadTemplate(_DetectorWraper, path);
            return _TemplateLoaded;
        }

        #endregion

        #region Register PCB
        public bool StartRecordVideo(string path)
        {
            return StartRecord(_DetectorWraper, path);
        }
        public bool StopRecordVideo()
        {
            return StopRecord(_DetectorWraper);
        }
        #endregion

        #region Inspect

        /// <summary>
        /// Detect both part location and rotaton
        /// </summary>
        /// <returns></returns>
        public List<BoundingBox> DetectAndEstimateArrow()
        {
            //IntPtr BoundingBoxesPtr = DetectBoth(DetectorWraper, 0.4f, 0.1f, 0.01f);

            //List<float> values = ConvertPtrToFloatVector(BoundingBoxesPtr);
            //List<BoundingBox> detections = ConvertFloatVectorToBoundingBox(values);
            //return detections;

            int frameId = _CaptureFrameId - 1;
            IntPtr BoundingBoxesPtr = Detect(_DetectorWraper, frameId, DETECT_CONF, NMS_CONF);
            string? response = Utils.ConvertPtrToString(BoundingBoxesPtr);
           
            List<BoundingBox> detections = Utils.ConvertJsonToBoxes(response!);
            for (int i = 0; i < detections.Count; i++)
            {
                BoundingBox detection = detections[i];
                    if (detection.Left > 50 && (detection.Left + detection.Width + 50) < _Width)
                    {
                        try
                        {
                            int rotation = DetectRotation(_DetectorWraper, frameId, i, ROTATION_CONF);
                            detections[i].Rotation = rotation;
                        }
                        catch (Exception ex)
                        {

                        }
                    }
             }
            return detections;
        }

        /// <summary>
        /// Check PCB is in center of screen or not - to Inspect pcb  only one time when it is in the center of image
        /// If average distance between detected and template is < 100 pixel then trigger event
        /// </summary>
        /// <param name="detections"></param>
        public void CheckPCB(int frameId, List<BoundingBox> detections)
        {
            float totalDist = 0;
            float count = 0;
            foreach (PartItem part in Board.Parts)
            {
                float minDist = 999999;
                int partIdx = -1;
                foreach (BoundingBox box in detections)
                {
                    if (box.Label == part.ID)
                    {
                        float dist = Utils.CalcPointDistance(box.GetCenter(), new PointF(part.X, part.Y));
                        if (dist < minDist)
                        {
                            minDist = dist;
                            partIdx = detections.IndexOf(box);
                        }

                    }
                }
                if (partIdx != -1)
                {
                    totalDist += minDist;
                    count++;
                }
            }
            float avgDist = totalDist / count;
            bool inPlace = false;
            if (avgDist < 100)
            {
                inPlace = true;
            }

            if (!PcbInPlace && inPlace)
            {
                _Inspecting = true;
                _InspectTime = DateTime.Now;
                lock (_InspectFrameLock)
                {
                    _InspectFrameOriginal = GetFrame(_DetectorWraper, frameId);
                    _InspectFrame = GetFrame(_DetectorWraper, frameId);
                    _InspectFrameDraw = GetFrame(_DetectorWraper, frameId);
                    List<BoundingBox> detectionsFinal = DetectMultiFrames(frameId, detections);
                    List<BoundingBox> partResults = Inspect(detectionsFinal);

                    string inspectMode = SatisAICtrl.ShowMode;
                    if (inspectMode == "NONE")
                    {
                        inspectMode = "NG";
                    }
                    DrawFrame(_InspectFrame, partResults, inspectMode);
                    DrawFrame(_InspectFrameDraw, partResults, SatisAICtrl.ShowMode);
                    InspectEvent?.Invoke(partResults, frameId);
                }
            }

            PcbInPlace = inPlace;
        }

        public void SaveFrame(string imagePath)
        {
            IntPtr framePtr = GetFrame(_DetectorWraper, _CaptureFrameId-1);
            SaveFrame(framePtr, imagePath);
            FreeBuffer(framePtr);
        }

        private List<BoundingBox> Inspect(List<BoundingBox> detections)
        {
            List<BoundingBox> results = new List<BoundingBox>();
            if (detections != null)
            {
                bool NG = false;

                foreach (PartItem item in _Board.Parts)
                {
                    BoundingBox outBox = new BoundingBox();
                    float minDistance = 99999999;
                    int minIdx = -1;
                    for (int i = 0; i < detections.Count; i++)
                    {
                        BoundingBox box = detections[i];
                        PointF positionTemplate = new PointF(item.X, item.Y);
                        PointF positionDetect = new PointF(box.XTemplate, box.YTemplate);
                        float distanceOk = item.Torelation * Math.Max(box.Width, box.Height) / 100;
                        if (box.Label == item.ID)
                        {
                            float distance = Utils.CalcPointDistance(positionTemplate, positionDetect);
                            if (distance < minDistance && distance <= distanceOk)
                            {
                                minDistance = distance;
                                minIdx = i;
                            }
                            else
                            {
                                //Wrong pos
                            }
                        }
                    }
                    if (minIdx != -1)
                    {
                        detections[minIdx].Pass = true;
                        detections[minIdx].WrongPlace = false;
                        BoundingBox box = detections[minIdx];
                        if (box.Rotation < 0)
                        {
                            detections[minIdx].MissedRotation = true;
                            detections[minIdx].Pass = false;
                        }
                        else
                        {
                            int diffAngle = Math.Abs(box.Rotation - item.Rotation);
                            if (diffAngle > item.RotationTorelation)
                            {
                                detections[minIdx].WrongRotation = true;
                                detections[minIdx].Pass = false;
                            }
                            else
                            {
                                detections[minIdx].WrongRotation = false;
                            }
                        }
                        outBox = detections[minIdx];
                    }
                    else
                    {
                        IntPtr ptr = MapPointFromStitch(_DetectorWraper, item.X, item.Y);
                        List<float> vec = Utils.ConvertPtrToFloatVector(ptr);
                        int x = (int)vec[0] - 50;
                        int y = (int)vec[1] - 50;
                        outBox.Label = item.Name;
                        outBox.Pass = false;
                        outBox.Top = y;
                        outBox.Left = x;
                        outBox.Width = 100;
                        outBox.Height = 100;
                        outBox.WrongPlace = true;
                    }
                    outBox.Inspected = true;
                    results.Add(outBox);
                }
            }
            return results;
        }


        /// <summary>
        /// Inspect PCB using temporal information
        /// </summary>
        /// <param name="saveVisPath">save path for pcb image</param>
        /// <param name="savePartsDir">save dir for NG parts</param>
        /// <param name="dateTimeStr"></param>
        /// <param name="numFrames">number of frames for averaging results</param>
        /// <param name="drawOkPart">draw OK part or not</param>
        /// <returns></returns>
        List<BoundingBox> DetectMultiFrames(int frameId, List<BoundingBox> _detections )
        { 
            // Stitch current frame
            IntPtr BoundingBoxesPtr = StitchBox(_DetectorWraper, frameId);
            string response = Utils.ConvertPtrToString(BoundingBoxesPtr)!;
            List<BoundingBox> detections = Utils.ConvertJsonToBoxes(response);

            List<List<BoundingBox>> frames = new List<List<BoundingBox>>();
            // Get results for multile frames
            for (int i = 0; i < NUM_FRAMES; i++)
            {
                int currFrameId = frameId - i - 1;
                BoundingBoxesPtr = Detect(_DetectorWraper, currFrameId, DETECT_CONF, NMS_CONF);
                List<BoundingBox> currDetections = Utils.ConvertJsonToBoxes(Utils.ConvertPtrToString(BoundingBoxesPtr)!);
                for (int j = 0; j < currDetections.Count; j++)
                {
                    if (_InspectLabels.Contains(currDetections[i].Label!))
                    {
                        int rotation = DetectRotation(_DetectorWraper, currFrameId, j, ROTATION_CONF);
                        currDetections[j].Rotation = rotation;
                    }
                }
                frames.Add(currDetections);
            }

            // Find median angle
            for (int i=0;i< detections.Count; i++)
            {
                if (_InspectLabels.Contains(detections[i].Label!))
                {
                    BoundingBox box = detections[i];
                    detections[i].Rotation = _detections[i].Rotation;
                    List<int> rotations = new List<int>();
                    int rotation = box.Rotation;
                    if (rotation > 0)
                    {
                        rotations.Add(box.Rotation);
                    }

                    for (int j = 0; j < NUM_FRAMES; j++) {
                        List<BoundingBox> currDetections = frames[j];
                        float minDistance = 999999f;
                        int minIdx = -1;
                        for (int k = 0; k < currDetections.Count; k++)
                        {
                            if (currDetections[k].Label == box.Label)
                            {
                                float dist = Utils.CalcPointDistance(box.GetCenter(), currDetections[k].GetCenter());
                                if (dist < minDistance)
                                {
                                    minDistance = dist;
                                    minIdx = k;
                                }
                            }
                        }
                        if (minDistance < Math.Max(box.Width, box.Height))
                        {
                            int currRotation = currDetections[minIdx].Rotation;
                            if (currRotation > 0)
                                rotations.Add(currRotation);
                        }
                     }

                    rotations.Sort();
                    
                    if (rotations.Count > 1)
                    {
                        int medianIndex = (rotations.Count / 2);
                        if (rotations.Count%2 == 1) medianIndex += 1;
                        rotation = rotations[medianIndex];
                    }
                    detections[i].Rotation = rotation;
                }
            }

            return detections;
        }


        #endregion

        #region Drawing

        public void SaveInspectImages(List<BoundingBox> detections, int frameId, string resultOutputDirPath, string dateTimeStr, string imagePath)
        {
            //StartDraw(DetectorWraper, frameId);
            //DrawFrame(frameId, detections);
            string ngPartDir = resultOutputDirPath + "\\NGParts";
            if (!Directory.Exists(ngPartDir)) Directory.CreateDirectory(ngPartDir);

            SaveFrame(_InspectFrame, imagePath);
            for (int i=0; i<detections.Count;i++)
            {
                 BoundingBox box = detections[i];
                if (!box.Pass)
                {
                    string path = ngPartDir + "\\" + dateTimeStr + "_" + box.Label + "_" + i.ToString() + ".jpg";
                    SavePart(_InspectFrameOriginal, box.Left, box.Top, box.Width, box.Height, path);
                }
            }
        }

        void DrawFrame(IntPtr image, List<BoundingBox> boxes, string mode)
        {
            foreach (BoundingBox box in boxes)
            {
                if (_InspectLabels.Contains(box.Label!))
                {
                    bool draw = false;
                    if (mode == "ALL") draw = true;
                    if (mode == "NG" && !box.Pass) draw = true;
                    if (draw)
                    {
                        string colorBox = COLOR_PENDING;
                        string colorArrow = COLOR_PENDING;
                        if (box.Inspected)
                        {
                            if (box.WrongPlace) colorBox = COLOR_NG;
                            else colorBox = COLOR_PASS;

                            if (box.WrongRotation) colorArrow = COLOR_NG;
                            else colorArrow = COLOR_PASS;
                        }

                        Point center = box.GetCenter();
                        DrawRectangle(image, box.Left, box.Top, box.Left + box.Width, box.Top + box.Height, colorBox, THICKNESS);
                        if (box.Rotation > 0)
                        {
                            if (box.MissedRotation)
                            {
                                DrawLine(image, box.Left, box.Top, box.Left + box.Width, box.Top + box.Height, COLOR_NG, THICKNESS);
                                DrawLine(image, box.Left + box.Width, box.Top, box.Left, box.Top + box.Height, COLOR_NG, THICKNESS);
                            }
                            else
                            {
                                DrawArrow(image, center.X, center.Y, box.Rotation, Math.Max(box.Width, box.Height) * 2 / 3, colorArrow, THICKNESS);
                            }
                        }
                    }
                }
            }
        }
        void DrawFrame(IntPtr image)
        {
            PutText(image, "FPS Capture: " + _CaptureFPS+ " Detect: " + _DetectFPS, 10, 50, COLOR_PASS, 1.5f, THICKNESS);
            FrameData data = FrameQueue.GetAverage(AVERAGING_TIME);
            if (SatisAICtrl.ShowMode == "ALL")
            {
                DrawFrame(image, data.BoundingBoxes, SatisAICtrl.ShowMode);
                
            }
        }
        #endregion

        #region Main loop
        /// <summary>
        /// Start capture and detect thread seperately
        /// </summary>
        public void StartCaptureAndDetect()
        {
            Capturing = true;
            Thread th = new Thread(() => CaptureThread());
            th.IsBackground = true;
            th.Start();

            Thread th2 = new Thread(() => DetectThread());
            th2.IsBackground = true;
            th2.Start();
        }
        /// <summary>
        /// Capture thread
        /// </summary>
        public void CaptureThread()
        {
            
            int MAX_WIDTH = 1000;
            while (true)
            {

                if (Capturing)
                {
                    DateTime now = DateTime.Now;
                    bool freeze = false;
                    IntPtr framePtr;
                    int w, h, c = 0;

                    // Start Inspecting
                    if (_Inspecting)
                    {
                        float diff = (float)(now - _InspectTime).TotalMilliseconds;
                        if (diff < DELAY_TIME) freeze = true;
                        else
                        {
                            if (_InspectFrame != IntPtr.Zero)
                            {
                                lock (_InspectFrameLock)
                                {
                                    FreeBuffer(_InspectFrame);
                                    _InspectFrame= IntPtr.Zero;
                                    FreeBuffer(_InspectFrameDraw);
                                    _InspectFrameDraw = IntPtr.Zero;
                                    FreeBuffer(_InspectFrameOriginal);
                                    _InspectFrameOriginal = IntPtr.Zero;
                                }
                            }
                            _Inspecting = false;
                            SatisAICtrl.RaiseUpdateInspectionResultEvent("Wait");
                        }
                    }
                    // Freeze for DELAY_TIME seconds while  inspecting for visualize
                    if (freeze && _InspectFrameDraw != IntPtr.Zero)
                    {
                        IntPtr imagePtr;
                        lock (_InspectFrameLock)
                        {
                            imagePtr = MatToBytes(_InspectFrameDraw);
                        }
                        byte[]? data = Utils.ConvertPtrToImageData(imagePtr, out w, out h, out c);
                        FreeBuffer(imagePtr);
                        if (data != null)
                            ImageDataUpdateEvent?.Invoke(data, w, h, c);
                    }
                    else
                    {
                        _CaptureFrameId = UpdateFrame(_DetectorWraper);
                        // shift capture thread to match bounding boxes
                        int displayFrameId = _CaptureFrameId - NumFramesDelay;
                        if (displayFrameId == _LastDisplayFrameId) displayFrameId++; //avoid draw two times on single frame
                        framePtr = GetFrame(_DetectorWraper, displayFrameId);
                        if (framePtr != IntPtr.Zero)
                        {
                            _Width = GetWidth(framePtr);
                            _Height = GetHeight(framePtr);
                            //FreeBuffer(framePtr);
                            if (FrameQueue.FrameDatas.Count > 0)
                            {
                                DrawFrame(framePtr);
                            }
                            if (_Width > MAX_WIDTH)
                            {
                                float scale = _Width / (float)MAX_WIDTH;
                                w = MAX_WIDTH;
                                h = (int)(_Height / scale);
                                ResizeImage(framePtr, w, h);
                                //Utils.ConvertPtrToImageData(framePtr, out _Width, out _Height, out c);
                            }
                            IntPtr imagePtr = MatToBytes(framePtr);
                            FreeImageDataBuffer(framePtr);
                            byte[]? data = Utils.ConvertPtrToImageData(imagePtr, out w, out h, out c);
                            FreeBuffer(imagePtr);
                            if (data != null)
                            {
                                ImageDataUpdateEvent?.Invoke(data, w, h, c);
                            }
                        }
                        if (_CaptureFrameId % 10 == 0 && _CaptureFrameId != 0)
                        {
                            float diff = (float)(DateTime.Now - _LastDateTimeCapture).TotalMilliseconds;
                            _CaptureFPS = 10f / (diff / 1000f);
                            _CaptureFPS = ((int)(_CaptureFPS * 10)) / 10f;
                            SatisAICtrl.Logger.Log("Capture FPS: " + _CaptureFPS);
                            _LastDateTimeCapture = DateTime.Now;
                        }
                        _LastDisplayFrameId = displayFrameId;
                        
                   }
                   
                }
                if (_CaptureFPS > 20)
                {
                    // Sleep for 10ms - avoid grabbing too fast in case of video input
                    Thread.Sleep(10);
                }
            }
        }
        /// <summary>
        /// Detect part thread
        /// </summary>
        public void DetectThread()
        {
            while (true)
            {
                if (Capturing && Loaded && SatisAICtrl.PcbBoard.Parts.Count>0 && !_Inspecting)
                {
                    
                    int frameId = _CaptureFrameId-1;
                    if (frameId <= 0) continue;
                    IntPtr BoundingBoxesPtr = Detect(_DetectorWraper, frameId, DETECT_CONF, NMS_CONF);
                    string? response = Utils.ConvertPtrToString(BoundingBoxesPtr);
                    FreeBuffer(BoundingBoxesPtr);
                    if (response != null)
                    {
                        List<BoundingBox> detections = Utils.ConvertJsonToBoxes(response);
                        for ( int i = 0; i < detections.Count; i++)
                        {
                            BoundingBox detection = detections[i];
                            if (_InspectLabels.Contains(detection.Label!))
                            {
                                if (detection.Left > 50 && (detection.Left + detection.Width + 50) < _Width)
                                {
                                    try
                                    {
                                        int rotation = DetectRotation(_DetectorWraper, frameId, i, ROTATION_CONF);
                                        detections[i].Rotation = rotation;
                                    }catch (Exception ex)
                                    {

                                    }
                                }
                            }
                        }
                        FrameQueue.Add(new FrameData(frameId, detections));
                        if (detections.Count > 0 && frameId > 10 && _TemplateLoaded)
                        {
                            if (InspectMode)
                                CheckPCB(frameId, detections);
                        }
                    }
                    NumFramesDelay = _CaptureFrameId - frameId;

                    if (_DetectFrameId % 10 == 0 && _DetectFrameId != 0)
                    {
                        float diff = (float)(DateTime.Now - _LastDateTimeDetect).TotalMilliseconds;
                        _DetectFPS = 10f / (diff / 1000f);
                        _DetectFPS = ((int)(_DetectFPS * 10)) / 10f;
                        SatisAICtrl.Logger.Log("Detect FPS: " + _DetectFPS);
                        _LastDateTimeDetect = DateTime.Now;
                    }
                    _DetectFrameId++;

                }
                Thread.Sleep(10);
            }
        }

        #endregion

    }
}
