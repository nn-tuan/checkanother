﻿using SatisAICtrl.Files;
using System.Xml;
using System.Diagnostics;
using System.Drawing;
using SatisAICtrl.Models;

namespace SatisAICtrl
{
    public class SatisAICtrl : ISatisAICtrl
    {
        #region Fields
        private readonly SAIPlace _SAIPlace = new SAIPlace();
        #endregion Fields

        #region Properties
        public string CameraType = "";
        public int CameraIndex { get; set; }=0;
        public string CameraPath = "";
        public List<string> PartNames = new List<string>();
        public PcbBoard PcbBoard { get; set; } = new PcbBoard();
        public CloudSynch Syncher { get; set; } = new CloudSynch();
        public LogType _LogType { get; set; } = LogType.All;
        public Logger Logger { get; set; } = new Logger();
        public int GpuId { get; set; } = -1; // -1: CPU, 0,1,2,.. GPU
        public string ShowMode { get; set; } = "NG"; // NG, ALL, NONE
        public bool InspectMode { get; set; } = true;

        private string _ResultOutputDirPath = "";
        private NativeWrapper? _NativeWrapper;
        private bool _UpdateFlag = true;

        public string ResultOutputDirPath
        {
            get
            {
                return _ResultOutputDirPath;
            }
            set
            {
                _ResultOutputDirPath = value;

                string dateTimeStr = DateTime.Now.ToString("yyyyMMdd-hhmmss");
                CsvPath = _ResultOutputDirPath + "\\result_" + dateTimeStr + ".csv";
                CsvDetailPath = _ResultOutputDirPath + "\\result_" + dateTimeStr + "_detail.csv";
            }
        }

        public string CsvPath { get; set; } = "";
        public string CsvDetailPath { get; set; } = "";
        #endregion Properties

        #region Event Actions
        /// <summary>
        /// ロードされた配置情報ファイルの変更イベント
        /// </summary>
        public event Action<PcbBoard>? SAIPlaceFileChangeEvent;
        public event Action<string>? SAILogChangeEvent;

        /// <summary>
        /// 画像描画開始イベント
        /// </summary>
        public event Action<Uri>? ViewImageOpenEvent;
        /// <summary>
        /// 動画描画開始イベント
        /// </summary>
        public event Action<Uri>? ViewMovieOpenEvent;
        /// <summary>
        /// 結果更新イベント
        /// </summary>
        public event Action<string>? UpdateInspectionResultEvent;

        public event Action<List<PartItem>>? NGPartItemsEvent;

        /// <summary>
        /// Closeイベント
        /// </summary>
        public event Action? CloseAppEvent;
        public event Action? StartCameraEvent;
        public event Action? StopCameraEvent;
        public event Action? StartInspectEvent;
        public event Action? StopInspectEvent;
        public event Action<byte[], int, int, int>? ImageDataUpdateEvent;
        public event Action<PcbBoard>? PcbUpdateEvent;

        /// <summary>
        /// Part registration screen
        /// </summary>
        public event Action? StartPartRegisterEvent;
        public event Action? StopPartRegisterEvent;
        public event Action? StartPcbRegisterEvent;
        public event Action? StopPcbRegisterEvent;
        public event Action? RegisterPcbRegisterEvent;

        #endregion Event Actions

        #region Public Methods
        public SatisAICtrl()
        {
            RaiseSAILogChangeEvent("Started");
            Thread th = new Thread(() =>
            {
                string systemInfo = new SystemInfo().GetInfo();
                Logger.Log(systemInfo);

                RaiseSAILogChangeEvent("Logged System Info");
            });
            th.IsBackground = true;
            th.Start();
        }
        public void SetCamera(string type, int index, string path)
        {
            Syncher.Log("camera/type", type);
            Syncher.Log("camera/path", path);
            Logger.Log("Camera Type: " + type);
            Logger.Log("Camera Index: " + index);
            Logger.Log("Camera Path: " + path);
            CameraType = type;
            CameraIndex = index;
            CameraPath = path;
            _NativeWrapper = new NativeWrapper(InspectMode, this);
            bool useGpu = false;
            if (GpuId >= 0)
            {
                
                useGpu = true;
                bool hasGPU = _NativeWrapper.CheckGPUAvailable();

                if (hasGPU)
                {
                    RaiseSAILogChangeEvent("GPU is detected");
                }
                else
                {
                    RaiseSAILogChangeEvent("GPU is not detected");
                    useGpu = false;
                }
                if (useGpu)
                {
                    int gpuMem = Utils.GetGPUFreeMem(GpuId);

                    if (gpuMem < 1000)
                    {
                        useGpu = false;
                        RaiseSAILogChangeEvent("GPU doesn't have enough free mem, using CPU instead");
                    }
                }
            }
            Syncher.Log("gpu/enable", useGpu.ToString());

            if (useGpu)
            {
                RaiseSAILogChangeEvent("Loading models using GPU " + GpuId);
                _NativeWrapper = new NativeWrapper(InspectMode, this, "Resources\\models\\pcb_parts.pt", GpuId);
            }
            else
            {
                RaiseSAILogChangeEvent("Loading models using CPU");
                _NativeWrapper = new NativeWrapper(InspectMode, this, "Resources\\models\\pcb_parts.pt", -1);
            }
            string[] modelPaths = Directory.GetFiles("Resources\\models");
            bool benchmark = false;
            RaiseSAILogChangeEvent("Loading rotation models");
            foreach (string modelPath in modelPaths)
            {
                if (modelPath.Contains(".pt") && !modelPath.Contains("parts"))
                {
                    if (!benchmark) benchmark = true;
                    string fileName = Path.GetFileName(modelPath);
                    string partName = fileName.Replace(".pt", "").Replace("part_", "");
                    _NativeWrapper.LoadRotationModel(modelPath, partName, benchmark);
                    benchmark = true;
                }
            }
            RaiseSAILogChangeEvent("Models Loaded, Opening camera");
            bool camOpened = _NativeWrapper.SetupCam(CameraType, CameraIndex, CameraPath);
            if (camOpened)
                RaiseSAILogChangeEvent("Camera opened");
            else
                RaiseSAILogChangeEvent("Cannot open camera, please restart app");
            _NativeWrapper.ImageDataUpdateEvent += RaiseImageDataUpdateEvent;
            _NativeWrapper.InspectEvent += RaiseInspectEvent;
        }

        public void ToggleNGShowMode()
        {
            if (ShowMode == "NG") ShowMode = "ALL";
            else if (ShowMode == "ALL") ShowMode = "NONE";
            else ShowMode = "NG";
            RaiseSAILogChangeEvent("Show "+ShowMode+" only");
            
        }

        /// <summary>
        /// 配置情報ファイルの読み込みを実行します。
        /// 1. 上位からFile取得
        /// 2. SequenceEngine側に変更
        /// </summary>
        /// <param name="filePath"></param>
        public void LoadSAIPlaceFile(string filePath, bool ignoreLack)
        {

            // ToDo: ファイルの内容を読み込み

            // Debug: 読み込んだと仮定してダミーデータで実装
            PartNames = new List<string>();
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(filePath);
            try
            {
                PcbBoard = new PcbBoard();
                //PcbBoard.OnBoardChanged += _OnBoardUpdate;
                
                XmlNode headerNode = xmlDoc.SelectSingleNode("saiplace/header")!;
                XmlNode imageNode = xmlDoc.SelectSingleNode("saiplace/image")!;
                XmlNode bodyNode = xmlDoc.SelectSingleNode("saiplace/body")!;

                string headerText = headerNode.InnerText.Trim();
                string partsText = bodyNode.InnerText.Trim();
                string[] headerLines = headerText.Split(',');
                string[] partsLines = partsText.Split('\n');
                PcbBoard.Name = headerLines[0];
                //_NativeWrapper!.SetPCBDims(PcbBoard.Width, PcbBoard.Height);
                string imagePath = imageNode.InnerText.Trim();
                bool result = _NativeWrapper!.LoadTemplateImage(imagePath!);
                if (!result)
                {
                    RaiseSAILogChangeEvent("Cannot load template in saiplace file");
                    return;
                }

                foreach (string line in partsLines)
                {
                    if (line.Length < 2) continue;
                    string[] values = line.Trim().Split(',');
                    PartItem item = new PartItem()
                    {
                        ID = values[1],
                        Name = values[0],
                        X = int.Parse(values[2]),
                        Y = int.Parse(values[3]),
                        Rotation = int.Parse(values[4]),
                        Torelation = int.Parse(values[5]),
                        RotationTorelation = int.Parse(values[6]),
                    };
                    PcbBoard.AddItem(item);
                    PartNames.Add(values[1]);
                }
                _NativeWrapper!.InspectMode = true;
                _NativeWrapper!.Board = PcbBoard;
                Syncher.Log("board", PcbBoard);
                // イベント呼び出し
                RaiseSAIPlaceFileChangeEvent(PcbBoard);
                RaiseSAILogChangeEvent("Loaded SaiPlace, detecting pcb");
                //RaiseViewImageOpenEvent(inspectionImage);
                //RaiseViewMovieOpenEvent(uriMovie);
                //RaiseUpdateInspectionResultEvent(result); // OK, NG
            }
            catch (Exception ex)
            {
                //TODO: show error message
                Trace.WriteLine("Error: " + ex.Message);
            }
        }

        public void _OnBoardUpdate()
        {
            UpdateCheckBox();
        }

        public bool StartRecord(string videoPath)
        {
           return _NativeWrapper!.StartRecordVideo(videoPath);
        }
        public bool StopRecord()
        {
            return _NativeWrapper!.StopRecordVideo();
        }

        public bool StartRegisterPcb()
        {
            _NativeWrapper!.Capturing = false;
            Thread.Sleep(50);
            List<BoundingBox> bboxes =  _NativeWrapper!.DetectAndEstimateArrow();
            PcbBoard = new PcbBoard();
            PcbBoard.Width = _NativeWrapper!._Width;
            PcbBoard.Height = _NativeWrapper!._Height;
            int count = 1;
            foreach(BoundingBox b in bboxes)
            {
                if (b.Label == "pcb_anchor")
                {
                    PcbBoard.corners.Add(b.GetCenter());
                }
                else
                {
                    if (b.Rotation >= 0)
                    {
                        PartItem item = new PartItem()
                        {
                            No = count,
                            ID = b.Label,
                            Rotation = b.Rotation,
                            Name = b.Label,
                            Confirm = false,
                            Box = b,
                        };
                        PcbBoard.Parts.Add(item);
                    }
                    count++;
                }
            }
            foreach (PartItem part in PcbBoard.Parts)
            {
                part.ConfirmUpdate += _OnBoardUpdate;
            }
            PcbUpdateEvent?.Invoke(PcbBoard);
            return true;
        }

        public void TogglePcbPart(int index)
        {
            if (index < PcbBoard.Parts.Count)
            {
                PcbBoard.Parts[index].Confirm = !PcbBoard.Parts[index].Confirm;
                PcbUpdateEvent?.Invoke(PcbBoard);
            }
        }
        public void TogglePcbPartBackground(int index)
        {
            if (index < PcbBoard.Parts.Count)
            {
                PcbBoard.Parts[index].IsMoveIn = !PcbBoard.Parts[index].IsMoveIn;
                PcbUpdateEvent?.Invoke(PcbBoard);
            }
        }
        public void UpdateCheckBox()
        {
            if(_UpdateFlag)
            {
                PcbUpdateEvent?.Invoke(PcbBoard);
            }
        }
        public void DisanableUpdate()
        {
            _UpdateFlag = false;
        }
        public void EnableUpdate()
        {
            _UpdateFlag = true;
        }

        //public void AddCorner(int x, int y)
        //{

        //    PcbBoard.corners.Add(new Point(x,y));
        //    while (PcbBoard.corners.Count > 4)
        //    {
        //        PcbBoard.corners.RemoveAt(0);
        //    }
        //    PcbUpdateEvent?.Invoke(PcbBoard);
        //}
        //public void UpdatePcb(PcbBoard pcb)
        //{
        //    PcbBoard = pcb;   
        //    PcbUpdateEvent?.Invoke(PcbBoard);
        //}
        public bool StopRegisterPcb()
        {
            PcbBoard = new PcbBoard();
            PcbUpdateEvent?.Invoke(PcbBoard);
            _NativeWrapper!.Capturing = true;
            return true;
        }
        public string RegisterPcb(string OutputPartTxt,string NewProjectName, string NewBoardName, string ModelNumber, int RotationTol, int DistanceTol)
        {
            string outPath = OutputPartTxt + "\\" + NewProjectName.Replace(" ", "-") + ".saiplace";
            string imagePath = OutputPartTxt + "\\" + NewProjectName.Replace(" ", "-") + ".png";
            _NativeWrapper!.SaveFrame(imagePath);

            XmlDocument xmlDoc = new XmlDocument();
            XmlDeclaration xmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", "UTF-8", null);
            XmlElement? root = xmlDoc.DocumentElement;
            xmlDoc.InsertBefore(xmlDeclaration, root);
            XmlElement SaiplaceElement = xmlDoc.CreateElement("", "saiplace", string.Empty);

            string header = "\n\t\t" + NewBoardName + "\n\t";
            //string header = "\n\t\t" + NewBoardName + "," + BoardWidth + "," + BoardHeight + "\n\t";
            XmlElement HeaderElement = xmlDoc.CreateElement("", "header", string.Empty);
            HeaderElement.AppendChild(xmlDoc.CreateTextNode(header));
            string model = "\n\t\t" + ModelNumber + "\n\t";
            XmlElement ModelElement = xmlDoc.CreateElement("", "model", string.Empty);
            ModelElement.AppendChild(xmlDoc.CreateTextNode(model));

            XmlElement ImageElement = xmlDoc.CreateElement("", "image", string.Empty);
            ImageElement.AppendChild(xmlDoc.CreateTextNode(imagePath));

            HeaderElement.SetAttribute("ver", "1.00");
            string body = "";
            foreach (PartItem part in PcbBoard.Parts)
            {
                if (part.Confirm)
                {
                    //PointF pt = _NativeWrapper!.TransformCoordinate(PcbBoard.corners, part.Box.GetCenter());
                    Point pt = part.Box!.GetCenter();
                    
                    int rotation = part.Rotation;
                    body += "\t\t" + part.Name + "," + part.ID + "," + pt.X + "," + pt.Y + "," + rotation + ","+ RotationTol + ","+ DistanceTol + "\r\n";
                }
            }
            body = body.Trim();
            body = "\n\t\t" + body + "\n\t";

            XmlElement BodyElement = xmlDoc.CreateElement("", "body", string.Empty);
            BodyElement.AppendChild(xmlDoc.CreateTextNode(body));
            SaiplaceElement.AppendChild(HeaderElement);
            SaiplaceElement.AppendChild(ModelElement);
            SaiplaceElement.AppendChild(ImageElement);
            SaiplaceElement.AppendChild(BodyElement);

            xmlDoc.AppendChild(SaiplaceElement);

            xmlDoc.Save(outPath);
            return outPath;
        }

        public void RaiseStartPartRegisterEvent()
        {
            StartPartRegisterEvent?.Invoke();
        }
        public void RaiseStopPartRegisterEvent()
        {
            StopPartRegisterEvent?.Invoke();
        }
        public void RaiseStartPcbRegisterEvent()
        {
            StartPcbRegisterEvent?.Invoke();
        }
        public void RaiseStopPcbRegisterEvent()
        {
            StopPcbRegisterEvent?.Invoke();
        }
        public void RaiseRegisterPcbRegisterEvent()
        {
            RegisterPcbRegisterEvent?.Invoke();
        }

        public void RaiseSAILogChangeEvent(String message)
        {
            SAILogChangeEvent?.Invoke(message);
        }

        /// <summary>
        /// 閉じる処理
        /// </summary>
        public void CloseApp()
        {
            RaiseCloseEvent();
        }

        #endregion Public Methods

        #region Private Methods

        /// <summary>
        /// このメソッドでは読み込まれている配置情報ファイルが変更されたイベントを上位に対して伝達するメソッドです。
        /// </summary>
        /// <param name="filePath"></param>
        private void RaiseSAIPlaceFileChangeEvent(PcbBoard pcb)
        {
            // 内部で確保している変数を更新
            //_ApplicationSettingFile.RecipeFilePath = filePath;

            // GUIの表示を更新
            SAIPlaceFileChangeEvent?.Invoke(pcb);
        }

        /// <summary>
        /// このメソッドでは検査映像の画像を開くイベントを上位に対して伝達するメソッドです。
        /// </summary>
        /// <param name="filePath"></param>
        private void RaiseViewImageOpenEvent(Uri inspectionImage)
        {
            // 内部で確保している変数を更新
            //_ApplicationSettingFile.RecipeFilePath = filePath;

            // GUIの表示を更新
            ViewImageOpenEvent?.Invoke(inspectionImage);
        }

        /// <summary>
        /// このメソッドでは検査映像の動画を開始するイベントを上位に対して伝達するメソッドです。
        /// </summary>
        /// <param name="filePath"></param>
        private void RaiseViewMovieOpenEvent(Uri inspectionMovieUri)
        {
            // GUIの表示を更新
            ViewMovieOpenEvent?.Invoke(inspectionMovieUri);
        }

        private void RasieUpdateNGPartItemsEvent(List<PartItem> items)
        {
            // GUIの表示を更新
            NGPartItemsEvent?.Invoke(items);
        }

        /// <summary>
        /// このメソッドでは検査結果更新イベントを上位に対して伝達するメソッドです。
        /// </summary>
        /// <param name="filePath"></param>
        public void RaiseUpdateInspectionResultEvent(string inspectionMessage)
        {
            // GUIの表示を更新
            UpdateInspectionResultEvent?.Invoke(inspectionMessage);
        }

        /// <summary>
        /// このメソッドでは閉じるイベントを上位に対して伝達するメソッドです。
        /// </summary>
        private void RaiseCloseEvent()
        {
            // 閉じる処理を実行
            CloseAppEvent?.Invoke();
        }

        private void RaiseImageDataUpdateEvent(byte[] data, int w, int h, int c)
        {
            ImageDataUpdateEvent?.Invoke(data, w, h, c);
        }

        private float CalcPointDistance(PointF p1, PointF p2)
        {
            double diffX = p1.X - p2.X;
            double diffY = p1.Y - p2.Y;
            return (float)Math.Sqrt(diffX * diffX + diffY * diffY);
        }

        private void RaiseInspectEvent(List<BoundingBox> detections, int frameId)
        {
            string logResults = "";
            string logResultsDetail = "";
            if (!File.Exists(CsvDetailPath))
            {
                logResultsDetail += "DateTime,PartID,PartName,X,Y,Rotation,ResultX,ResultY,ResultRotation,Error\n";
            }
            string dateTimeStr = DateTime.Now.ToString("yyyyMMdd-hhmmss");
            string imagePath = ResultOutputDirPath + "\\image_" + dateTimeStr + ".jpg";
            string partResultLog = "";

            _NativeWrapper!.SaveInspectImages(detections, frameId, ResultOutputDirPath, dateTimeStr, imagePath);

            if (detections != null)
            {
                bool NG = false;
                List<string> errors = new List<string>();
                List<PartItem> NGPartItems = new List<PartItem>();
                for(int i=0;i<PcbBoard.Parts.Count;i++)
                {
                    PartItem item = PcbBoard.Parts[i];
                    BoundingBox box = detections[i];
                    //logResults += item.Name + "\tx:" + item.X + ", y:" + item.Y + ", rotation:" + item.Rotation + ", tol:" + item.Torelation + "\t";
                    logResultsDetail += $"{dateTimeStr},{item.ID},{item.Name},{item.X},{item.Y},{item.Rotation},";
                    string partResult = "OK";
                    string error = "";
                    if (box.WrongPlace)
                    {
                        error = "Missed";
                        partResult = "None";
                        logResultsDetail += ",,,";
                    }
                    else
                    {
                        logResultsDetail+= $"{box.XTemplate},{box.YTemplate},{box.Rotation},";
                        if (box.MissedRotation)
                        {
                            error += "Cannot detect angle ";
                            partResult = "NG";
                        }
                        else
                        {
                            if (box.WrongRotation)
                            {
                                error += "Wrong angle ";
                                partResult = "NG";
                            }
                        }
                    }
                    logResultsDetail += error+"\n";
                    errors.Add(error);
                    if (error != "")
                    {
                        PartItem ngItem = new PartItem()
                        {
                            ID = item.ID,
                            Name = item.Name,
                            Reason = error
                        };
                        NGPartItems.Add(ngItem);
                    }
                    partResultLog += item.ID + "-" + item.Name + "," + partResult + ",";
                }


                foreach (string error in errors)
                {
                    if (error != "") NG = true;
                }
                string result = "OK";
                if (NG) result = "NG";
                partResultLog += imagePath + "\n";

                logResults = dateTimeStr + "," + result + "," + partResultLog;
                Syncher.LogPush("results", logResultsDetail);

                bool writeLog = true;
                //Trace.WriteLine("Inspect 3 "+ result);
                RaiseUpdateInspectionResultEvent(result);
                //Trace.WriteLine("Inspect 4");
                RasieUpdateNGPartItemsEvent(NGPartItems);
                //File.WriteAllText("out//" + dateTimeStr + "__pcb.log", logResults);
                

                if (_LogType == LogType.None) writeLog = false;
                if (_LogType == LogType.NG && !NG) writeLog = false;
                if (writeLog)
                {
                    if (!Directory.Exists(_ResultOutputDirPath))
                    {
                        RaiseSAILogChangeEvent("Output folder is not existed, saving failed");
                    }
                    else
                    {
                        File.AppendAllText(CsvPath, logResults);
                        File.AppendAllText(CsvDetailPath, logResultsDetail);
                        RaiseSAILogChangeEvent("Saved log, visualization image");
                    }
                }
                else
                {
                    if (File.Exists(imagePath)) File.Delete(imagePath);
                }
                //Trace.WriteLine("Inspect 5");
            }
        }

        public void StartStopInspect(bool isStart)
        {
            if (isStart)
                StartInspectEvent?.Invoke();
            else
                StopInspectEvent?.Invoke();
        }

        public void StartStopCam(bool isStart)
        {
            if (isStart)
                StartCameraEvent?.Invoke();
            else
                StopCameraEvent?.Invoke();
        }


        #endregion Private Methods
    }
}