﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SatisAICtrl
{
    public class Logger
    {
        String LoggerPath { get; set; } = "";
        public Logger(string directory = "logs", string prefix = "")
        {
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
            string dateTimeStr = DateTime.Now.ToString("yyyyMMdd-hhmmss");
            if (prefix.Length > 0)
            {
                LoggerPath = directory + "\\" + prefix + "_" + dateTimeStr + "_log.txt";
            }
            else
            {
                LoggerPath = directory + "\\" +  dateTimeStr + "_log.txt";
            }
            Log("Started"); 
        }

        public void Log(string message)
        {
            string dateTimeStr = DateTime.Now.ToString("yyyyMMdd-hhmmss");
            if (LoggerPath.Length > 0)
            {
                try
                {
                    File.AppendAllText(LoggerPath, dateTimeStr + ": " + message.Trim() + "\n");
                }catch (Exception)
                {

                }
            }
        }

    }
}
