﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Threading.Tasks;

namespace SatisAICtrl.Models
{
    public class PcbBoard
    {
        public string Name { get; set; }
        public int Width { get; set; } // in mm
        public int Height { get; set; } // in mm
        public List<Point> corners { get; set; } 
        public ObservableCollection<PartItem> Parts { get; set; }
        public PcbBoard()
        {
            Name = "";
            Parts = new ObservableCollection<PartItem>();
            corners = new List<Point>();
        }
        public void AddItem(PartItem item)
        {
            Parts.Add(item);
            //_PartsConfirmUpdate();
        }

    }
}
