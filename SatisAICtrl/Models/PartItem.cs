﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace SatisAICtrl.Models
{
    public class PartItem: ViewModelBase
    {
        public event Action? ConfirmUpdate;

        private string? _ID;
        public string? ID 
        {
            get
            {
                return _ID;
            }
            set
            {
                _ID = value;
                Confirm = false;
                RaisePropertyChangedEvent("ID");
            }
        }
        private string? _Name;
        public string? Name 
        { 
            get
            {
                return _Name;
            }
            set
            {
                _Name = value;
                Confirm = false;
                RaisePropertyChangedEvent("Name");
            }
        }

        private bool _Confirm = false;
        public bool Confirm
        {
            get
            {
                return _Confirm;
            }
            set
            {
                _Confirm = value;
                RaisePropertyChangedEvent("confirm");
                ConfirmUpdate?.Invoke();
            }
        }
        public bool IsMoveIn { get; set; }
        public int No { get; set; } 
        public int X { get; set; } // in mm
        public int Y { get; set; } // in mm
        public string Reason { get; set; } = ""; // NG Reason
        private int _Rotation;
        public int Rotation  // in mm
        { 
            get
            {
                return _Rotation;
            }
            set
            {
                _Rotation = value;
                Confirm = false;
                RaisePropertyChangedEvent("Rotation");
            }
        }
        public int Torelation { get; set; } // in mm
        public int RotationTorelation { get; set; } // in angle



        public BoundingBox? Box { get; set; }

        public bool Check(int x, int y, int rotation)
        {
            if (Math.Abs(X - x) > Torelation ||
                Math.Abs(Y - y) > Torelation ||
                Math.Abs(Rotation - rotation) > 10
                )
                return false;
            return true;
        }
        public PartItem()
        {
        }
    }

    public class ViewModelBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler? PropertyChanged;

        protected void RaisePropertyChangedEvent(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChangedEventArgs e = new PropertyChangedEventArgs(propertyName);
                PropertyChanged(this, e);
            }
        }
    }
}
