﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using Newtonsoft.Json;

namespace SatisAICtrl.Models
{
    public class FrameDataQueue
    {
        public List<FrameData> FrameDatas;
        public FrameDataQueue()
        {
            FrameDatas = new List<FrameData>();
        }
        public void Add(FrameData data)
        {
            FrameDatas.Add(data);  
            if (FrameDatas.Count > 100)
            {
                FrameDatas.RemoveAt(0);
            }
        }

        public FrameData? Get(int frameId)
        {
            for (int i=FrameDatas.Count-1; i >= 0; i--)
            {
                if (FrameDatas[i].FrameId == frameId)
                    return FrameDatas[i];
            }
            return null;
        }
        public FrameData Get()
        {
            return FrameDatas[FrameDatas.Count-1];
        }
        public FrameData GetAverage(int durationMs)
        {
            if (durationMs <= 0) return Get();
            FrameData frame = Get().Copy();
            
            // Find results in a duration window
            List<FrameData> frames = new List<FrameData>();
            for (int i= FrameDatas.Count-2; i >=0; i--)
            {
                FrameData curFrame = FrameDatas[i];
                long timeDiff = frame.TimeStamp - curFrame.TimeStamp;
                if (timeDiff <= durationMs)
                {
                    frames.Add(curFrame);
                }
            }
            
            //Find a match boxes based on distances
            for (int i=0; i<frame.BoundingBoxes.Count;i++)
            {
                BoundingBox box = frame.BoundingBoxes[i];
                int boxSize = Math.Max(box.Width, box.Height);
                List <BoundingBox> boxes = new List<BoundingBox>();
                boxes.Add(box);

                for (int j = 0; j < frames.Count; j++)
                {
                    float minDistance = 999999;
                    int minId = -1;
                    for(int k=0; k< frames[j].BoundingBoxes.Count; k++)
                    {
                        BoundingBox _box = frames[j].BoundingBoxes[k];
                        if (box.Label == _box.Label)
                        {
                            float distance = box.Distance(_box);
                            // Distance between two boxes must smaller than size of the box 
                            if (distance < minDistance && distance < boxSize)
                            {
                                minDistance = distance;
                                minId = k;
                            }
                        }
                    }
                    if (minId != -1)
                    {
                        boxes.Add(frames[j].BoundingBoxes[minId]);
                    }
                }

                // Averaging results
                List<int> rotationList = new List<int>();
                foreach (BoundingBox _box in boxes)
                {
                    if (_box.Rotation != -1)
                    {
                        rotationList.Add(_box.Rotation);
                    }
                }
                rotationList.Sort();
                if (rotationList.Count > 1) {
                    int midPos = rotationList.Count / 2;
                    if (rotationList.Count%2 == 1) midPos++;
                    frame.BoundingBoxes[i].Rotation = rotationList[midPos];
                }
            }
            
            return frame;
        }
    }
    public class FrameData
    {
        public List<BoundingBox> BoundingBoxes;
        public int FrameId;
        public long TimeStamp;
        public FrameData()
        {
            BoundingBoxes = new List<BoundingBox>();
            FrameId = 0;
            TimeStamp = DateTime.Now.Ticks;
        }
        public FrameData(int frameId, List<BoundingBox> boundingBoxes)
        {
            FrameId = frameId;
            BoundingBoxes = boundingBoxes;
            TimeStamp = DateTime.Now.Ticks/10000;
        }
        public FrameData Copy()
        {
            FrameData obj = new FrameData();
            obj.FrameId = FrameId;
            obj.TimeStamp = TimeStamp;
            obj.BoundingBoxes = new List<BoundingBox>();
            foreach(BoundingBox box in BoundingBoxes)
            {
                obj.BoundingBoxes.Add(box);
            }
            return obj;
        }
    }
    public class BoundingBox
    {
        #region Properties
        [JsonProperty("y")]
        public int Top { get; set; } = 0;
        [JsonProperty("x")]
        public int Left { get; set; } = 0;
        [JsonProperty("w")]
        public int Width { get; set; } = 0;
        [JsonProperty("h")]
        public int Height { get; set; } = 0;
        [JsonProperty("label")]
        public string? Label { get; set; }

        [JsonProperty("rotation")]
        public int Rotation { get; set; } = -1;
        public int AbsoluteAngle { get; set; } = -1;
        public bool Inspected { get; set; } = false;
        public bool Pass { get; set; } = true;
        public bool WrongPlace { get; set; } = false;
        public bool WrongRotation { get; set; } = false;
        public bool MissedRotation { get; set; } = false;
        public int ArrowLength { get; set; } = 0;
        [JsonProperty("score")]
        public float Score { get; set; } = 0;

        [JsonProperty("xTemplate")]
        public float XTemplate { get; set; } = 0;

        [JsonProperty("yTemplate")]
        public float YTemplate { get; set; } = 0;
        public string Color { get; set; } = "#FF0000";
        #endregion
        public BoundingBox()
        {

        }
        public BoundingBox(int left, int top, int width, int height)
        {
            Top = top;
            Left = left;
            Width = width;
            Height = height;
            ArrowLength = (int)(Math.Max(width, height) * 0.75);
        }
        public BoundingBox(int left, int top, int width, int height, float score, string label)
        {
            Top = top;
            Left = left;
            Width = width;
            Height = height;
            ArrowLength = (int)(Math.Max(width, height) * 0.75);
            Score = score;
            Label = label;
        }

        public Point GetCenter()
        {
            return new Point(Left + Width / 2, Top + Height / 2);
        }
        public Point GetEndOfArrow()
        {
            double angleRadian = -Rotation * 3.1415f / 180.0f;
            int dy = (int)(ArrowLength * Math.Sin(angleRadian));
            int dx = (int)(ArrowLength * Math.Cos(angleRadian));
            return new Point(Left + dx, Top + dy);
        }
        public float Distance(BoundingBox obj)
        {
            Point p1 = GetCenter();
            Point p2 = obj.GetCenter();
            return Utils.CalcPointDistance(p1, p2);
        }
    }
}
