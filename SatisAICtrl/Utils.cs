﻿using System.Diagnostics;
using System.Drawing;
using System.Runtime.InteropServices;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SatisAICtrl.Models;

namespace SatisAICtrl
{
    public static class Utils
    {

#region Conversions from native pointer
        public static string? ConvertPtrToString(IntPtr pBuffer)
        {
            return Marshal.PtrToStringAnsi(pBuffer);
        }

        /// <summary>
        /// Convert native pointer to float vector
        /// </summary>
        /// <param name="pBuffer"></param>
        /// <returns></returns>
        public static List<float> ConvertPtrToFloatVector(IntPtr pBuffer)
        {
            List<float> arrList = new List<float>();
            byte[] lenBuffer = new byte[4];
            Marshal.Copy(pBuffer, lenBuffer, 0, 4);

            float arrSize = BitConverter.ToSingle(lenBuffer);
            int numBytes = (int) arrSize * 4 + 4;
            if (arrSize == 0) return arrList;
            byte[] data = new byte[numBytes];
            Marshal.Copy(pBuffer, data, 0, numBytes);

            for (int i = 1; i < (arrSize + 1); i++)
            {
                float v = BitConverter.ToSingle(data, i * 4);
                arrList.Add (v);
            }
            return arrList;
        }

        /// <summary>
        /// Convert native pointer to image data object, first 12 bytes are encoded to height (int - 4 bytes-32 bit), width, channels
        /// </summary>
        /// <param name="pBuffer"></param>
        /// <returns>byte object</returns>
        public static byte[]?
        ConvertPtrToImageData(IntPtr pBuffer, out int w, out int h, out int c)
        {
            byte[] buffer = new byte[12];
            Marshal.Copy(pBuffer, buffer, 0, 12);
            List<float> headers = new List<float>();
            for (int i = 0; i < 3; i++)
            {
                float v = BitConverter.ToSingle(buffer, i * 4);
                headers.Add (v);
            }

            h = (int) headers[0];
            w = (int) headers[1];
            c = (int) headers[2];
            if (h == 0 || w == 0) return null;
            int imgBytesCount = w * h * c + 12;
            byte[] fullData = new byte[imgBytesCount];
            Marshal.Copy(pBuffer, fullData, 0, imgBytesCount);
            byte[] imageData = new byte[imgBytesCount - 12];
            Array.Copy(fullData, 12, imageData, 0, imgBytesCount - 12);
            return imageData;
        }
#endregion



#region Utils
        /// <summary>
        /// Convert native pointer to vector of BoudingBox objects
        /// </summary>
        /// <param name="pBuffer"></param>
        /// <returns></returns>
        public static List<BoundingBox> ConvertJsonToBoxes(string _json)
        {
            int index = _json.IndexOf(']');
            string json = _json.Substring(0, index+1);
            List<BoundingBox> detections = new List<BoundingBox>();
            try
            {
                var result = JsonConvert.DeserializeObject<List<BoundingBox>>(json);
                if (result != null)
                {
                    return result;
                }
                else
                {
                    return detections;
                }
            }catch (Exception ex)
            {
                Console.Write(ex.ToString());
                return detections;
            }
        }

        /// <summary>
        /// Euclidean distance between points
        /// </summary>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        /// <returns></returns>
        public static float CalcPointDistance(PointF p1, PointF p2)
        {
            double diffX = p1.X - p2.X;
            double diffY = p1.Y - p2.Y;
            return (float) Math.Sqrt(diffX * diffX + diffY * diffY);
        }

        /// <summary>
        /// Get GPU's free memory
        /// </summary>
        /// <param name="device_id">GPU ID</param>
        /// <returns>free memory (MB)</returns>
        public static int GetGPUFreeMem(int device_id)
        {
            try
            {
                string command =
                    "-i " +
                    device_id +
                    " --query-gpu=memory.free --format=csv,noheader,nounits";

                Process cmd = new Process();
                cmd.StartInfo.FileName = "nvidia-smi";
                cmd.StartInfo.Arguments = command;
                cmd.StartInfo.RedirectStandardInput = true;
                cmd.StartInfo.RedirectStandardOutput = true;
                cmd.StartInfo.CreateNoWindow = true;
                cmd.StartInfo.UseShellExecute = false;
                cmd.Start();

                //cmd.StandardInput.WriteLine(command);
                cmd.StandardInput.Flush();
                cmd.StandardInput.Close();
                cmd.WaitForExit();
                string res = cmd.StandardOutput.ReadToEnd();
                int free_mem = int.Parse(res.Trim());
                return free_mem;
            }
            catch (Exception)
            {
                return -1;
            }
        }


#endregion

    }
}
