#include <fstream>
#include <opencv2/opencv.hpp>
#include <stdio.h>
#include <string>
#include <iostream>
#include <vector>
#include <memory>
#ifndef __BBOX_H__
#define __BBOX_H__
#include "BBox.h"
#endif

using namespace cv;
using namespace std;

void Log(unsigned char* msg)
{
    ofstream writer("log.txt", std::ios_base::app);
    writer << msg << endl;
    writer.close();
}
void Log(string msg)
{
    ofstream writer("log.txt", std::ios_base::app);
    writer << msg << endl;
    writer.close();
}
void Logf(float value)
{
    ofstream writer("log.txt", std::ios_base::app);
    writer << value << endl;
    writer.close();
}
void Logi(int value)
{
    ofstream writer("log.txt", std::ios_base::app);
    writer << value << endl;
    writer.close();
}

Mat resizeMax(Mat img, int maxWidth)
{
    float scale = (float)maxWidth / img.cols;
    if (scale > 1)
        return img;
    Size size((int)img.cols * scale, (int)img.rows * scale);
    Mat img2;
    resize(img, img2, size);
    return img2;
}

Mat resizeImg(Mat img)
{
    return resizeMax(img, 1000);
}

unsigned char *arrayToBytes(vector<float> arr)
{
    float len = (float)arr.size();
    ofstream writer;
    /* if (arr.size() > 0)
         writer.open("tmp.bin");*/
    vector<float> new_arr;
    new_arr.push_back(len);
    for (int i = 0; i < arr.size(); i++)
    {
        new_arr.push_back(arr[i]);
    }
    unsigned char *bytes = new unsigned char[(len + 1) * sizeof(float)];
    int c = 0;
    for (int i = 0; i < new_arr.size(); i++)
    {
        const unsigned char *byteVal = reinterpret_cast<const unsigned char *>(&new_arr[i]);
        for (int j = 0; j < sizeof(float); j++)
        {
            bytes[c] = byteVal[j];
            c++;
            /* if (arr.size() > 0)
             writer << byteVal[j];*/
        }
    }
    /*if (arr.size() > 0)
    writer.close();*/
    return bytes;
}

/// <summary>
/// Serialize list of boxes to json format
/// </summary>
/// <param name="bboxes"></param>
/// <returns></returns>
string serializeBoxes(vector<BBox> bboxes) {
    stringstream ss;
    ss << "[ ";
    for (int i = 0; i < bboxes.size(); i++)
    {
        ss << bboxes[i].toJson() << ", ";
    }
    ss << "]";
    return ss.str();
}


Mat bytesToMat(int width, int height, int channels, unsigned char *img_pointer)
{
    Mat img;
    if (channels == 4)
    {
        img = Mat(height, width, CV_8UC4, img_pointer);
        cvtColor(img, img, COLOR_BGRA2BGR);
    }
    if (channels == 3)
    {
        img = Mat(height, width, CV_8UC3, img_pointer);
    }

    if (channels == 1)
    {
        img = Mat(height, width, CV_8UC1, img_pointer);
    }
    return img;
}


unsigned char* stringToBytes(string text)
{
    unsigned char* bytes = new unsigned char[text.size()];
    std::memcpy(bytes, text.c_str(), text.size());
    return bytes;
}
unsigned char *matToBytes(Mat image)
{
    int size = image.rows * image.cols * image.channels();
    vector<float> header;
    header.push_back((float)image.rows);
    header.push_back((float)image.cols);
    header.push_back((float)image.channels());
    unsigned char *headerBytes = new unsigned char[4 * 3];
    int c = 0;
    for (int i = 0; i < 3; i++)
    {
        const unsigned char *byteVal = reinterpret_cast<const unsigned char *>(&header[i]);
        for (int j = 0; j < sizeof(float); j++)
        {
            headerBytes[c] = byteVal[j];
            c++;
        }
    }
    unsigned char *bytes = new unsigned char[size + 4 * 3];
    std::memcpy(bytes, headerBytes, 4 * 3);
    std::memcpy(&bytes[4 * 3], image.data, size);
    return bytes;
}

/** Match current image with template image
 *
 *  @param image Inspecting image
 *  @param rect Detected rectangle
 *  @param imageTemplate Template Image
 *  @param threshold Matching threshold
 *  @param expandRation Expand rect to find part in templatee Image
 *  @return Point indicates shifting between images
 *
 */
Point matchPcb(Mat image, Rect rect, Mat imageTemplate, float threshold, float expandRation)
{
    // expandRation =2, threshold = 0.88;
    Point pt(-1, -1);
    int centerX = rect.x + rect.width / 2;
    int centerY = rect.y + rect.height / 2;
    int w2 = rect.width * expandRation;
    int h2 = rect.height * expandRation;
    Rect rectExpanded(centerX - w2 / 2, centerY - h2 / 2, w2, h2);

    Mat partTemplateImage = image(rect);
    Mat lookupImg = imageTemplate(rectExpanded);
    /*cvtColor(subImg, subImg, COLOR_BGR2GRAY);
    cvtColor(lookupImg, lookupImg, COLOR_BGR2GRAY);*/
    Mat result;
    matchTemplate(partTemplateImage, lookupImg, result, TM_CCOEFF_NORMED);
    double minVal;
    double maxVal;
    Point minLoc;
    Point maxLoc;
    minMaxLoc(result, &minVal, &maxVal, &minLoc, &maxLoc, Mat());

    if (maxVal >= threshold)
    {
        pt.x = rectExpanded.x + maxLoc.x + rect.width / 2;
        pt.y = rectExpanded.y + maxLoc.y + rect.height / 2;
    }
    return pt;
}

/// <summary>
/// Conver Hex color to OpenCV Type(BGR)
/// </summary>
/// <param name="_hex"></param>
/// <returns></returns>
cv::Scalar hexToColor(const char* _hex) {
    cv::Scalar color(0, 0, 0);
    string hex(_hex);
    if (hex[0] == '#') hex = hex.substr(1, hex.size() - 1);
    int x;
    std::stringstream ss;
    ss << std::hex << hex;
    ss >> x;

    if (hex.size() == 6) {
        color[2] = x / (256 * 256);
        color[1] = int((x - color[2] * (256 * 256)) / 256);
        color[0] = x % 256;
    }
    return color;
}

/// <summary>
/// Draw Arrow
/// </summary>
/// <param name="result">Input/Output Image</param>
/// <param name="center">Origin point</param>
/// <param name="angle">angle</param>
/// <param name="len">arrow length</param>
/// <param name="color"></param>
/// <param name="thickness"></param>
void drawArrow(Mat& result, Point center, int angle, int len, Vec3b color, int thickness)
{
    if (angle >= 0)
    {
        double angleR = -angle * 3.1415f / 180.0f;
        int dy = (int)(len * sin(angleR));
        int dx = (int)(len * cos(angleR));
        Point p2 = Point(center.x + dx, center.y + dy);

        arrowedLine(result, center, p2, color, 4, 8, 0, 0.45f);
    }
}

/// <summary>
/// Get square rect given source rect  and image size
/// rect will be expanded with ratio = EXPAND
/// </summary>
/// <param name="input"></param>
/// <param name="size"></param>
/// <returns></returns>
Rect getSquareRect(Rect input, Size size)
{
    float EXPAND = 0.2f;
    int center_x = input.x + input.width / 2;
    int center_y = input.y + input.height / 2;
    int dim = max(input.width, input.height);
    dim += dim * EXPAND;
    int x = center_x - dim / 2;
    int y = center_y - dim / 2;
    x = max(x, 0);
    y = max(y, 0);
    if ((x + dim) > size.width)
    {
        x = size.width - dim - 1;
    }
    if ((y + dim) > size.height)
    {
        y = size.height - dim - 1;
    }
    return Rect(x, y, dim, dim);
}