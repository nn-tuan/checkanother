
#ifndef _MV_ERROR_DEFINE_H_
#define _MV_ERROR_DEFINE_H_

/********************************************************************/
///  \~chinese
///  \name 正确聛Eㄒ丒
///  @{
///  \~english
///  \name Definition of correct code
///  @{
#define MV_OK                   0x00000000  ///< \~chinese 成功，无代陙E            \~english Successed, no error
/// @}

/********************************************************************/
///  \~chinese
///  \name 通用代牦聛Eㄒ丒范围0x80000000-0x800000FF
///  @{
///  \~english
///  \name Definition of General error code
///  @{
#define MV_E_HANDLE             0x80000000  ///< \~chinese 代牦或无效的句眮E        \~english Error or invalid handle
#define MV_E_SUPPORT            0x80000001  ///< \~chinese 不支持的功能             \~english Not supported function
#define MV_E_BUFOVER            0x80000002  ///< \~chinese 缓存已聛E                \~english Buffer overflow
#define MV_E_CALLORDER          0x80000003  ///< \~chinese 函数调用顺序代陙E        \~english Function calling order error
#define MV_E_PARAMETER          0x80000004  ///< \~chinese 代牦的参数               \~english Incorrect parameter
#define MV_E_RESOURCE           0x80000006  ///< \~chinese 资源蓙E丒О�             \~english Applying resource failed
#define MV_E_NODATA             0x80000007  ///< \~chinese 无数据                   \~english No data
#define MV_E_PRECONDITION       0x80000008  ///< \~chinese 前置条件有误，或运行环境已发生变化       \~english Precondition error, or running environment changed
#define MV_E_VERSION            0x80000009  ///< \~chinese 版本不匹艁E              \~english Version mismatches
#define MV_E_NOENOUGH_BUF       0x8000000A  ///< \~chinese 传葋E哪诖婵占洳蛔丒      \~english Insufficient memory
#define MV_E_ABNORMAL_IMAGE     0x8000000B  ///< \~chinese 襾EＭ枷瘢赡苁嵌丒贾峦枷癫煌丒丒      \~english Abnormal image, maybe incomplete image because of lost packet
#define MV_E_LOAD_LIBRARY       0x8000000C  ///< \~chinese 动态导葋ELL失败          \~english Load library failed
#define MV_E_NOOUTBUF           0x8000000D  ///< \~chinese 没有可输出的缓磥E        \~english No Avaliable Buffer
#define MV_E_ENCRYPT            0x8000000E  ///< \~chinese 加密代陙E                \~english Encryption error
#define MV_E_UNKNOW             0x800000FF  ///< \~chinese 未知的代陙E              \~english Unknown error
/// @}

/********************************************************************/
///  \~chinese
///  \name GenICam系列代陙E范围0x80000100-0x800001FF
///  @{
///  \~english
///  \name GenICam Series Error Codes: Range from 0x80000100 to 0x800001FF
///  @{
#define MV_E_GC_GENERIC         0x80000100  ///< \~chinese 通用代陙E                \~english General error
#define MV_E_GC_ARGUMENT        0x80000101  ///< \~chinese 参数非法                 \~english Illegal parameters
#define MV_E_GC_RANGE           0x80000102  ///< \~chinese 值超出范围               \~english The value is out of range
#define MV_E_GC_PROPERTY        0x80000103  ///< \~chinese 属性                     \~english Property
#define MV_E_GC_RUNTIME         0x80000104  ///< \~chinese 运行环境有问虂E          \~english Running environment error
#define MV_E_GC_LOGICAL         0x80000105  ///< \~chinese 逻辑代陙E                \~english Logical error
#define MV_E_GC_ACCESS          0x80000106  ///< \~chinese 节点访问条件有蝸E        \~english Node accessing condition error
#define MV_E_GC_TIMEOUT         0x80000107  ///< \~chinese 超时                     \~english Timeout
#define MV_E_GC_DYNAMICCAST     0x80000108  ///< \~chinese 转换襾E�                 \~english Transformation exception
#define MV_E_GC_UNKNOW          0x800001FF  ///< \~chinese GenICam未知代陙E         \~english GenICam unknown error
/// @}

/********************************************************************/
///  \~chinese
///  \name GigE_STATUS对应的代牦聛E范围0x80000200-0x800002FF
///  @{
///  \~english
///  \name GigE_STATUS Error Codes: Range from 0x80000200 to 0x800002FF
///  @{
#define MV_E_NOT_IMPLEMENTED    0x80000200  ///< \~chinese 脕E槐簧璞钢С�         \~english The command is not supported by device
#define MV_E_INVALID_ADDRESS    0x80000201  ///< \~chinese 访问的目眮E刂凡淮嬖�     \~english The target address being accessed does not exist
#define MV_E_WRITE_PROTECT      0x80000202  ///< \~chinese 目眮E刂凡豢尚�           \~english The target address is not writable
#define MV_E_ACCESS_DENIED      0x80000203  ///< \~chinese 设备无访问权限           \~english No permission
#define MV_E_BUSY               0x80000204  ///< \~chinese 设备忙，或网络断开       \~english Device is busy, or network disconnected
#define MV_E_PACKET             0x80000205  ///< \~chinese 网络皝E荽丒          \~english Network data packet error
#define MV_E_NETER              0x80000206  ///< \~chinese 网络相关代陙E            \~english Network error
#define MV_E_IP_CONFLICT        0x80000221  ///< \~chinese 设备IP冲突               \~english Device IP conflict
/// @}

/********************************************************************/
///  \~chinese
///  \name USB_STATUS对应的代牦聛E范围0x80000300-0x800003FF
///  @{
///  \~english
///  \name USB_STATUS Error Codes: Range from 0x80000300 to 0x800003FF
///  @{
#define MV_E_USB_READ           0x80000300      ///< \~chinese 读usb出磥E           \~english Reading USB error
#define MV_E_USB_WRITE          0x80000301      ///< \~chinese 写usb出磥E           \~english Writing USB error
#define MV_E_USB_DEVICE         0x80000302      ///< \~chinese 设备襾E�             \~english Device exception
#define MV_E_USB_GENICAM        0x80000303      ///< \~chinese GenICam相关代陙E     \~english GenICam error
#define MV_E_USB_BANDWIDTH      0x80000304      ///< \~chinese 带窥位讈E 该代牦聛E略丒  \~english Insufficient bandwidth, this error code is newly added
#define MV_E_USB_DRIVER         0x80000305      ///< \~chinese 驱动不匹配或者未装驱动   \~english Driver mismatch or unmounted drive
#define MV_E_USB_UNKNOW         0x800003FF      ///< \~chinese USB未知的代陙E       \~english USB unknown error
/// @}

/********************************************************************/
///  \~chinese
///  \name 升级时对应的代牦聛E范围0x80000400-0x800004FF
///  @{
///  \~english
///  \name Upgrade Error Codes: Range from 0x80000400 to 0x800004FF
///  @{
#define MV_E_UPG_FILE_MISMATCH     0x80000400 ///< \~chinese 升级固件不匹艁E        \~english Firmware mismatches
#define MV_E_UPG_LANGUSGE_MISMATCH 0x80000401 ///< \~chinese 升级固件觼E圆黄ヅ丒    \~english Firmware language mismatches
#define MV_E_UPG_CONFLICT          0x80000402 ///< \~chinese 升级冲突（设备已经在升级了再次莵E笊都捶祷卮舜螅�   \~english Upgrading conflicted (repeated upgrading requests during device upgrade)
#define MV_E_UPG_INNER_ERR         0x80000403 ///< \~chinese 升级时设备内部出现代陙E    \~english Camera internal error during upgrade
#define MV_E_UPG_UNKNOW            0x800004FF ///< \~chinese 升级时未知代陙E        \~english Unknown error during upgrade
/// @}

#endif //_MV_ERROR_DEFINE_H_
