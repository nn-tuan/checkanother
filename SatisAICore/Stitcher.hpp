#include <opencv2/opencv.hpp>
#include <opencv2/xfeatures2d.hpp>
#include <calib3d/calib3d.hpp>
#include <stdio.h>
#include <string>

using namespace cv;
using namespace cv::xfeatures2d;
using namespace std;

class BoardStitcher
{
public:
    // Homography matrix, visualzie image
    Mat H, imgVis;
    void stitch(Mat imgTemplate, Mat imgCurrent, bool visualize)
    {
        Mat img1 = resizeImg(imgCurrent);
        Mat img2 = resizeImg(imgTemplate);
        float scale = imgCurrent.cols / (float)(img1.cols);
        int minHessian = 800;
        Ptr<SURF> detector = SURF::create(minHessian);
        std::vector<KeyPoint> keypoints1, keypoints2;
        Mat descriptors1, descriptors2;
        detector->detectAndCompute(img1, noArray(), keypoints1, descriptors1);
        detector->detectAndCompute(img2, noArray(), keypoints2, descriptors2);

        Ptr<DescriptorMatcher> matcher = DescriptorMatcher::create(DescriptorMatcher::FLANNBASED);
        std::vector<std::vector<DMatch>> knn_matches;
        matcher->knnMatch(descriptors1, descriptors2, knn_matches, 2);

        const float ratio_thresh = 0.8f;
        std::vector<DMatch> good_matches;
        for (size_t i = 0; i < knn_matches.size(); i++)
        {
            if (knn_matches[i][0].distance < ratio_thresh * knn_matches[i][1].distance)
            {
                good_matches.push_back(knn_matches[i][0]);
            }
        }

        std::vector<Point2f> obj;
        std::vector<Point2f> scene;
        for (unsigned i = 0; i < good_matches.size(); i++)
        {
            //-- Get the keypoints from the matches
            Point2f p1 = keypoints1[good_matches[i].queryIdx].pt;
            Point2f p1Src = Point2f(p1.x * scale, p1.y * scale);
            Point2f p2 = keypoints2[good_matches[i].trainIdx].pt;
            Point2f p2Src = Point2f(p2.x * scale, p2.y * scale);
            obj.push_back(p1Src);
            scene.push_back(p2Src);
        }

        if (visualize)
        {
            //-- Draw matches
            drawMatches(img1, keypoints1, img2, keypoints2, good_matches, imgVis, Scalar::all(-1),
                        Scalar::all(-1), std::vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);
        }

        H = findHomography(obj, scene, RANSAC);
    }

    Point transformCoordinate(Point pt)
    {
        std::vector<Point2f> obj_corners;
        std::vector<Point2f> scene_corners;
        obj_corners.push_back(Point2f(pt.x, pt.y));
        perspectiveTransform(obj_corners, scene_corners, H);
        return Point(scene_corners[0].x, scene_corners[0].y);
    }

    Point transformCoordinateInv(Point pt)
    {
        std::vector<Point2f> obj_corners;
        std::vector<Point2f> scene_corners;
        obj_corners.push_back(Point2f(pt.x, pt.y));
        perspectiveTransform(obj_corners, scene_corners, H.inv());
        return Point(scene_corners[0].x, scene_corners[0].y);
    }
    vector<Point> transformCoordinate(vector<Point> pts)
    {
        std::vector<Point> outPts;
        std::vector<Point2f> obj_corners;
        std::vector<Point2f> scene_corners;
        for (int i = 0; i < pts.size(); i++)
        {
            obj_corners.push_back(Point2f(pts[i].x, pts[i].y));
        }
        perspectiveTransform(obj_corners, scene_corners, H);
        for (int i = 0; i < pts.size(); i++)
        {
            outPts.push_back(Point(scene_corners[i].x, scene_corners[i].y));
        }
        return outPts;
    }

private:
    Mat resizeMax(Mat img, int maxWidth)
    {
        float scale = (float)maxWidth / img.cols;
        if (scale > 1)
            return img;
        Size size((int)img.cols * scale, (int)img.rows * scale);
        Mat img2;
        resize(img, img2, size);
        return img2;
    }

    Mat resizeImg(Mat img)
    {
        return resizeMax(img, 1000);
    }
};