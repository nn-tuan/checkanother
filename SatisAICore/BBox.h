#include <stdio.h>
#include <string>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

class BBox {
public:
    float x = -1, y = -1, w = -1, h = -1,  centerX = -1, centerY = -1, xTemplate = -1, yTemplate = -1;
    int rotation = -1;
    float score = 0;
    string label = "";

    BBox(string _label, float _score, int _x, int _y, int _x2, int _y2) {
        x = _x;
        y = _y;
        w = _x2 - _x;
        h = _y2 - _y;
        label = _label;
        score = _score;
        centerX = x + w / 2;
        centerY = y + h / 2;

        compareVal = centerX + centerY * 5;
    }

    BBox(string _label, float _score, float _x, float _y, float _x2, float _y2) {
        x = _x;
        y = _y;
        w = _x2 - _x;
        h = _y2 - _y;
        label = _label;
        score = _score;
        centerX = x + w / 2;
        centerY = y + h / 2;
        compareVal = centerX + centerY * 5;
    }

    /// <summary>
    /// Serialize Bounding Box
    /// </summary>
    /// <returns></returns>
    string toJson() {
        stringstream ss;
        ss << "{"
            << "\"x\":" << (int)x << " , "
            << "\"y\":" << (int)y << " , "
            << "\"xTemplate\":" << (int)xTemplate << " , "
            << "\"yTemplate\":" << (int)yTemplate << " , "
            << "\"w\":" << (int)w << " , "
            << "\"h\":" << (int)h << " , "
            << "\"label\":\"" << label << "\" , "
            << "\"rotation\":" << rotation << " , "
            << "\"score\":" << score << "}";
        return ss.str();
    }

    Point getCenter() {
        return Point(centerX, centerY);
    }
    Rect getBox() {
        return Rect(x, y, w, h);
    }
    bool operator< (const BBox& other) const {
        return compareVal < other.compareVal;
    }
    bool operator> (const BBox& other) const {
        return compareVal > other.compareVal;
    }
private:
    int compareVal = -1;
};
