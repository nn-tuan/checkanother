#include <iostream>
#include <windows.h>
#include <string>    
#include <opencv2/opencv.hpp>
#include "function.h"

//#ifdef _DEBUG
//#pragma comment(lib, "opencv_world455d.lib")
//#else
//#pragma comment(lib, "opencv_world455.lib")
//#endif

using namespace std;
using namespace cv;

class OmronCameraDriver {
private:
    Function tagFc;
    HANDLE hCam = NULL;
    /*
    * byColorInterpolationMethod：色補間方法
    * 0：色補間処理を行わずモノクロ表示する
    * 1：色補間処理を行わずカラー表示する
    * 2：最も近い画素の値をそのまま使用して補間を行う。
    * 3：周辺の4画素を用いて補間を行う。
    * 4：周辺の16画素を用いて補間を行う。
    * 5：周辺の4画素を用いて補間を行う。3と比較し、エッジ部における偽色の発生が抑制される。
    */
    BYTE  byColorInterpolationMethod = 4;
    PBYTE pBGRFrameBuf;
    PBYTE pRawFrameBuf;
    DWORD dwDataSize = NULL;
    DWORD dwError = 0;
    DWORD  dwNumberOfByteTrans = NULL;
    DWORD  dwFrameNo = NULL;
    DWORD  dwMaxWidth = NULL;
    DWORD  dwMaxHeight = NULL;
    /*
    * dwPixelFormat：プレビューピクセルフォーマット
    * 0x00000001：1画素を8ビットで表します。モノクロカメラでのみ設定可能です。
    * 0x00000004：1画素を24ビットで表します。画像の左上から青緑赤の順にバッファへ格納されます。
    * 0x00000008：1画素を32ビットで表します。画像の左上から青緑赤Xの順にバッファへ格納されます。
    */
    DWORD  dwPreview_PixelFormat = 0x00000004;



    bool bOpened = false;
public:

    Mat getFrame() {
        pRawFrameBuf = (BYTE*)malloc(dwDataSize);
        pBGRFrameBuf = (BYTE*)malloc(dwDataSize * 3);

        if (!tagFc.Cam_TakeRawSnapShot(hCam, pRawFrameBuf, dwDataSize, &dwNumberOfByteTrans, &dwFrameNo, 1000/*[ms]*/)) {
            tagFc.Cam_GetLastError(hCam);
            printf("Cam_TakeRawSnapShot error\n");
        }

        if (!tagFc.Cam_GetMaximumImageSize(hCam, &dwMaxWidth, &dwMaxHeight)) {
            tagFc.Cam_GetLastError(hCam);
            printf("Cam_GetMaximumImageSize error\n");
        }

        if (!tagFc.Cam_ConvertRawToBGR(hCam, dwMaxWidth, dwMaxHeight, pRawFrameBuf, pBGRFrameBuf, byColorInterpolationMethod, dwPreview_PixelFormat)) {
            tagFc.Cam_GetLastError(hCam);
            printf("Cam_ConvertRawToBGR error\n");
        }

        Mat Frame = Mat(dwMaxHeight, dwMaxWidth, CV_8UC3, pBGRFrameBuf);
        Mat CloneFrame = Frame.clone();

        free(pRawFrameBuf);
        free(pBGRFrameBuf);

        return CloneFrame;
    }

    bool setup(int cameraId) {
        if (!StartAcquisition()) {
            return bOpened = false;
        }
        return bOpened = true;
    }

    bool isOpened() {
        return bOpened;
    }

    void release() {
        StopAcquisition();
    }

    bool StartAcquisition() {
        if (NULL == g_hModule) {		//hModule functionのグローバル変数
            printf("loadDLL error\n");
            return  false;
        }

        hCam = tagFc.Cam_Open(0);
        if (NULL == hCam) {
            printf("Cam_Open error\n");
            return false;
        }

        if (!tagFc.Cam_StartTransfer(hCam)) {
            tagFc.Cam_GetLastError(hCam);
            printf("Cam_StartTransfer error\n");
            return  false;
        }

        if (!tagFc.Cam_GetRawDataSize(hCam, &dwDataSize)) {
            tagFc.Cam_GetLastError(hCam);
            printf("Cam_GetRawDataSize error\n");
            return  false;
        }
        return  true;
    }

    void StopAcquisition() {
        if (!tagFc.Cam_StopTransfer(hCam)) {
            tagFc.Cam_GetLastError(hCam);
            printf("Cam_GetRawDataSize error\n");
        }
        tagFc.Cam_Close(hCam);
        CloseDLL();
    }


};