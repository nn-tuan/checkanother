// Change this to true to use cam Shoden/ ShodenCS
#define USE_CAM_SHODEN false
#define USE_CAM_SHODEN_CS true

#define FRAME_QUEUE_SIZE 100

#ifdef SATISAI_EXPORTS
#define SATISAI_API __declspec(dllexport)
#else
#define SATISAI_API __declspec(dllimport)
#endif

#include <fstream>
#include <opencv2/opencv.hpp>
#include <stdio.h>
#include <string>
#include <torch/script.h> // One-stop header.
#include <iostream>
#include <vector>
#include <memory>
#include <ctime>
#include <math.h>
#include "utils.hpp"
#include "Stitcher.hpp"
#include <Windows.h>
#ifndef __BBOX_H__
#define __BBOX_H__
#include "BBox.h"
#endif

#include "CvCameraDriver.hpp"
#include "FlirCameraDriver.hpp"
#if USE_CAM_SHODEN
#include "ShodenshaCameraDriver.hpp"
#endif
#if USE_CAM_SHODEN_CS
#include "ShodenshaCSCameraDriver.hpp"
#endif
#include "YoloV4Detector.hpp"
#include "YoloV5Detector.hpp"
#include "ResnetAngleDetector.hpp"
#include <torch/script.h>
#include <torch/cuda.h>

using namespace cv;
using namespace std;

class FrameData
{
public:
    vector<BBox> detections;
    int frameId;
    Mat img, vis;
    bool inspected = false;
    bool detected = false;
    FrameData()
    {
    }

    FrameData(Mat _img, int _frameId)
    {
        img = _img;
        frameId = _frameId;
        inspected = false;
        detected = false;
    }
    /// <summary>
    /// Clone current image to visualize
    /// </summary>
    void initVisualize()
    {
        if (vis.empty())
        {
            vis = img.clone();
        }
    }
};

///// <summary>
///// Class stores part's information (defined in .saiplace file)
///// </summary>
// class PcbPartInfo
//{
// public:
//     string id = "";
//     string label = "";
//     string recordPath = "";
//     float x = 0, y = 0;
//     float angle = 0;
//     float distanceTolerance = 0;
//     float rotationTolerance = 0;
// };

class CSatisAI
{
public:
    YoloV5Detector detector;
    vector<ResnetAngleDetector> angleDetector;
    BoardStitcher stitcher;

    CSatisAI();
    bool setCam(const char *cameraType, int index, const char *cameraPath);
    void setLabels(string labelsStr);
    void loadPartsModel(const char *path, int device_id);
    void loadAngleModel(const char *path, const char *partName, int deviceId);
    vector<BBox> detect(int frameId, float threshold, float nms);
    bool loadTemplate(const char *path);
    vector<BBox> stitchBox(int frameId);
    int detectRotation(int frameId, int boxId, float conf);
    int updateFrame();
    FrameData getFrame(int id);
    void setFrame(FrameData frame);
    bool startRecord(const char *filePath);
    bool stopRecord();

private:
    string cameraType;
    FlirCameraDriver camFlir;
    CvCameraDriver camCv;
#if USE_CAM_SHODEN
    ShodenshaCameraDriver camShoden;
#endif
#if USE_CAM_SHODEN_CS
    ShodenshaCSCameraDriver camShodenCS;
#endif
    int w = 0, h = 0;          // frame width, frame height
    vector<cv::Scalar> colors; // colors for visualization
    vector<FrameData> frames;
    vector<string> labels;
    int frameCounter = 0;

    VideoWriter recorder;
    bool recording = false; // recording mode

    vector<string> modelNames; // rotation model names

    Mat imageTemplate; // taken from saiplace file, use for stitching
    Mat H;             // homography matrix
};

class ImageData
{
public:
    Mat mat;
    ImageData() {}
};

//// Exported functions
//
extern "C" SATISAI_API int GetAppCode() {
    return 1;
}
extern "C" SATISAI_API void FreeBuffer(unsigned char *myBuffer);
extern "C" SATISAI_API void FreeImageDataBuffer(ImageData * myBuffer);
extern "C" SATISAI_API CSatisAI *CreateClass();
extern "C" SATISAI_API void RemoveClass(CSatisAI *pObject);
extern "C" SATISAI_API bool CheckGPU();
extern "C" SATISAI_API void ResizeImage(ImageData *image, int w, int h);
extern "C" SATISAI_API ImageData *GetFrame(CSatisAI *pObject, int frameId);
extern "C" SATISAI_API int GetWidth(ImageData *image);
extern "C" SATISAI_API int GetHeight(ImageData *image);
extern "C" SATISAI_API void DrawLine(ImageData *image, int x0, int y0, int x1, int y1, const char *color, int lineWidth);
extern "C" SATISAI_API void DrawRectangle(ImageData *image, int x0, int y0, int x1, int y1, const char *color, int lineWidth);
extern "C" SATISAI_API void DrawArrow(ImageData *image, int x, int y, int angle, int len, const char *color, int thickness);
extern "C" SATISAI_API void PutText(ImageData *image, const char *label, int x, int y, const char *color, float fontScale, int thickness);
extern "C" SATISAI_API unsigned char *MatToBytes(ImageData *image);
extern "C" SATISAI_API bool SetCam(CSatisAI *pObject, const char *cameraType, int index, const char *cameraPath);
extern "C" SATISAI_API void SetLabels(CSatisAI *pObject, const char *labelsStr);
extern "C" SATISAI_API void LoadPartsModel(CSatisAI *pObject, const char *path, int device_id);
extern "C" SATISAI_API void LoadAngleModel(CSatisAI *pObject, const char *path, const char *partName, int device_id);
extern "C" SATISAI_API unsigned char *BenchmarkPartsModel(CSatisAI *pObject);
extern "C" SATISAI_API unsigned char *BenchmarkAngleModel(CSatisAI *pObject);
extern "C" SATISAI_API unsigned char *Detect(CSatisAI *pObject, int frameId, float threshold, float nms);
extern "C" SATISAI_API bool LoadTemplate(CSatisAI *pObject, const char *path);
extern "C" SATISAI_API unsigned char *StitchBox(CSatisAI *pObject, int frameId);
extern "C" SATISAI_API unsigned char *MapPointFromStitch(CSatisAI *pObject, int x, int y);
extern "C" SATISAI_API int DetectRotation(CSatisAI *pObject, int frameId, int boxId, float conf);
extern "C" SATISAI_API int UpdateFrame(CSatisAI *pObject);
extern "C" SATISAI_API bool SaveFrame(ImageData *image, const char *filePath);
extern "C" SATISAI_API bool SavePart(ImageData *image, int x, int y, int w, int h, const char *filePath);
extern "C" SATISAI_API bool StartRecord(CSatisAI *pObject, const char *filePath);
extern "C" SATISAI_API bool StopRecord(CSatisAI *pObject);