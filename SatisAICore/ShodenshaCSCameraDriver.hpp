#include <stdio.h>
#include <Windows.h>
#include <conio.h>
#include "MvCameraControl.h"
#include <opencv2/opencv.hpp>

//#ifdef _DEBUG
//#pragma comment(lib, "opencv_world455d.lib")
//#else
//#pragma comment(lib, "opencv_world455.lib")
//#endif

using namespace cv;
using namespace std;

class ShodenshaCSCameraDriver 
{
private:
    bool g_bOpened = false;

	int g_nDiscrimination = -1;
	void* g_m_Handle = NULL;
	unsigned char* g_pFrameBuf = NULL;
	int g_nBufSize = NULL; 
	MV_FRAME_OUT_INFO_EX g_FrameOutInfo;
    unsigned char* g_pImage = NULL;
public:

    Mat getFrame(){
		g_nDiscrimination 
			= MV_CC_GetOneFrameTimeout(g_m_Handle, g_pFrameBuf, g_nBufSize, &g_FrameOutInfo, 1000);
		
		MV_CC_PIXEL_CONVERT_PARAM ConvertedFrameToMat
			= ConvertPixelTypeToRGB8(g_m_Handle, g_pFrameBuf, g_nBufSize, g_FrameOutInfo);
		
		Mat Frame = Mat(g_FrameOutInfo.nHeight, g_FrameOutInfo.nWidth, CV_8UC3, ConvertedFrameToMat.pDstBuffer);
        Mat CloneFrame = Frame.clone(); //Memory leak measures

        MV_FRAME_OUT FrameOut;
        FrameOut.pBufAddr = g_pFrameBuf;
        FrameOut.stFrameInfo = g_FrameOutInfo;
        int nRet=MV_CC_FreeImageBuffer(g_m_Handle, &FrameOut);


		return CloneFrame;
    }
    bool setup(int cameraId){
        if (!StartAcquisition()) {
            g_bOpened = false;
            return g_bOpened;
        }
        g_bOpened = true;
        return g_bOpened;
    }
    bool isOpened(){
        return g_bOpened;
    }
    void release(){
		StopAcquisition();
    }


    bool StartAcquisition() {
		g_bOpened = true;
        if (g_pFrameBuf != NULL) {
            free(g_pFrameBuf);
            g_pFrameBuf = NULL;
        }
        if (g_pImage != NULL) {
            free(g_pImage);
            g_pImage = NULL;
        }

		//Enumerate all devices of specific transport protocol in the subnet
		unsigned int nTLayerType = MV_GIGE_DEVICE | MV_USB_DEVICE;
		MV_CC_DEVICE_INFO_LIST m_stDevList = { 0 };
		g_nDiscrimination = MV_CC_EnumDevices(nTLayerType, &m_stDevList);
		if (g_nDiscrimination != MV_OK)
		{
			printf("error: EnumDevices fail [%x]\n", g_nDiscrimination);
			return false;
		}
		int i = 0;
		if (m_stDevList.nDeviceNum == 0)
		{
			printf("no camera found!\n");
			return false;
		}

		//Select the first found online device and create handle
		MV_CC_DEVICE_INFO m_stDevInfo = { 0 };
		memcpy(&m_stDevInfo, m_stDevList.pDeviceInfo[0], sizeof(MV_CC_DEVICE_INFO));
		g_nDiscrimination = MV_CC_CreateHandle(&g_m_Handle, &m_stDevInfo);
		if (g_nDiscrimination != MV_OK)
		{
			printf("error: CreateHandle fail [%x]\n", g_nDiscrimination);
			return false;
		}
		//Connect to device
		unsigned int nAccessMode = MV_ACCESS_Exclusive;
		g_nDiscrimination = MV_CC_OpenDevice(g_m_Handle, nAccessMode, 0);
		if (g_nDiscrimination != MV_OK)
		{
			printf("error: OpenDevice fail [%x]\n", g_nDiscrimination);
			return false;
		}
		
		//Start image acquisition
		g_nDiscrimination = MV_CC_StartGrabbing(g_m_Handle);
		if (g_nDiscrimination != MV_OK)
		{
			printf("error: StartGrabbing fail [%x]\n", g_nDiscrimination);
			return false;
		}

		//Get the size of one data frame
		MVCC_INTVALUE stIntvalue = { 0 };
		g_nDiscrimination = MV_CC_GetIntValue(g_m_Handle, "PayloadSize", &stIntvalue);
		if (g_nDiscrimination != MV_OK)
		{
			printf("Get PayloadSize failed! nRet [%x]\n", g_nDiscrimination);
			return false;
		}
		unsigned int nTestFrameSize = 0;
		g_nBufSize = stIntvalue.nCurValue + 2048;	//
        if (g_pFrameBuf != NULL) {
            free(g_pFrameBuf);
            g_pFrameBuf = NULL;
        }
		g_pFrameBuf = (unsigned char*)malloc(g_nBufSize);
		memset(&g_FrameOutInfo, 0, sizeof(MV_FRAME_OUT_INFO_EX));
    }
	void StopAcquisition() {
        if (g_pFrameBuf != NULL) {
            free(g_pFrameBuf);
            g_pFrameBuf = NULL;
        }
        if (g_pImage != NULL) {
            free(g_pImage);
            g_pImage = NULL;
        }
		
		//Stop image acquisition
		g_nDiscrimination = MV_CC_StopGrabbing(g_m_Handle);
		if (g_nDiscrimination != MV_OK)
		{
			printf("error: StopGrabbing fail [%x]\n", g_nDiscrimination);
			return;
		}
		//Close camera
		g_nDiscrimination = MV_CC_CloseDevice(g_m_Handle);
		if (g_nDiscrimination != MV_OK)
		{
			printf("error: CloseDevice fail [%x]\n", g_nDiscrimination);
			return;
		}
		//Destroy handle and release resources
		g_nDiscrimination = MV_CC_DestroyHandle(g_m_Handle);
		if (g_nDiscrimination != MV_OK)
		{
			printf("error: DestroyHandle fail [%x]\n", g_nDiscrimination);
			return;
		}
	}
	MV_CC_PIXEL_CONVERT_PARAM ConvertPixelTypeToRGB8(void* handle, unsigned char* FrameBuf, int BufSize, MV_FRAME_OUT_INFO_EX OneframeInfo) {
		MV_CC_PIXEL_CONVERT_PARAM ConvertParam;
		memset(&ConvertParam, 0, sizeof(MV_CC_PIXEL_CONVERT_PARAM));
		ConvertParam.pSrcData = FrameBuf;
		ConvertParam.nSrcDataLen = OneframeInfo.nFrameLen;
		ConvertParam.enSrcPixelType = OneframeInfo.enPixelType;
		ConvertParam.nWidth = OneframeInfo.nWidth;
		ConvertParam.nHeight = OneframeInfo.nHeight;
		//Note: When PixelType_Gvsp_BGR8_Packed is specified, RGB is output.
		ConvertParam.enDstPixelType = PixelType_Gvsp_BGR8_Packed;
		int RGBnBufSize = g_nBufSize * 3;
		ConvertParam.nDstBufferSize = RGBnBufSize;
        if (g_pImage != NULL) {
            free(g_pImage);
            g_pImage = NULL;
        }
        g_pImage = (unsigned char*)malloc(RGBnBufSize);
		ConvertParam.pDstBuffer = g_pImage;
		g_nDiscrimination = MV_CC_ConvertPixelType(g_m_Handle, &ConvertParam);
		if (g_nDiscrimination != MV_OK)
		{
			printf("MV_CC_ConvertPixelType failed");
		}
		return ConvertParam;
	}

	
};