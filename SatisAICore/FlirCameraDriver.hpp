#include "Spinnaker.h"
#include "SpinGenApi/SpinnakerGenApi.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <opencv2/opencv.hpp>
#include <windows.h>

using namespace Spinnaker;
using namespace Spinnaker::GenApi;
using namespace Spinnaker::GenICam;
using namespace std;
using namespace cv;

class FlirCameraDriver
{

public:
	// Print Camera Device Info
	int PrintDeviceInfo(INodeMap &nodeMap, std::string camSerial)
	{
		int result = 0;

		cout << "[" << camSerial << "] Printing device information ..." << endl
			 << endl;

		FeatureList_t features;
		CCategoryPtr category = nodeMap.GetNode("DeviceInformation");
		if (IsAvailable(category) && IsReadable(category))
		{
			category->GetFeatures(features);

			FeatureList_t::const_iterator it;
			for (it = features.begin(); it != features.end(); ++it)
			{
				CNodePtr pfeatureNode = *it;
				CValuePtr pValue = (CValuePtr)pfeatureNode;
				cout << "[" << camSerial << "] " << pfeatureNode->GetName() << " : "
					 << (IsReadable(pValue) ? pValue->ToString() : "Node not readable") << endl;
			}
		}
		else
		{
			cout << "[" << camSerial << "] "
				 << "Device control information not available." << endl;
		}

		cout << endl;

		return result;
	}

	Spinnaker::ImagePtr spinnakerImagePtrToColor(const Spinnaker::ImagePtr &imagePtr)
	{
		return imagePtr->Convert(Spinnaker::PixelFormat_BGR8, Spinnaker::IPP);
	}

	// This function converts between Spinnaker::ImagePtr container to cv::Mat container used in OpenCV.
	cv::Mat spinnakerWrapperToCvMat(const Spinnaker::ImagePtr &src)
	{
		try
		{
			Spinnaker::ImagePtr imagePtr = spinnakerImagePtrToColor(src);
			const auto XPadding = imagePtr->GetXPadding();
			const auto YPadding = imagePtr->GetYPadding();
			const auto rowsize = imagePtr->GetWidth();
			const auto colsize = imagePtr->GetHeight();

			// Image data contains padding. When allocating cv::Mat container size, you need to account for the X,Y
			// image data padding.
			Mat image = cv::Mat((int)(colsize + YPadding), (int)(rowsize + XPadding), CV_8UC3, imagePtr->GetData(),
								imagePtr->GetStride())
							.clone();
			return image;
		}
		catch (const std::exception &e)
		{
			cout << (e.what(), __LINE__, __FUNCTION__, __FILE__);
			return cv::Mat();
		}
	}

	string getLibraryVersion()
	{
		const LibraryVersion spinnakerLibraryVersion = system->GetLibraryVersion();
		stringstream ss;
		ss << spinnakerLibraryVersion.major << "." << spinnakerLibraryVersion.minor
		   << "." << spinnakerLibraryVersion.type << "." << spinnakerLibraryVersion.build << endl
		   << endl;
		return ss.str();
	}
	bool StartAcquisition(CameraPtr pCam)
	{
		INodeMap &nodeMapTLDevice = pCam->GetTLDeviceNodeMap();
		CStringPtr ptrStringSerial = pCam->GetTLDeviceNodeMap().GetNode("DeviceSerialNumber");
		std::string serialNumber = "";
		if (IsAvailable(ptrStringSerial) && IsReadable(ptrStringSerial))
		{
			serialNumber = ptrStringSerial->GetValue();
		}
		cout << endl
			 << "[" << serialNumber << "] "
			 << "*** IMAGE ACQUISITION THREAD STARTING"
			 << " ***" << endl
			 << endl;

		PrintDeviceInfo(nodeMapTLDevice, serialNumber);

		pCam->Init();
		CEnumerationPtr ptrAcquisitionMode = pCam->GetNodeMap().GetNode("AcquisitionMode");
		if (!IsAvailable(ptrAcquisitionMode) || !IsWritable(ptrAcquisitionMode))
		{
			cout << "Unable to set acquisition mode to continuous (node retrieval; camera " << serialNumber
				 << "). Aborting..." << endl
				 << endl;
			return false;
		}

		CEnumEntryPtr ptrAcquisitionModeContinuous = ptrAcquisitionMode->GetEntryByName("Continuous");
		if (!IsAvailable(ptrAcquisitionModeContinuous) || !IsReadable(ptrAcquisitionModeContinuous))
		{
			cout << "Unable to set acquisition mode to continuous (entry 'continuous' retrieval " << serialNumber
				 << "). Aborting..." << endl
				 << endl;
			return false;
		}

		int64_t acquisitionModeContinuous = ptrAcquisitionModeContinuous->GetValue();

		ptrAcquisitionMode->SetIntValue(acquisitionModeContinuous);

		cout << "[" << serialNumber << "] "
			 << "Acquisition mode set to continuous..." << endl;

		// Begin acquiring images
		pCam->BeginAcquisition();

		cout << "[" << serialNumber << "] "
			 << "Started acquiring images..." << endl;
		return true;
	}
	void StopAcquisition(CameraPtr pCam)
	{
		// End acquisition
		pCam->EndAcquisition();
		// Deinitialize camera
		pCam->DeInit();
	}

	SystemPtr system;
	CameraList camList;
	string libraryVersion;
	CameraPtr camera;
	bool opened = false;
	Mat frame;
	Mat getFrame()
	{
		ImagePtr pResultImage = camera->GetNextImage(1000);
		if (pResultImage->IsIncomplete())
		{
			cout << "Image incomplete with image status " << pResultImage->GetImageStatus() << "..." << endl;
		}
		else
		{
			frame = spinnakerWrapperToCvMat(pResultImage).clone();
		}
		pResultImage->Release();
		return frame;
	}
	bool setup(int cameraId)
	{
		system = System::GetInstance();
		libraryVersion = getLibraryVersion();
		camList = system->GetCameras();

		unsigned int camListSize = 0;
		camListSize = camList.GetSize();
		if (camListSize == 0)
		{
			// Clear camera list before releasing system
			camList.Clear();

			// Release system
			system->ReleaseInstance();
			return false;
		}
		else
		{
			camera = camList.GetByIndex(cameraId);

			INodeMap &nodeMapTLDevice = camera->GetTLDeviceNodeMap();

			if (!StartAcquisition(camera))
				opened = false;
			else
				opened = true;
			return opened;
		}
	}
	bool isOpened()
	{
		return opened;
	}
	void release()
	{
		StopAcquisition(camera);
	}
};
