#include "NET_ICube_API.h"
#include <stdio.h>
#include <tchar.h>
#include  <conio.h>
#include <SDKDDKVer.h>
#include <opencv2/opencv.hpp>

//#ifdef _DEBUG
//#pragma comment(lib, "opencv_world455d.lib")
//#else
//#pragma comment(lib, "opencv_world455.lib")
//#endif

using namespace std;
using namespace cv;

//---------------------------------------------------------------------------
// DEFINES
// start with software trigger mode
#define MODE_SW_TRIGGER		

int nGoodFramesCnt = { 0 };		// statistics
int nBadFramesCnt = { 0 };		// statistics

Mat latestFrame;
HANDLE  m_hndEvent = { NULL };
long CALLBACK MyCallbackFuncEx(int event_type, BYTE* pBuffer, long lBufferSize, PVOID pContext)
{
    int cameraInd = reinterpret_cast<int>(pContext);	// retrieve camera index (set as callback context parameter)
    switch (event_type)
    {
    case EVENT_NEW_FRAME:

        if (lBufferSize == 0)
            nBadFramesCnt++;	// a bad frame arrived
        else
            nGoodFramesCnt++;	// a good frame arrived

        printf("\nCam[%d] Image Ev cb;  GoodFr: %d ,BadFr: %d , Size %d  ", cameraInd, nGoodFramesCnt, nBadFramesCnt, lBufferSize);
#ifdef MODE_SW_TRIGGER
        printf("\nWelcom_MODE_SW_TRIGGER!!");
        latestFrame = Mat(1200, 1600, CV_8UC3, pBuffer);
        SetEvent(m_hndEvent);	// signal that a frame is arrived
#endif
        break;
    case EVENT_DEV_DISCONNECTED:
        // lBufferSize returns the disconnection counter value
        printf("\nCam[%d] EVENT_DEV_DISCONNECTED  %d", cameraInd, lBufferSize);
        break;
    case EVENT_DEV_RECONNECTED:
        // lBufferSize returns the reconnection counter value
        printf("\nCam[%d] EVENT_DEV_RECONNECTED   %d ", cameraInd, lBufferSize);
        break;
    case EVENT_USB_TRANSFER_FAILED:
        // lBufferSize returns the transfer failed counter value
        printf("\nCam[%d] EVENT_USB_TRANSFER_FAILED  %d ", cameraInd, lBufferSize);
        break;
    default:
        // just in case
        printf("\nCam[%d] EVENT_UNKNOWN  %d ", cameraInd, event_type);
        break;
    }

    return 42;
}

long CALLBACK MyCallbackFunc(BYTE* pBuffer, long lBufferSize, PVOID pContext)
{
    int cameraInd = reinterpret_cast<int>(pContext);	// retrieve camera index (set as callback context parameter)

    if (lBufferSize == 0)
        nBadFramesCnt++;	// a bad frame arrived
    else
        nGoodFramesCnt++;	// a good frame arrived

    printf("\nCam[%d] Image cb;  GoodFr: %d ,BadFr: %d , Size %d  ", cameraInd, nGoodFramesCnt, nBadFramesCnt, lBufferSize);
#ifdef MODE_SW_TRIGGER
    SetEvent(m_hndEvent);	// signal that a frame is arrived
#endif

    return 42;
}


class ShodenshaCameraDriver {
private:
    Mat FrameCopy;
    int cameraID;
    int stat = 0;
    bool opened = true;
public:
    bool StartAcquisition(int cameraId) {
        cameraID = cameraId;
        opened = true;

        m_hndEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
        if (m_hndEvent == NULL) {
            printf("Error: CreateEvent \n");
            return false;
        }

        stat = ICubeSDK_Open(cameraID);
        if (stat != IC_SUCCESS) {
            PrintError("ICubeSDK_Open", stat, cameraID);
            return false;
        }

        // check camera name
        char CamName[NETCAM_NAME_LENGTH];
        stat = ICubeSDK_GetName(cameraID, CamName);
        if (stat != IC_SUCCESS) {
            PrintError("ICubeSDK_GetName", stat, cameraID);
            return false;
        }
        printf("Camera[%d]  model: %s \n", cameraID, CamName);

        // register callback function 
        int* context = (int*)(cameraID);  // set camera index as context parameter for callback	
        if (ICubeSDK_SetCallbackEx != NULL)
            stat = ICubeSDK_SetCallbackEx(cameraID, CALLBACK_RGB, MyCallbackFuncEx, context);
        else {
            printf("This ICubeSDK.dll does not support a event callback \n");
            stat = ICubeSDK_SetCallback(cameraID, CALLBACK_RGB, MyCallbackFunc, context);
        }
        if (stat != IC_SUCCESS) {
            PrintError("ICubeSDK_SetCallback", stat, cameraID);
            return false;
        }

        stat = ICubeSDK_SetCamParameter(cameraID, REG_SW_TRIG_MODE, IMMEDIATE_TRIGGER_RETURN);
        if (stat != IC_SUCCESS) {
            PrintError("NETUSBCAM_SetCamParameter: REG_SW_TRIG_MODE", stat, cameraID);
            return false;
        }

        // badframes can be detected through the callback
        stat = ICubeSDK_SetCamParameter(cameraID, REG_CALLBACK_BR_FRAMES, 1);
        if (stat != IC_SUCCESS) {
            PrintError("NETUSBCAM_SetCamParameter: REG_CALLBACK_BR_FRAMES", stat, cameraID);
            return false;
        }

#define TRIGGER_TIMEOUT_VALUE 2000	// [msec]
        stat = ICubeSDK_SetCamParameter(cameraID, REG_TRIG_TIMEOUT, TRIGGER_TIMEOUT_VALUE);
        if (stat != IC_SUCCESS) {
            PrintError("NETUSBCAM_SetCamParameter: REG_TRIG_TIMEOUT", stat, cameraID);
            return false;
        }
        // check camera name
        stat = ICubeSDK_SetTrigger(cameraID, TRIG_SW_START);
        if (stat != IC_SUCCESS) {
            PrintError("ICubeSDK_SetTrigger: TRIG_SW_START", stat, cameraID);
            return false;
        }

        // start frame transfer without preview
        stat = ICubeSDK_Start(cameraID, NULL, false, true);
        if (stat != IC_SUCCESS) {
            PrintError("ICubeSDK_Start", stat, cameraID);
            return false;
        }
        return true;
    }
    void StopAcquisition(int cameraInd) {
        stat = ICubeSDK_Stop(cameraInd);
        if (stat != IC_SUCCESS) {
            PrintError("ICubeSDK_Stop ", stat, cameraInd);
        }
        stat = ICubeSDK_Close(cameraInd);
        if (stat != IC_SUCCESS) {
            PrintError("ICubeSDK_Close ", stat, cameraInd);
        }
    }
    int getcameraId() {
        return cameraID;
    }


    Mat getFrame() {
        stat = ICubeSDK_SetTrigger(cameraID, TRIG_SW_DO);
        if (stat != IC_SUCCESS) {
            PrintError("TRIG_SW_DO ", stat, cameraID);
        }
        // wait for frame (signaled from callback function)
        if (WaitForSingleObject(m_hndEvent, TRIGGER_TIMEOUT_VALUE) != WAIT_TIMEOUT)
        {
            FrameCopy = latestFrame.clone();
            ResetEvent(m_hndEvent);	//Reset for the next set
        }
        return	FrameCopy;
    }
    bool setup(int cameraId) {
        printf("ICube Event sample (version 1.0.0.0) \n");
        //chec  device existing
        if (ICubeSDK_Init() < 1) {
            printf("No Devices! \n");
        }
        cameraID = cameraId;
        if (!StartAcquisition(cameraID)) {
            opened = false;
        }
        else {
            opened = true;
        }
        return opened;
    }
    bool isOpened() {
        return opened;
    }
    void release() {
        StopAcquisition(cameraID);
    }

    void PrintError(const char* text, int stat, int nInd) {
        switch (stat) {

        case IC_IF_NOT_OPEN:
            printf("Cam[%d] Error: %s (%d), interface not open or camera not present\n", nInd, text, stat);
            break;
        case IC_WRONG_PARAM:
            printf("Cam[%d] Error: %s (%d), invalid parameter\n", nInd, text, stat);
            break;
        case IC_OUT_OF_MEMORY:
            printf("Cam[%d] Error: %s (%d), out of memory\n", nInd, text, stat);
            break;
        case IC_ALREADY_DONE:
            printf("Cam[%d] Error: %s (%d), ICubeSDK_Open,ICubeSDK_Start,... called again without closing\n", nInd, text, stat);
            break;
        case IC_WRONG_CLOCK_VAL:
            printf("Cam[%d] Error: %s (%d), invalid clock value for this mode\n", nInd, text, stat);
            break;
        case IC_COM_LIB_INIT:
            printf("Cam[%d] Error: %s (%d), dll initialisation\n", nInd, text, stat);
            break;
        case IC_NOT_IF_STARTED:
            printf("Cam[%d] Error: %s (%d), dll preview initialisation\n", nInd, text, stat);
            break;
        case IC_WRONG_ROI_ID:
            printf("Cam[%d] Error: %s (%d), invalid roi id\n", nInd, text, stat);
            break;
        case IC_IF_NOT_ENABLED:
            printf("Cam[%d] Error: %s (%d), feature is not enabled\n", nInd, text, stat);
            break;
        case IC_COLOR_CAM_ONLY:
            printf("Cam[%d] Error: %s (%d), feature is not supported by color cameras\n", nInd, text, stat);
            break;
        case IC_DRIVER_VERSION:
            printf("Cam[%d] Error: %s (%d), usb driver version mismatch\n", nInd, text, stat);
            break;
        case IC_D3D_INIT:
            printf("Cam[%d] Error: %s (%d), d3d initialisation\n", nInd, text, stat);
            break;
        case IC_BAD_POINTER:
            printf("Cam[%d] Error: %s (%d), bad pointer\n", nInd, text, stat);
            break;
        case IC_ERROR_FILE_SIZE:
            printf("Cam[%d] Error: %s (%d), invalid file size\n", nInd, text, stat);
            break;
        case IC_RECONNECTION_ACTIVE:
            printf("Cam[%d] Error: %s (%d), usb reconnection is active\n", nInd, text, stat);
            break;
        case IC_USB_REQUEST_FAIL:
            printf("Cam[%d] Error: %s (%d), usb request failed\n", nInd, text, stat);
            break;
        case IC_RESOURCE_IN_USE:
            printf("Cam[%d] Error: %s (%d), resource in use by another proccess\n", nInd, text, stat);
            break;
        case IC_DEVICE_GONE:
            printf("Cam[%d] Error: %s (%d), device is gone\n", nInd, text, stat);
            break;
        case IC_DLL_MISMATCH:
            printf("Cam[%d] Error: %s (%d), dll version mismatch\n", nInd, text, stat);
            break;
        case IC_WRONG_FW_VERSION:
            printf("Cam[%d] Error: %s (%d), feature is not supported by this firmware\n", nInd, text, stat);
            break;
        case IC_NO_RGB_CALLBACK:
            printf("Cam[%d] Error: %s (%d), feature is not without rgb callback\n", nInd, text, stat);
            break;
        case IC_NO_USB30_CAMERA:
            printf("Cam[%d] Error: %s (%d), feature is not supported by usb2.0 cameras\n", nInd, text, stat);
            break;
        case IC_ERR_FIX_RELATION:
            printf("Cam[%d] Error: %s (%d), serial number of fix registered device does not match\n", nInd, text, stat);
            break;
        case IC_CRC_CONFIG_DATA:
            printf("Cam[%d] Error: %s (%d), crc of configuration data is corrupt\n", nInd, text, stat);
            break;
        case IC_CONFIG_DATA:
            printf("Cam[%d] Error: %s (%d), configuration data is corrupt\n", nInd, text, stat);
            break;
        default:
            printf("Cam[%d] Error: %s (%d), unspecified error\n", nInd, text, stat);
            break;
        }
    }
};