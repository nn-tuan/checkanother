#include <opencv2/opencv.hpp>
#include <torch/script.h>
#include <algorithm>
#include <iostream>
#include <time.h>
#ifndef __BBOX_H__
#define __BBOX_H__
#include "BBox.h"
#endif

using namespace cv;
using namespace std;
using namespace torch;

class ResnetAngleDetector
{
public:
    torch::jit::script::Module model;
    int width = 112, height = 112, depth = 3;
    int gpu_id = -1;

    ResnetAngleDetector()
    {
    }

    void loadModel(const char *path, int device_id)
    {
        gpu_id = device_id;
        if (device_id == -1)
        {
            model = torch::jit::load(path);
        }
        else
        {
            torch::Device device(torch::kCUDA, device_id);
            model = torch::jit::load(path, device = device);
        }
    }

    int detectAngle(Mat imgSource, float conf)
    {
        long t = clock();
        Mat img;
        if (imgSource.rows != height || imgSource.cols != width)
        {

            resize(imgSource, img, Size(width, height));
        }
        else
        {
            img = imgSource.clone();
        }
        torch::Tensor tensor_image = torch::from_blob(img.data, {1, width, height, 3}, torch::kUInt8);
        if (gpu_id >= 0)
        {
            torch::Device device(torch::kCUDA, gpu_id);
            tensor_image = tensor_image.to(device);
        }
        tensor_image = tensor_image.to(torch::kFloat32);
        tensor_image = tensor_image.permute({0, 3, 1, 2});
        // tensor_image[0][0] = (tensor_image[0][0] / 255.0 - 0.485) / 0.229;
        // tensor_image[0][1] = (tensor_image[0][1] / 255.0 - 0.456) / 0.224;
        // tensor_image[0][2] = (tensor_image[0][2] / 255.0 - 0.406) / 0.225;

        tensor_image[0][0] = (tensor_image[0][0] / 255.0);
        tensor_image[0][1] = (tensor_image[0][1] / 255.0);
        tensor_image[0][2] = (tensor_image[0][2] / 255.0);

        std::vector<torch::jit::IValue> inputs;
        auto img_var = torch::autograd::make_variable(tensor_image, false);
        inputs.push_back(tensor_image);
        at::Tensor output = model.forward(inputs).toTensor();

        at::Tensor output1 = torch::softmax(output, 1);
        int dim = output1.numel();
        float max_val = 0;
        int angle = -1;
        for (int i = 0; i < dim; i++)
        {
            float v = output1[0][i].item<float>();
            if (v > conf)
            {
                if (v > max_val)
                {
                    max_val = v;
                    angle = i * 10;
                }
            }
        }

        stringstream ss0;
        ss0 << "tmp/" << t << "_" << angle << "_data.pt";
        // torch::jit::pickle_save(tensor_image, ss0.str());

        stringstream ss1;
        ss1 << "tmp/" << t << "_" << angle << ".txt";
        ofstream writer(ss1.str());
        int dim1 = output.numel();
        for (int i = 0; i < dim1; i++)
        {
            float v = output[0][i].item<float>();
            writer << v << " ; ";
        }
        writer.close();

        stringstream ss;
        ss << "tmp/" << t << "_" << angle << ".png";
        imwrite(ss.str(), img);

        return angle;
    }
};