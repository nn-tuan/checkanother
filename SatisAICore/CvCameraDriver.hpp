#include <fstream>
#include <opencv2/opencv.hpp>
#include <stdio.h>
#include <string>
#include <iostream>
#include <vector>
#include <memory>

using namespace cv;
using namespace std;

class CvCameraDriver
{
public:
	VideoCapture vid;
	Mat getFrame()
	{
		Mat img;
		vid >> img;
		return img;
	}
	bool setup(int cameraId)
	{
		Mat img;
		vid = VideoCapture(cameraId);
		vid >> img;
		return !img.empty();
	}
	bool setup(const char *cameraPath)
	{
		Mat img;
		vid = VideoCapture(cameraPath);
		vid >> img;
		return !img.empty();
	}
	bool isOpened()
	{
		return vid.isOpened();
	}
	void release()
	{
		return vid.release();
	}
};
