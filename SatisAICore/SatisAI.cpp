// SatisAI.cpp : Defines the exported functions for the DLL.
//

#include "pch.h"
#include "framework.h"
#include "SatisAI.h"

/// <summary>
/// Default constructor: Load Libtorch dll + create colors
/// </summary>
CSatisAI::CSatisAI()
{
    LoadLibraryA("torch_cuda.dll");
    colors.push_back(cv::Scalar(0, 255, 0));
    colors.push_back(cv::Scalar(255, 100, 0));
    colors.push_back(cv::Scalar(0, 255, 255));
    colors.push_back(cv::Scalar(0, 0, 255));
    colors.push_back(cv::Scalar(255, 255, 0));
    colors.push_back(cv::Scalar(255, 0, 255));
    colors.push_back(cv::Scalar(0, 255, 100));
    colors.push_back(cv::Scalar(100, 255, 0));
    colors.push_back(cv::Scalar(100, 0, 255));
    colors.push_back(cv::Scalar(0, 255, 100));
    colors.push_back(cv::Scalar(0, 255, 100));
}

bool CSatisAI::setCam(const char *_cameraType, int index, const char *cameraPath)
{
    bool opened = false;
    cameraType = _cameraType;

    if (cameraType == "video")
    {
        camCv = CvCameraDriver();
        opened = camCv.setup(cameraPath);
    }
    if (cameraType == "camera")
    {
        camCv = CvCameraDriver();
        opened = camCv.setup(index);
    }
    if (cameraType == "camera_flir")
    {
        camFlir = FlirCameraDriver();
        opened = camFlir.setup(index);
    }
#if USE_CAM_SHODEN
    if (cameraType == "camera_shoden")
    {
        camShoden = ShodenshaCameraDriver();
        opened = camShoden.setup(index);
    }
#endif
#if USE_CAM_SHODEN_CS
    if (cameraType == "camera_shodenCS")
    {
        camShodenCS = ShodenshaCSCameraDriver();
        opened = camShodenCS.setup(index);
    }
#endif

    return opened;
}

void CSatisAI::setLabels(string labelsStr)
{
    stringstream ss(labelsStr);
    string word;
    while (ss >> word)
    {
        labels.push_back(word);
    }
}

void CSatisAI::loadPartsModel(const char *path, int device_id)
{
    detector.loadModel(path, device_id, labels);
    detector.labels = labels;
}

void CSatisAI::loadAngleModel(const char *path, const char *partName, int device_id)
{
    ResnetAngleDetector detector = ResnetAngleDetector();
    detector.loadModel(path, device_id);
    modelNames.push_back(partName);
    angleDetector.push_back(detector);
}

/// <summary>
/// Update Frame and return frame id
/// </summary>
/// <returns>frame id, -1 if fail</returns>
int CSatisAI::updateFrame()
{
    Mat image;
    if (cameraType == "video" || cameraType == "camera")
        image = camCv.getFrame();
    if (cameraType == "camera_flir")
        image = camFlir.getFrame();
#if USE_CAM_SHODEN
    if (cameraType == "camera_shoden")
    {
        image = camShoden.getFrame();
    }
#endif
#if USE_CAM_SHODEN_CS
    if (cameraType == "camera_shodenCS")
    {
        image = camShodenCS.getFrame();
    }
#endif
    if (image.data == NULL)
    {
        return -1;
    }
    w = image.cols;
    h = image.rows;
    if (recording)
        if (recorder.isOpened())
        {
            recorder.write(image);
        }
    FrameData frame = FrameData(image, frameCounter);
    frames.push_back(frame);
    if (frames.size() >= FRAME_QUEUE_SIZE)
    {
        frames.erase(frames.begin());
    }
    frameCounter++;
    return frame.frameId;
}

vector<BBox> CSatisAI::detect(int frameId, float threshold, float nms)
{
    FrameData frame = getFrame(frameId);
    frame.detections = detector.detect(frame.img, threshold, nms);
    frame.detected = true;
    setFrame(frame);
    return frame.detections;
}

bool CSatisAI::loadTemplate(const char *path)
{
    imageTemplate = imread(path, -1);
    return !imageTemplate.empty();
}

vector<BBox> CSatisAI::stitchBox(int frameId)
{
    vector<BBox> results;
    FrameData frame = getFrame(frameId);
    Mat image = frame.img;
    // Mat vis1 = image.clone();
    // Mat vis = imageTemplate.clone();
    stitcher.stitch(imageTemplate, image, false);
    for (int i = 0; i < frame.detections.size(); i++)
    {
        BBox detection = frame.detections[i];
        // circle(vis1, detection.getCenter(), 10, cv::Scalar(0, 255, 0), -1);
        Point mappedPoint = stitcher.transformCoordinate(detection.getCenter());
        detection.xTemplate = mappedPoint.x;
        detection.yTemplate = mappedPoint.y;
        results.push_back(detection);
    }
    // imwrite("src.jpg", vis1);
    // imwrite("mapped.jpg", vis);
    return results;
}

int CSatisAI::detectRotation(int frameId, int boxId, float conf)
{
    FrameData frame = getFrame(frameId);
    Mat image = frame.img.clone();
    vector<BBox> detections = frame.detections;
    if (detections.size() <= boxId)
        return -1;
    BBox box = detections[boxId];
    Rect r(box.x, box.y, box.w, box.h);
    Rect r2 = getSquareRect(r, image.size());
    Mat subImage;
    image(r2).clone().copyTo(subImage);
    string name = box.label;
    int rotation = -1;
    for (int i = 0; i < modelNames.size(); i++)
    {
        if (modelNames[i].compare(name) == 0)
        {
            rotation = angleDetector[i].detectAngle(subImage, conf);
        }
    }
    frame.detections[boxId].rotation = rotation;
    return rotation;
}

FrameData CSatisAI::getFrame(int id)
{
    for (int i = 0; i < frames.size(); i++)
    {
        if (frames[i].frameId == id)
            return frames[i];
    }
    return FrameData();
}

void CSatisAI::setFrame(FrameData data)
{
    for (int i = 0; i < frames.size(); i++)
    {
        if (data.frameId == frames[i].frameId)
        {
            frames[i] = data;
        }
    }
}

bool CSatisAI::startRecord(const char *filePath)
{
    if (w == 0)
        return false;
    int codec = VideoWriter::fourcc('M', 'P', '4', 'V');

    double fps = 20.0;
    Size sizeFrame(w, h);
    recorder.open(filePath, codec, fps, sizeFrame);
    recording = true;
    return true;
}

bool CSatisAI::stopRecord()
{
    recording = false;
    if (recorder.isOpened())
    {
        recorder.release();
    }
    else
    {
        return false;
    }
    return true;
}

extern "C" SATISAI_API void FreeBuffer(unsigned char *myBuffer)
{
    delete[] myBuffer;
}

extern "C" SATISAI_API void FreeImageDataBuffer(ImageData * myBuffer) {
    delete myBuffer;
}

extern "C" SATISAI_API CSatisAI *CreateClass()
{
    return new CSatisAI();
};

extern "C" SATISAI_API void RemoveClass(CSatisAI *pObject)
{
    if (pObject != NULL)
    {
        delete pObject;
        pObject = NULL;
    }
};
extern "C" SATISAI_API bool CheckGPU()
{
    return torch::cuda::is_available();
}

FrameData getFrameToDraw(CSatisAI *pObject, int frameId)
{
    FrameData frame = pObject->getFrame(frameId);
    frame.initVisualize();
    return frame;
}

extern "C" SATISAI_API void ResizeImage(ImageData *image, int w, int h)
{
    if (image != NULL)
        resize(image->mat, image->mat, Size(w, h));
}

extern "C" SATISAI_API ImageData *GetFrame(CSatisAI *pObject, int frameId)
{
    FrameData frame = getFrameToDraw(pObject, frameId);
    ImageData *image = new ImageData();

    image->mat = frame.img.clone();
    return image;
}

extern "C" SATISAI_API int GetWidth(ImageData *image)
{
    return image->mat.cols;
}
extern "C" SATISAI_API int GetHeight(ImageData *image)
{
    return image->mat.rows;
}

extern "C" SATISAI_API void DrawLine(ImageData *img, int x0, int y0, int x1, int y1, const char *colorStr, int lineWidth)
{
    cv::Scalar color = hexToColor(colorStr);
    if (img != NULL)
        line(img->mat, Point(x0, y0), Point(x1, y1), color, lineWidth);
}

extern "C" SATISAI_API void DrawRectangle(ImageData *img, int x0, int y0, int x1, int y1, const char *colorStr, int lineWidth)
{
    cv::Scalar color = hexToColor(colorStr);
    if (img != NULL)
        rectangle(img->mat, Point(x0, y0), Point(x1, y1), color, lineWidth);
}

extern "C" SATISAI_API void DrawArrow(ImageData *img, int x, int y, int angle, int len, const char *colorStr, int thickness)
{
    cv::Scalar color = hexToColor(colorStr);
    cv::Vec3b colorVec(color[0], color[1], color[2]);
    if (img != NULL)
        drawArrow(img->mat, Point(x, y), angle, len, colorVec, thickness);
}

extern "C" SATISAI_API void PutText(ImageData *img, const char *label, int x, int y, const char *colorStr, float fontScale, int thickness)
{
    cv::Scalar color = hexToColor(colorStr);
    if (img != NULL)
        putText(img->mat, label, Point(x, y), FONT_HERSHEY_DUPLEX, fontScale, color, thickness);
}
extern "C" SATISAI_API unsigned char *MatToBytes(ImageData *image)
{
    return matToBytes(image->mat);
}

extern "C" SATISAI_API bool SetCam(CSatisAI *pObject, const char *cameraType, int index, const char *cameraPath)
{
    return pObject->setCam(cameraType, index, cameraPath);
}

extern "C" SATISAI_API void SetLabels(CSatisAI *pObject, const char *labelsStr)
{
    return pObject->setLabels(labelsStr);
}

extern "C" SATISAI_API void LoadPartsModel(CSatisAI *pObject, const char *path, int deviceId)
{
    return pObject->loadPartsModel(path, deviceId);
}

extern "C" SATISAI_API void LoadAngleModel(CSatisAI *pObject, const char *path, const char *partName, int deviceId)
{
    return pObject->loadAngleModel(path, partName, deviceId);
}

extern "C" SATISAI_API unsigned char *BenchmarkPartsModel(CSatisAI *pObject)
{
    string result = "";
    if (pObject != NULL)
    {
        Mat image(1080, 1920, CV_8UC3);
        image.setTo(0);

        clock_t begin = clock();
        for (int i = 0; i < 10; i++)
        {
            pObject->detector.detect(image, 0.1, 0.1);
        }
        clock_t end = clock();
        stringstream ss;
        ss << "Detection part time (10 iterations) " << (end - begin) * 1000 / CLOCKS_PER_SEC << " ms";
        result = ss.str();
    }
    else
    {
        result = "Null Object";
    }
    return (unsigned char *)result.c_str();
}

extern "C" SATISAI_API unsigned char *BenchmarkAngleModel(CSatisAI *pObject)
{
    string result = "";
    if (pObject != NULL)
    {
        Mat image(224, 224, CV_8UC3);
        image.setTo(0);

        clock_t begin = clock();
        for (int i = 0; i < 10; i++)
        {
            pObject->angleDetector[0].detectAngle(image, 0.01);
        }
        clock_t end = clock();
        stringstream ss;
        ss << "Detection angle time (10 iterations) " << (end - begin) * 1000 / CLOCKS_PER_SEC << " ms";
        result = ss.str();
    }
    else
    {
        result = "Null Object";
    }
    return reinterpret_cast<unsigned char *>(const_cast<char *>(result.c_str()));
}

extern "C" SATISAI_API unsigned char *Detect(CSatisAI *pObject, int frameId, float threshold, float nms)
{
    vector<BBox> boxes = pObject->detect(frameId, threshold, nms);
    string result = serializeBoxes(boxes);
    return stringToBytes(result);
}

extern "C" SATISAI_API bool LoadTemplate(CSatisAI *pObject, const char *path)
{
    return pObject->loadTemplate(path);
}

extern "C" SATISAI_API unsigned char *StitchBox(CSatisAI *pObject, int frameId)
{
    vector<BBox> boxes = pObject->stitchBox(frameId);
    string result = serializeBoxes(boxes);
    return stringToBytes(result);
}

extern "C" SATISAI_API unsigned char *MapPointFromStitch(CSatisAI *pObject, int x, int y)
{
    Point pt(x, y);
    Point ptReal = pObject->stitcher.transformCoordinateInv(pt);
    vector<float> vec;
    vec.push_back(ptReal.x);
    vec.push_back(ptReal.y);
    return arrayToBytes(vec);
}

extern "C" SATISAI_API int DetectRotation(CSatisAI *pObject, int frameId, int boxId, float conf)
{
    return pObject->detectRotation(frameId, boxId, conf);
}

extern "C" SATISAI_API int UpdateFrame(CSatisAI *pObject)
{
    return pObject->updateFrame();
}
/// <summary>
/// Save image given frame id
/// </summary>
/// <param name="pObject"></param>
/// <param name="frameId"></param>
/// <param name="filePath"></param>
/// <param name="saveOriginalFrame">Save original frame or visualize frame</param>
/// <returns></returns>
extern "C" SATISAI_API bool SaveFrame(ImageData *image, const char *filePath)
{
    imwrite(filePath, image->mat);
}

extern "C" SATISAI_API bool SavePart(ImageData *image, int x, int y, int w, int h, const char *filePath)
{
    Mat partImage = image->mat(Rect(x, y, w, h));
    imwrite(filePath, partImage);
    return true;
}

extern "C" SATISAI_API bool StartRecord(CSatisAI *pObject, const char *filePath)
{
    if (pObject != NULL)
    {
        return pObject->startRecord(filePath);
    }
    return false;
}

extern "C" SATISAI_API bool StopRecord(CSatisAI *pObject)
{
    if (pObject != NULL)
    {
        return pObject->stopRecord();
    }
    return false;
}