#include <fstream>
#include <opencv2/opencv.hpp>
#include <stdio.h>
#include <string>
#include <iostream>
#include <vector>
#include <memory>
#ifndef __BBOX_H__
#define __BBOX_H__
#include "BBox.h"
#endif

using namespace cv;
using namespace dnn;
using namespace std;

class YoloV4Detector
{
public:
	dnn::Net model;
    vector<string> labels;

	void loadModel(string modelPath, string cfgPath, vector<string> _labels)
	{
		model = readNet(modelPath, cfgPath);
		model.setPreferableBackend(DNN_BACKEND_OPENCV);
		model.setPreferableTarget(DNN_TARGET_CPU);
        labels = _labels;
	}

	vector<BBox> detect(int width, int height, Mat image)
	{
		vector<BBox> results;

		int inpWidth = 416;
		int inpHeight = 416;
		Mat blob;
		Size sz = letterbox(image, 416);
		blobFromImage(image, blob, 1 / 255.0, Size(inpWidth, sz.height), cv::Scalar(0, 0, 0), true, false);
		model.setInput(blob);
		vector<Mat> outs;
		model.forward(outs, getOutputsNames(model));
		vector<BBox> bboxes = postprocess(image, outs);
		return bboxes;
	}

private:
	Size letterbox(Mat frame, int new_shape = 416)
	{
		// Mat frame = inputframe.clone();
		int width = frame.cols;
		int height = frame.rows;
		// cout<<width << " hain? " << height<< endl;
		float ratio = float(new_shape) / max(width, height);
		float ratiow = ratio;
		float ratioh = ratio;
		// cout<<width*ratio << " " << height*ratio << endl;
		int new_unpad0 = int(round(width * ratio));
		int new_unpad1 = int(round(height * ratio));
		int dw = ((new_shape - new_unpad0) % 32) / 2;
		int dh = ((new_shape - new_unpad1) % 32) / 2;
		int top = int(round(dh - 0.1));
		int bottom = int(round(dh + 0.1));
		int left = int(round(dw - 0.1));
		int right = int(round(dw + 0.1));

		// cout<<" ---- "<< new_unpad0 <<  " " << new_unpad1<<endl;
		cv::resize(frame, frame, cv::Size(new_unpad0, new_unpad1), 0, 0, 1); // CV_INTER_LINEAR = 1
		cv::Scalar value(127.5, 127.5, 127.5);
		cv::copyMakeBorder(frame, frame, top, bottom, left, right, cv::BORDER_CONSTANT, value);
		return frame.size();
	}

	// Get the names of the output layers
	vector<String> getOutputsNames(const Net &net)
	{
		static vector<String> names;
		if (names.empty())
		{
			// Get the indices of the output layers, i.e. the layers with unconnected outputs
			vector<int> outLayers = net.getUnconnectedOutLayers();

			// get the names of all the layers in the network
			vector<String> layersNames = net.getLayerNames();

			// Get the names of the output layers in names
			names.resize(outLayers.size());
			for (size_t i = 0; i < outLayers.size(); ++i)
				names[i] = layersNames[outLayers[i] - 1];
		}
		return names;
	}

	vector<BBox> postprocess(Mat &frame, const vector<Mat> &outs)
	{
		float confThreshold = 0.3f; // Confidence threshold
		float nmsThreshold = 0.5;	// Non-maximum suppression threshold

		vector<BBox> results;
		vector<int> classIds;
		vector<float> confidences;
		vector<Rect> boxes;

		for (size_t i = 0; i < outs.size(); ++i)
		{
			// Scan through all the bounding boxes output from the network and keep only the
			// ones with high confidence scores. Assign the box's class label as the class
			// with the highest score for the box.
			float *data = (float *)outs[i].data;
			for (int j = 0; j < outs[i].rows; ++j, data += outs[i].cols)
			{
				Mat scores = outs[i].row(j).colRange(5, outs[i].cols);
				Point classIdPoint;
				double confidence;
				// Get the value and location of the maximum score
				minMaxLoc(scores, 0, &confidence, 0, &classIdPoint);
				if (confidence > confThreshold)
				{
					int centerX = (int)(data[0] * frame.cols);
					int centerY = (int)(data[1] * frame.rows);
					int width = (int)(data[2] * frame.cols);
					int height = (int)(data[3] * frame.rows);
					int left = centerX - width / 2;
					int top = centerY - height / 2;

					classIds.push_back(classIdPoint.x);
					confidences.push_back((float)confidence);
					boxes.push_back(Rect(left, top, width, height));
					// boxes.push_back(newbox);
				}
			}
		}

		// Perform non maximum suppression to eliminate redundant overlapping boxes with
		// lower confidences
		vector<int> indices;
		NMSBoxes(boxes, confidences, confThreshold, nmsThreshold, indices);
		for (size_t i = 0; i < indices.size(); ++i)
		{
			int idx = indices[i];
			Rect box = boxes[idx];
			BBox bbox(
                labels[classIds[idx]],
				confidences[idx],
				box.tl().x, box.tl().y,
				box.br().x, box.br().y);
			results.push_back(bbox);
		}
		return results;
	}
};