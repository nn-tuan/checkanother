﻿using SatisAIManage.MVVMTool;
using System;
using System.Windows.Controls;
using System.Collections.ObjectModel;
using System.Windows.Media;
using System.Windows;

namespace SatisAIManage.ViewModels
{
    public class RectItem
    {
        public double X { get; set; }
        public double Y { get; set; }
        public double Width { get; set; }
        public double Height { get; set; }
        public Brush? Stroke { get; set; }
        public Brush? Fill { get; set; }
        public int StrokeThickness { get; set; }
        public Brush? Background { get; set; }
        public double Opacity { get; set; }
    }
    public class ArrowItem
    {
        public Brush? Stroke { get; set; }
        public int StrokeThickness { get; set; }
        public int ArrowLength { get; set; }
        public int Degree { get; set; }
        public double X { get; set; }
        public double Y { get; set; }

        public double X0_1 { get; set; }
        public double X0_2 { get; set; }
        public double Y0_1 { get; set; }
        public double Y0_2 { get; set; }

        public double X1_1 { get; set; }
        public double X1_2 { get; set; }
        public double Y1_1 { get; set; }
        public double Y1_2 { get; set; }

        public double X2_1 { get; set; }
        public double X2_2 { get; set; }
        public double Y2_1 { get; set; }
        public double Y2_2 { get; set; }


        public void DrawArrow()
        {
            DrawLine(Degree, ArrowLength, 0);
            DrawLine(Degree - 30 - 180, ArrowLength * 0.2, 1);
            DrawLine(Degree + 30 - 180, ArrowLength * 0.2, 2);
        }
        private void DrawLine(int degrees, double length, int state_line)
        {
            double angle = Math.PI * (degrees + 180) / 180.0;
            double x2 = 0, y2 = 0;


            if (degrees % 360 == 0)
            {
                x2 = -length;
                y2 = 0;
            }
            else if (degrees % 360 == 180)
            {
                x2 = length;
                y2 = 0;
            }
            else if (degrees % 360 == 90)
            {
                x2 = 0;
                y2 = -length;
            }
            else if (degrees % 360 == 270)
            {
                x2 = 0;
                y2 = length;
            }
            else
            {
                x2 = Math.Cos(angle) * length;
                y2 = Math.Sin(angle) * length;
            }
            if (state_line == 0)
            {
                X0_1 = X;
                Y0_1 = Y;
                X0_2 = X0_1 + x2;
                Y0_2 = Y0_1 + y2;
            }
            else if (state_line == 1) 
            {
                X1_1 = X0_2;
                Y1_1 = Y0_2;
                X1_2 = X1_1 + x2;
                Y1_2 = Y1_1 + y2;
            }
            else if (state_line == 2)
            {
                X2_1 = X0_2;
                Y2_1 = Y0_2;
                X2_2 = X2_1 + x2;
                Y2_2 = Y2_1 + y2;
            }
        }
    }
    public class TextItem
    {
        public double X { get; set; }
        public double Y { get; set; }
        public Brush? Background { get; set; }
        public Brush? Foreground { get; set; }
        public FontStyle FontStyle { get; set; }
        public Thickness Padding { get; set; }
        public string? Text { get; set; }
        public double FontSize { get; set; }
    }


    public class BoxTxtArrowItem
    {
        public double X { get; set; }
        public double Y { get; set; }
        public RectItem? RectItem { get; set; }
        public ArrowItem? ArrowItem { get; set; }
        public TextItem? TextItem { get; set; }

    }

    public class ItemOverlay
    {
        public ObservableCollection<RectItem> RectItems { get; set; }
        public ObservableCollection<ArrowItem> ArrowItems { get; set; }
        public ObservableCollection<TextItem> TextItems { get; set; }

        public  ItemOverlay()
        {
            RectItems = new ObservableCollection<RectItem>();
            ArrowItems = new ObservableCollection<ArrowItem>();
            TextItems = new ObservableCollection<TextItem>();
        }

        public void Clear()
        {
            RectItems.Clear();
            ArrowItems.Clear();
            TextItems.Clear();
        }
        public void AddCreateItem(BoxTxtArrowItem BoxTxtArrowItem)
        {
            BoxTxtArrowItem.RectItem!.X = BoxTxtArrowItem.X;
            BoxTxtArrowItem.RectItem!.Y = BoxTxtArrowItem.Y;

            BoxTxtArrowItem.TextItem!.X = BoxTxtArrowItem.X;
            BoxTxtArrowItem.TextItem!.Y = BoxTxtArrowItem.Y - BoxTxtArrowItem.TextItem.FontSize - BoxTxtArrowItem.TextItem.Padding.Bottom * 2; 

            AddBox(BoxTxtArrowItem.RectItem, true);
            BoxTxtArrowItem.ArrowItem!.X = (BoxTxtArrowItem.RectItem.X + BoxTxtArrowItem.RectItem.Width / 2) / 2;
            BoxTxtArrowItem.ArrowItem!.Y = (BoxTxtArrowItem.RectItem.Y + BoxTxtArrowItem.RectItem.Height / 2) / 2;
            AddArrow(BoxTxtArrowItem.ArrowItem, true);
            AddTextBlock(BoxTxtArrowItem.TextItem, true);
        }

        public void AddItem(BoxTxtArrowItem BoxTxtArrowItem)
        {
            BoxTxtArrowItem.RectItem!.X = BoxTxtArrowItem.X;
            BoxTxtArrowItem.RectItem!.Y = BoxTxtArrowItem.Y;

            BoxTxtArrowItem.TextItem!.X = BoxTxtArrowItem.X;
            BoxTxtArrowItem.TextItem!.Y = BoxTxtArrowItem.Y - BoxTxtArrowItem.TextItem.FontSize - BoxTxtArrowItem.TextItem.Padding.Bottom * 2;

            AddBox(BoxTxtArrowItem.RectItem, false);
            BoxTxtArrowItem.ArrowItem!.X = (BoxTxtArrowItem.RectItem.X + BoxTxtArrowItem.RectItem.Width / 2) / 2;
            BoxTxtArrowItem.ArrowItem!.Y = (BoxTxtArrowItem.RectItem.Y + BoxTxtArrowItem.RectItem.Height / 2) / 2;
            AddArrow(BoxTxtArrowItem.ArrowItem, false);
            AddTextBlock(BoxTxtArrowItem.TextItem, false);
        }

        private void AddBox(RectItem RectItem, bool CreatNewItem)
        {
            if(CreatNewItem)
            {
                RectItem rectItem = new RectItem()
                {
                    X = RectItem.X,
                    Y = RectItem.Y,
                    Width = RectItem.Width,
                    Height = RectItem.Height,
                    Stroke = RectItem.Stroke,
                    StrokeThickness = RectItem.StrokeThickness,
                    Fill = RectItem.Fill,
                    Background = RectItem.Background,
                    Opacity = RectItem.Opacity,
                };
                RectItems.Add(rectItem);
            }
            else
            {
                RectItems.Add(RectItem);
            }
        }

        private void AddArrow(ArrowItem ArrowItem, bool CreatNewItem)
        {
            if (CreatNewItem)
            {
                ArrowItem arrowItem = new ArrowItem()
                {
                    X = ArrowItem.X,
                    Y= ArrowItem.Y,
                    Degree = ArrowItem.Degree,
                    ArrowLength = ArrowItem.ArrowLength,
                    Stroke = ArrowItem.Stroke,
                    StrokeThickness= ArrowItem.StrokeThickness,
                };
                arrowItem.DrawArrow();
                ArrowItems.Add(arrowItem);
            }
            else
            {
                ArrowItem.DrawArrow();
                ArrowItems.Add(ArrowItem);
            }
        }
        private void AddTextBlock(TextItem TextItem, bool CreatNewItem)
        {
            if (CreatNewItem)
            {
                TextItem textItem = new TextItem()
                {
                    Text = TextItem.Text,
                    FontSize = TextItem.FontSize,
                    FontStyle = TextItem.FontStyle,
                    Foreground = TextItem.Foreground,
                    Background = TextItem.Background,
                    Padding = TextItem.Padding,
                    X = TextItem.X,
                    Y = TextItem.Y,
                };
                TextItems.Add(textItem);
            }
            else
            {
                TextItems.Add(TextItem);
            }
        }
    }
}
