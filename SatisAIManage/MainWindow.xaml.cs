using SatisAIManage.ViewModels;
using System.Windows;
using System.Windows.Input;
using System;
using System.IO;

namespace SatisAIManage
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        // Parent ViewModel
        private readonly MainFrameViewModel MainFrameVM;

        // Controller Class
        private SatisAICtrl.SatisAICtrl controlManager = new SatisAICtrl.SatisAICtrl();

        public MainWindow()
        {
            controlManager.InspectMode = false; // Manage mode
            MVVMTool.DispatcherHelper.SetDispatcher(Dispatcher);
            InitializeComponent();

            // VMとの結合
            MainFrameVM = new MainFrameViewModel(controlManager);
            MainFrameVM.CloseAppEvent += CloseEventHandler;
            MainFrame.DataContext = MainFrameVM;
            
       }

        /// <summary>
        /// WindowStyle="None"時のウィンドウ移動
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        /// <summary>
        /// 閉じるボタンから呼ばれるイベント
        /// </summary>
        public void CloseEventHandler()
        {
            Close();
        }
    }
}