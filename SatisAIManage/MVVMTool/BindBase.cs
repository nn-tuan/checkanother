using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace SatisAIManage.MVVMTool
{
    public class BindBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler? PropertyChanged;

        /// <summary>
        /// 画面上の値を更新する
        /// </summary>
        /// <param name="propertyName">
        ///   - Null : CallerMemberName
        ///   - Not Null: nameof(BindしているpropertyName)
        /// </param>
        protected void RaisePropertyChanged([CallerMemberName] string? propertyName = null)
            => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
}