﻿using System.Windows.Media.Imaging;
using System.Windows.Media;

namespace SatisAIManage.Utils
{
    public class BitmapUtil
    {
        public static BitmapSource GetBitmap(byte[] data, int w, int h, int c)
        {
            var dpiX = 96d;
            var dpiY = 96d;
            var pixelFormat = PixelFormats.Bgr24; // bgr
            if (c == 4)
            {
                pixelFormat = PixelFormats.Bgra32; // + alpha channel
            }
            if (c == 1)
            {
                pixelFormat = PixelFormats.Gray8; // + alpha channel
            }
            var bytesPerPixel = (pixelFormat.BitsPerPixel + 7) / 8; // == 1 in this example
            var stride = bytesPerPixel * w; // == width in this example

            var bitmap = BitmapSource.Create(w, h, dpiX, dpiY,
                                             pixelFormat, null, data, stride);
            bitmap.Freeze();
            return bitmap;
        }
    }
}
