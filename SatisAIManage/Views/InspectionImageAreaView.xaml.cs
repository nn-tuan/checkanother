
using SatisAICtrl.Models;
using SatisAIManage.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace SatisAIManage.Views
{
    /// <summary>
    /// InspectionImageAreaView.xaml の相互作用ロジック
    /// </summary>
    public partial class InspectionImageAreaView : UserControl
    {
        public InspectionImageAreaView()
        {
            InitializeComponent();

            this.DataContextChanged += InspectionImageAreaView_DataContextChanged;


            //System.Windows.Shapes.Rectangle rect;
            //rect = new System.Windows.Shapes.Rectangle();
            //rect.Stroke = new SolidColorBrush(Colors.Black);
            //rect.Fill = new SolidColorBrush(Color.FromArgb(100, 0, 255, 0));
            //rect.Width = 200;
            //rect.Height = 200;
            //Canvas.SetLeft(rect, 0);
            //Canvas.SetTop(rect, 0);
            //DrawCanvas.Children.Add(rect);

        }

        private void InspectionImageAreaView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue is InspectionImageAreaViewModel)
            {
                var vm = e.NewValue as InspectionImageAreaViewModel;
                if(vm != null)
                {
                    vm.MediaPlayEvent += MediaPlayEventHandler;
                }
            }
        }

        private void MediaPlayEventHandler()
        {
            //MovieMediaElement.LoadedBehavior = MediaState.Manual;
            //MovieMediaElement.Play();
        }

        private void DrawCanvas_MouseMove(object sender, MouseEventArgs e)
        {

        }

        public void DrawBoundingBoxes(List<BoundingBox> boxes)
        {
            //DrawCanvas.Children.Clear();
            //foreach (var box in boxes)
            //{

            //}
        }

        private void MediaElement_MouseMove(object sender, MouseEventArgs e)
        {
        }

        public void Test() { }

        private void MyCanvas_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void MyCanvas_SizeChanged(object sender, SizeChangedEventArgs e)
        {

        }
    }
}
