using SatisAIManage.ViewModels;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SatisAIManage.Views
{
    /// <summary>
    /// MainControlView.xaml の相互作用ロジック
    /// </summary>
    public partial class MainControlView : UserControl
    {
        public MainControlView()
        {
            InitializeComponent();

            this.DataContextChanged += MainControlView_DataContextChanged;
        }

        private void MainControlView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue is MainControlViewModel)
            {
                var vm = e.NewValue as MainControlViewModel;
                if (vm != null)
                {
                }
            }
        }

        private void UpdateResultDesignEventHandler(string result)
        {
            LinearGradientBrush myLinearGradientBrush = new LinearGradientBrush();

            if (result == "NG")
            {
                myLinearGradientBrush.StartPoint = new Point(0.5, 0);
                myLinearGradientBrush.EndPoint = new Point(0.5, 1);
                myLinearGradientBrush.GradientStops.Add(new GradientStop((Color)ColorConverter.ConvertFromString("#FFED8C7D"), 0.743));
                myLinearGradientBrush.GradientStops.Add(new GradientStop(Colors.White, 0.26));
                myLinearGradientBrush.GradientStops.Add(new GradientStop((Color)ColorConverter.ConvertFromString("#FFFB0202"), 1));
            }
            else
            {
                myLinearGradientBrush.StartPoint = new Point(0.5, 0);
                myLinearGradientBrush.EndPoint = new Point(0.5, 1);
                myLinearGradientBrush.GradientStops.Add(new GradientStop((Color)ColorConverter.ConvertFromString("#FF0C78F1"), 0.743));
                myLinearGradientBrush.GradientStops.Add(new GradientStop(Colors.White, 0.26));
                myLinearGradientBrush.GradientStops.Add(new GradientStop((Color)ColorConverter.ConvertFromString("#FF081C6B"), 1));
            }

            this.Background = myLinearGradientBrush;
        }

        private void TargetInfoAreaView_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void ItemListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
        private void OnMove(object sender, System.Windows.Input.MouseEventArgs e)
        {

        }

    }
}