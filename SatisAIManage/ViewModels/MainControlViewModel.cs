using Microsoft.Win32;
using SatisAICtrl.Models;
using SatisAICtrl;
using SatisAIManage.MVVMTool;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Media;



namespace SatisAIManage.ViewModels
{
   

    public class MainControlViewModel : BindBase
    {
        #region Fields
        private readonly SatisAICtrl.SatisAICtrl controlManager;
        #endregion Fields

        #region Properties
        public ButtonAreaViewModel? ButtonAreaVM { get; set; }

        #endregion Properties

        #region Event
        /// <summary> 閉じるボタン押下時のイベント </summary>
        public event Action? CloseAppEvent;
        /// <summary> 画面遷移時のイベント</summary>
        internal event Action<FigureState>? FigureChangeEvent;
        #endregion Event

        /// <summary>
        /// デザイナの参照用（使用しない）
        /// </summary>
        /// 
        public MainControlViewModel()
        {
        }
        public MainControlViewModel(SatisAICtrl.SatisAICtrl managerObj)
        {
            controlManager = managerObj; 
            controlManager.CloseAppEvent += _Ctrl_CloseAppEvent;
            ButtonAreaVM = new ButtonAreaViewModel(managerObj);
            ButtonAreaVM.FigureChangeEvent += (state) =>
            {
                FigureChangeInvoke(state);
            };
        }

        private void _Ctrl_CloseAppEvent()
        {
            CloseAppEvent?.Invoke();
        }

        /// <summary>
        /// 画面遷移処理
        /// </summary>
        /// <param name="state">遷移先</param>
        private void FigureChangeInvoke(FigureState state)
        {
            FigureChangeEvent?.Invoke(state);
            //RaisePropertyChanged(nameof(MenuVisibility));
            //RaisePropertyChanged(nameof(ViewLow));
        }


    }
}