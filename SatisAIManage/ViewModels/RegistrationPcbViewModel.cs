﻿using SatisAICtrl;
using SatisAICtrl.Models;
using SatisAIManage.MVVMTool;
using System;
using System.Collections.Generic;
using System.Windows.Media;
using System.Windows.Forms;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Collections.Specialized;

namespace SatisAIManage.ViewModels
{
    public class RegistrationPcbViewModel : BindBase 
    {
        #region Fields
        private readonly ISatisAICtrl? _ControlManager;
        #endregion Fields

        #region Properties
        public RegistrationPcbButtonAreaModel? RegistrationPcbButtonAreaVM { get; set; }
        public DelegateCommand? OutputPartBtnCommand { get; set; }
        public InspectionImageAreaViewModel? InspectionImageAreaVM { get; set; }

        private string _OutputPartTxt = "";
        public string OutputPartTxt
        {
            get { return _OutputPartTxt; }
            set
            {
                _OutputPartTxt = value;
                base.RaisePropertyChanged("OutputPartTxt");
            }
        }
        public string? NewProjectName { get; set; } = "NewProject";
        public string? NewBoardName { get; set; } = "NewBoard";
        public string? ModelNumber { get; set; } = "ModelNumber";
        public int? RotationTol { get; set; } = 30; //Rotation

        public int? DistanceTol { get; set; } = 30; //Percentage
        public OperationLogViewModel? OperationLogVM { get; set; }

        private ObservableCollection<PartItem> _PartsList = new ObservableCollection<PartItem>();

        private bool _AllConfirm = false;

        public bool AllConfirm
        {
            get
            {
                return _AllConfirm;
            }
            set
            {
                _AllConfirm = value;
                RaisePropertyChanged("AllConfirm");
            }
        }
        //public bool _AllConfirm { get; set; } = true;

        public bool ListViewClickFlag { get; set; } = false;
        public bool ListViewUpdateFlag { get; set; } = false;


        private System.Windows.Input.ICommand? _Unchecked;
        public System.Windows.Input.ICommand Unchecked
        {
            get
            {
                return _Unchecked ?? (_Unchecked = new RelayCommand<System.Windows.Controls.CheckBox>(
                          x => {
                              DoStuffWhenUnchecked(x);
                          }));
            }
        }
        private void DoStuffWhenUnchecked(System.Windows.Controls.CheckBox obj)
        {
            bool IsAllChecked = true;
            _ControlManager!.DisanableUpdate();
            for (int i = 0; i < PartsList.Count; i++)
            {
                if(PartsList[i].Confirm == false)
                {
                    IsAllChecked = false;
                    break;
                }
            }
            if(IsAllChecked)
            {
                for (int i = 0; i < PartsList.Count; i++)
                {
                    PartsList[i].Confirm = AllConfirm;
                }
            }

            _ControlManager.EnableUpdate();
            _ControlManager.UpdateCheckBox();
            /*ListViewClickFlag = false;
            ListViewUpdateFlag = false;*/
            //_ControlManager.UpdateCheckBox();
        }


        private System.Windows.Input.ICommand? _CheckedChanged;
        public System.Windows.Input.ICommand CheckedChanged
        {
            get
            {
                return _CheckedChanged ?? (_CheckedChanged = new RelayCommand<System.Windows.Controls.CheckBox>(
                          x => {
                              DoStuffWhenChecked(x);
                          }));
            }
        }
        private void DoStuffWhenChecked(System.Windows.Controls.CheckBox obj)
        {
            _ControlManager?.DisanableUpdate();
            for (int i = 0; i < PartsList.Count; i++)
            {
                PartsList[i].Confirm = AllConfirm;
            }
            _ControlManager?.EnableUpdate();
            _ControlManager?.UpdateCheckBox();
        }

        private System.Windows.Input.ICommand? _mouseClick;
        public System.Windows.Input.ICommand MouseClick
        {
            get
            {
                return _mouseClick ?? (_mouseClick = new RelayCommand<System.Windows.Controls.ListView>(
                          x => {
                              DoStuffWhenMouseClick(x);
                          }));
            }
        }

        private System.Windows.Input.ICommand? _mouseMove;
        public System.Windows.Input.ICommand MouseMove
        {
            get
            {
                return _mouseMove ?? (_mouseMove = new RelayCommand<System.Windows.Controls.ListView>(
                          x => {
                              DoStuffWhenMouseMove(x);
                          }));
            }
        }
        
        private void DoStuffWhenMouseMove(System.Windows.Controls.ListView obj)
        {
        }
        private void DoStuffWhenMouseClick(System.Windows.Controls.ListView obj)
        {
        }

        public ObservableCollection<PartItem> PartsList
        {
            get
            {
                return _PartsList;
            }
            set
            {
                _PartsList = value;
                RaisePropertyChanged();
            }
        }

        public string SourceVM { get; set; } = "";
        #endregion Properties

        #region Event
        /// <summary> 画面遷移時のイベント</summary>
        internal event Action<FigureState>? FigureChangeEvent;
        public event PropertyChangedEventHandler? PropertyChanged;
        #endregion Event

        public RegistrationPcbViewModel()
        {

        }

        public RegistrationPcbViewModel(SatisAICtrl.SatisAICtrl controlManager)
        {
            _ControlManager = controlManager;
            OperationLogVM = new OperationLogViewModel(controlManager);
            OperationLogVM?.AddLog("Started");
            
            RegistrationPcbButtonAreaVM = new RegistrationPcbButtonAreaModel(_ControlManager);
            InspectionImageAreaVM = new InspectionImageAreaViewModel(controlManager);

            OutputPartBtnCommand = new DelegateCommand(() => OutputPartRegistrationPcb());

            PartsList.CollectionChanged += PartsList_CollectionChanged!;
            controlManager.ImageDataUpdateEvent += _Ctrl_BitmapDataUpdateEvent;
            controlManager.PcbUpdateEvent += _Ctrl_PcbUpdateEvent;
            controlManager.StartPcbRegisterEvent += _StartPcbRegisterEvent;
            controlManager.StopPcbRegisterEvent += _StopPcbRegisterEvent;
            controlManager.RegisterPcbRegisterEvent += _RegisterPcbRegisterEvent;

            RegistrationPcbButtonAreaVM.FigureChangeEvent += (state) =>
            {
                FigureChangeInvoke(state);
            };
        }

        #region Private Methods


        private void PartsList_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.OldItems != null)
            {
                foreach (INotifyPropertyChanged item in e.OldItems)
                    item.PropertyChanged -= Partitem_PropertyChanged!;
            }
            if (e.NewItems != null)
            {
                foreach (INotifyPropertyChanged item in e.NewItems)
                    item.PropertyChanged += Partitem_PropertyChanged!;
            }
        }

        private void Partitem_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            PartItem item = (PartItem)sender;
            //_ControlManager!.TogglePcbPart(item.No - 1);

            //PcbBoard board = _ControlManager!.PcbBoard;
            //board.Parts.Clear();
            //for (int i = 0; i < PartsList.Count; i++)
            //{
            //    board.Parts[i].Confirm = PartsList[i].Confirm;
            //}
            //_ControlManager!.UpdatePcb(board);
        }
        private void _StartPcbRegisterEvent()
        {
            OperationLogVM?.AddLog("Start Pcb Register Event");
            RegistrationPcbButtonAreaVM?.UpdateEventRegisterState(true);
            _ControlManager!.StartRegisterPcb();
        }

        private void _StopPcbRegisterEvent()
        {
            OperationLogVM?.AddLog("Stop Pcb Register Event");
            RegistrationPcbButtonAreaVM?.UpdateEventRegisterState(false);
            _ControlManager?.StopRegisterPcb();
        }
        private void _RegisterPcbRegisterEvent()
        {
            if (OutputPartTxt.Length == 0
                || NewProjectName?.Length == 0
                || NewBoardName?.Length == 0
                || RotationTol == 0
                || DistanceTol == 0
                )
            {
                OperationLogVM?.AddLog("Missing information");
            }
            else
            {
               string outPath = _ControlManager!.RegisterPcb(OutputPartTxt, NewProjectName!, NewBoardName!, ModelNumber!, (int)RotationTol!, (int) DistanceTol!);
                OperationLogVM?.AddLog("Saved! " + outPath);
            }
        }
        /// <summary>
        /// 画面遷移処理
        /// </summary>
        /// <param name="state">遷移先</param>
        private void FigureChangeInvoke(FigureState state)
        {
            FigureChangeEvent?.Invoke(state);
            //RaisePropertyChanged(nameof(MenuVisibility));
            //RaisePropertyChanged(nameof(ViewLow));
        }
        private string OpenFolderEventHandler()
        {
            FolderBrowserDialog chosefolder = new FolderBrowserDialog();
            chosefolder.ShowDialog();
            return chosefolder.SelectedPath;
        }


        private void _Ctrl_BitmapDataUpdateEvent(byte[] data, int w, int h, int c)
        {
            InspectionImageAreaVM?._Ctrl_ViewImageUpdateEvent(Utils.BitmapUtil.GetBitmap(data, w, h, c));
        }
        private void _Ctrl_PcbUpdateEvent(PcbBoard board)
        {

            OperationLogVM?.AddLog("Detected "+board.Parts.Count+" parts");
            
            if (PartsList.Count == board.Parts.Count)
            {
                for(int i = 0; i < board.Parts.Count; i++)
                {
                    PartItem part = board.Parts[i];
                    if (part!= PartsList[i])
                    {
                        PartsList[i] = part;
                    }
                }
            }
            else
            {
                PartsList.Clear();
                foreach (PartItem part in board.Parts)
                {
                    PartsList.Add(part);
                }
            }
            bool IsAllConfirm = true;
            for (int i = 0; i < PartsList.Count; i++)
            {
                if (PartsList[i].Confirm == false)
                {
                    AllConfirm = false;
                    IsAllConfirm = false;
                    break;
                }
            }
            if (IsAllConfirm)
            {
                AllConfirm = true;
            }    
        }

        private void PartItem_PropertyChanged(object arg1, PartItem arg2)
        {
        }

        private void OutputPartRegistrationPcb()
        {
            /* =============== Chose save Folder =============== */
            OutputPartTxt = OpenFolderEventHandler();
            // TO DO: Handle save folder output

            /* ======= ReShow RegistrationPcbView Windown ======== */
            //FigureChangeEvent?.Invoke(FigureState.RegisterPcb);
        }
        #endregion Private Methods

        #region INotifyPropertyChanged implementation
        public void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion INotifyPropertyChanged implementation
    }
}
