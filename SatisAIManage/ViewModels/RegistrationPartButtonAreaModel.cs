﻿using Microsoft.Win32;
using SatisAICtrl;
using SatisAIManage.MVVMTool;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SatisAIManage.ViewModels
{
    public class RegistrationPartButtonAreaModel : BindBase
    {
        private readonly ISatisAICtrl? _ControlManager;

        #region Fields

        // 機種変更 ボタンを押せるかどうかの状態変数
        private bool StartButtonCanExecute = true;
        private bool StopButtonCanExecute = true;

        // 閉じる ボタンを押せるかどうかの状態変数
        private bool CloseButtonCanExecute = true;

        #endregion Fields

        #region Properties

        #region Command
        public DelegateCommand? StartRegistrationPartCommand { get; }
        public DelegateCommand? StopRegistrationPartCommand { get; }
        public DelegateCommand? CloseRegistrationPartCommand { get; }
        #endregion Command
        #endregion Properties

        #region Event
        /// <summary> 画面遷移時のイベント </summary>
        internal event Action<FigureState>? FigureChangeEvent;
        #endregion Event


        private bool _StopEnable;
        public bool StopEnable
        {
            get { return _StopEnable; }
            set
            {
                _StopEnable = value;
                base.RaisePropertyChanged("StopEnable");
            }
        }


        private bool _StartEnable;
        public bool StartEnable
        {
            get { return _StartEnable; }
            set
            {
                _StartEnable = value;
                base.RaisePropertyChanged("StartEnable");
            }
        }

        public RegistrationPartButtonAreaModel()
        {
        }

        public RegistrationPartButtonAreaModel(ISatisAICtrl controlManager)
        {
            _ControlManager = controlManager;
            StartRegistrationPartCommand   = new DelegateCommand(() => StartRegistrationPartEventHandler(), () => { return StartButtonCanExecute; });
            StopRegistrationPartCommand    = new DelegateCommand(() => StopRegistrationPartEventHandler(), () => { return StopButtonCanExecute;  });
            CloseRegistrationPartCommand   = new DelegateCommand(() => CloseEventHandler(), () => { return CloseButtonCanExecute; });
            UpdateEventRegisterState(false);
        }


        private void StartRegistrationPartEventHandler()
        {
            _ControlManager?.RaiseStartPartRegisterEvent();
        }
   

        private void StopRegistrationPartEventHandler()
        {
            _ControlManager?.RaiseStopPartRegisterEvent();
        }
        private void RegistrationPartEventHandler()
        {

        }

        public void UpdateEventRegisterState(bool start)
        {
            if (start)
            {
                StartEnable = false;
                StopEnable = true;
            }
            else
            {
                StartEnable = true;
                StopEnable = false;
            }
        }

        /// <summary>
        /// 閉じるボタン押下時のイベント
        /// </summary>
        private void CloseEventHandler()
        {
            // Closeイベント呼び出し
            FigureChangeEvent?.Invoke(FigureState.MainControl);
        }

        
    }
}