﻿using Microsoft.Win32;
using SatisAICtrl;
using SatisAIManage.MVVMTool;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SatisAIManage.ViewModels
{
    public class RegistrationPcbButtonAreaModel : BindBase
    {
        private readonly ISatisAICtrl? _ControlManager;

        #region Fields

        // 機種変更 ボタンを押せるかどうかの状態変数
        private bool ModelChangeButtonCanExecute = true;

        // 閉じる ボタンを押せるかどうかの状態変数
        private bool CloseButtonCanExecute = true;

        #endregion Fields

        #region Properties

        #region Command
        public DelegateCommand? StartRegistrationPcbCommand { get; }
        public DelegateCommand? StopRegistrationPcbCommand { get; }
        public DelegateCommand? RegistrationPcbCommand { get; }
        public DelegateCommand? CloseRegistrationPcbCommand { get; }
        #endregion Command
        #endregion Properties

        #region Event
        /// <summary> 画面遷移時のイベント </summary>
        internal event Action<FigureState>? FigureChangeEvent;
        #endregion Event

        private bool _StopEnable = false;
        public bool StopEnable
        {
            get { return _StopEnable; }
            set
            {
                _StopEnable = value;
                base.RaisePropertyChanged("StopEnable");
            }
        }


        private bool _StartEnable;
        public bool StartEnable
        {
            get { return _StartEnable; }
            set
            {
                _StartEnable = value;
                base.RaisePropertyChanged("StartEnable");
            }
        }

        private bool _RegisterEnable;
        public bool RegisterEnable
        {
            get { return _RegisterEnable; }
            set
            {
                _RegisterEnable = value;
                base.RaisePropertyChanged("RegisterEnable");
            }
        }


        public RegistrationPcbButtonAreaModel()
        {
        }

        public RegistrationPcbButtonAreaModel(ISatisAICtrl controlManager)
        {
            _ControlManager = controlManager;
            ModelChangeButtonCanExecute = true;
            StartRegistrationPcbCommand   = new DelegateCommand(() => StartRegistrationPcbEventHandler(), () => { return ModelChangeButtonCanExecute; });
            StopRegistrationPcbCommand    = new DelegateCommand(() => StopRegistrationPcbEventHandler());
            RegistrationPcbCommand        = new DelegateCommand(() => RegistrationPcbEventHandler());
            CloseRegistrationPcbCommand   = new DelegateCommand(() => CloseEventHandler(), () => { return CloseButtonCanExecute; });
            UpdateEventRegisterState(false);
        }


        private void StartRegistrationPcbEventHandler()
        {
            if(StartEnable)
            {
                _ControlManager?.RaiseStartPcbRegisterEvent();
            }   
            else
            {
                System.Windows.Forms.MessageBox.Show("Erorr!!! RegistrantionPCB is starting");
            }    
        }

        private void StopRegistrationPcbEventHandler()
        {
            if (!StartEnable)
            {
                _ControlManager?.RaiseStopPcbRegisterEvent();
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("Erorr!!! RegistrantionPCB stoped!");
            }
        }
        private void RegistrationPcbEventHandler()
        {
            if (!StartEnable)
            {
                _ControlManager?.RaiseRegisterPcbRegisterEvent();
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("Erorr!!! RegistrantionPCB stoped!");
            }
        }

        /// <summary>
        /// 閉じるボタン押下時のイベント
        /// </summary>
        private void CloseEventHandler()
        {
            // Closeイベント呼び出し
            FigureChangeEvent?.Invoke(FigureState.MainControl);
        }

        public void UpdateEventRegisterState(bool start)
        {
            if(start)
            {
                StartEnable = false;
                StopEnable = true;
                RegisterEnable = true;
            }    
            else
            {
                StartEnable = true;
                StopEnable = false;
                RegisterEnable = false;
            }    
        }

    }
}