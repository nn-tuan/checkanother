using SatisAICtrl.Models;
using SatisAIManage.MVVMTool;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace SatisAIManage.ViewModels
{
    public class TargetInfoAreaViewModel : BindBase
    {
        private readonly SatisAICtrl.SatisAICtrl? _ControlManager;

        #region バインド変数(GUIパラメータ)
        private string _PCBName = "";

        /// <summary>
        /// PCB名
        /// </summary>
        public string PCBName
        {
            get
            {
                return _PCBName;
            }
            set
            {
                if (_PCBName == value) return;
                _PCBName = value;
                RaisePropertyChanged();
            }
        }

        private ObservableCollection<PartItem> _partsList = new ObservableCollection<PartItem>();

        public ObservableCollection<PartItem> PartsList
        {
            get
            {
                return _partsList;
            }
            set
            {
                _partsList = value;
                RaisePropertyChanged();
            }
        }

        #endregion バインド変数(GUIパラメータ)

        public TargetInfoAreaViewModel()
        {
            ObservableCollection<PartItem> parts = new ObservableCollection<PartItem>();
        }

        public TargetInfoAreaViewModel(SatisAICtrl.SatisAICtrl controlManager)
        {
            _ControlManager = controlManager;
            ObservableCollection<PartItem> parts = new ObservableCollection<PartItem>();

            _ControlManager.SAIPlaceFileChangeEvent += _Ctrl_SAIPlaceFileChangeEvent;
        }

        private void _Ctrl_SAIPlaceFileChangeEvent(PcbBoard? pcb)
        {
            if (!DispatcherHelper.CheckAccess())
            {
                DispatcherHelper.BeginInvoke((Action<PcbBoard>)_Ctrl_SAIPlaceFileChangeEvent, pcb);
            }
            else
            {
                if (pcb == null)
                {
                    PCBName = "";
                    PartsList.Clear();
                }
                else
                {
                    //RecipeFilePath = path;
                    PCBName = pcb.Name!;
                    PartsList = new ObservableCollection<PartItem>(pcb.Parts);
                }
            }
        }
    }
}