﻿using SatisAICtrl;
using SatisAIManage.MVVMTool;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SatisAIManage.ViewModels
{
    public class OperationLogViewModel : BindBase
    {
        private readonly ISatisAICtrl? _ControlManager;


        private string _TextLog = "";

        /// <summary>
        /// Set Text Log
        /// </summary>
        public string TextLog
        {
            get
            {
                return _TextLog;
            }
            set
            {
                if (_TextLog == value) return;
                _TextLog = value;
                RaisePropertyChanged();
            }
        }
        private string GetTime()
        {
            return DateTime.Now.ToString("hh:mm:ss");
        }

        public void AddLog(string text)
        {
            TextLog += GetTime()+" "+text + "\n";
        }
        public OperationLogViewModel()
        {
        }
        
        public OperationLogViewModel(ISatisAICtrl controlManager)
        {
            _ControlManager = controlManager;
            _ControlManager.SAILogChangeEvent += _Ctrl_SAILogChangeEvent;
        }

        private void _Ctrl_SAILogChangeEvent(string message)
        {
            AddLog(message); 
        }
    }
}
