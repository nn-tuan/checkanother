using Microsoft.Win32;
using SatisAICtrl;
using SatisAIManage.MVVMTool;
using System;
using System.Windows;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SatisAIManage.ViewModels
{
    public class ButtonAreaViewModel : BindBase
    {
        private readonly ISatisAICtrl? _ControlManager;

        #region Fields

        // 機種変更 ボタンを押せるかどうかの状態変数
        private bool ModelChangeButtonCanExecute = true;

        // 閉じる ボタンを押せるかどうかの状態変数
        private bool CloseButtonCanExecute = true;

        #endregion Fields

        #region Properties

        #region Command
        public DelegateCommand? ModelChangeCommand { get; }
        public DelegateCommand? TransitionSettingCommand { get; }
        public DelegateCommand? TransitionPixelCommand { get; }
        public DelegateCommand? CloseCommand { get; }
        public DelegateCommand? RegisterPartCommand { get; }
        public DelegateCommand? RegisterPcbCommand { get; }
        public DelegateCommand? RegisterProjectCommand { get; }

        #endregion Command
        #endregion Properties

        #region Event
        /// <summary> 画面遷移時のイベント </summary>
        internal event Action<FigureState>? FigureChangeEvent;
        #endregion Event

        public ButtonAreaViewModel()
        {
        }

        public ButtonAreaViewModel(ISatisAICtrl controlManager)
        {
            _ControlManager = controlManager;
           
            ModelChangeButtonCanExecute = true;
            ModelChangeCommand = new DelegateCommand(() => ModelChangeEventHandler(), () => { return ModelChangeButtonCanExecute; });
            TransitionSettingCommand = new DelegateCommand(() => TransitionSettingEventHandler());
            RegisterPartCommand = new DelegateCommand(() => RegisterPartEventHandler());
            RegisterPcbCommand = new DelegateCommand(() => RegisterPcbEventHandler());

            TransitionPixelCommand = new DelegateCommand(() => TransitionPixelEventHandler());
            CloseCommand = new DelegateCommand(() => CloseEventHandler(), () => { return CloseButtonCanExecute; });
        }

        /// <summary>
        /// 機種変更ボタン押下時のイベント
        /// </summary>
        private void ModelChangeEventHandler()
        {
            // ファイルダイアログの表示
            string? filePath = OpenFileEventHandler("配置情報ファイル(*.saiplace)|*.saiplace");

            if (filePath != null)
            {
                // TargetInfoAreaVMに対して更新させるイベントを呼び出し
                _ControlManager?.LoadSAIPlaceFile(filePath, true);
            }
        }

        /// <summary>
        /// 閉じるボタン押下時のイベント
        /// </summary>
        private void CloseEventHandler()
        {
            // Closeイベント呼び出し
            _ControlManager?.CloseApp();
        }

        /// <summary>
        /// ファイルダイアログの表示
        /// </summary>
        private string? OpenFileEventHandler(string? filter = null)
        {
            // ファイルを開くダイアログを生成します。
            var dialog = new OpenFileDialog();

            // フィルターを設定します。
            if (filter != null)
            {
                // この設定は任意です。
                dialog.Filter = filter;
            }

            // ファイルを開くダイアログを表示します。
            var result = dialog.ShowDialog() ?? false;

            // 開くボタン以外が押下された場合
            if (!result)
            {
                // 終了します。
                return null;
            }

            // ファイルを開くダイアログで選択されたファイルパス名を表示します。
            //MessageBox.Show(dialog.FileName);
            return dialog.FileName;
        }

        /// <summary>
        /// 設定ボタン押下時のイベント(設定画面への遷移)
        /// </summary>
        private void TransitionSettingEventHandler()
        {
            //_CurrentFigureState = state;
            FigureChangeEvent?.Invoke(FigureState.Setting);
        }

        private void RegisterPartEventHandler()
        {
            //_CurrentFigureState = state;
            FigureChangeEvent?.Invoke(FigureState.RegisterPart);
        }
        private void RegisterPcbEventHandler()
        {
            //_CurrentFigureState = state;
            FigureChangeEvent?.Invoke(FigureState.RegisterPcb);
        }

        /// <summary>
        /// 画素分解能調整ボタン押下時のイベント(設定画面への遷移)
        /// </summary>
        private void TransitionPixelEventHandler()
        {
            //_CurrentFigureState = state;
            FigureChangeEvent?.Invoke(FigureState.PixelAdjustment);
        }
    }
}