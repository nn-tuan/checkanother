﻿using SatisAICtrl;
using SatisAIManage.MVVMTool;
using System;
using System.Windows.Forms;

namespace SatisAIManage.ViewModels
{
    public class SelectFolderButtonViewModel : BindBase
    {
        #region Fields
        private readonly ISatisAICtrl? _ControlManager;
        #endregion Fields

        #region Properties
        #region Command
        public DelegateCommand? SelectFolderCommand { get; }
        #endregion Command
        #endregion Properties
        #region Event
        /// <summary> 画面遷移時のイベント</summary>
        internal event Action<FigureState>? FigureChangeEvent;
        #endregion Event

        private string _ResultPath;
        public string ResultPath
        {
            get { return _ResultPath; }
            set
            {
                _ResultPath = value;
            }
        }

        public SelectFolderButtonViewModel()
        {
        }
        public SelectFolderButtonViewModel(ISatisAICtrl controlManager)
        {
            _ControlManager = controlManager;
            SelectFolderCommand = new DelegateCommand(() => SelectFolderHandler());
            
        }

        
        #region Private Methods

        /// <summary>
        /// フォルダ選択ボタン押下時のイベント
        /// </summary>
        private void SelectFolderHandler()
        {
            // ファイルダイアログの表示
            string? filePath = OpenFolderEventHandler();
            if (filePath != null)
            {
                // TargetInfoAreaVMに対して更新させるイベントを呼び出し
                ResultPath = filePath;
                //_ControlManager?.LoadSAIPlaceFile(filePath, true);
            }
            FigureChangeEvent?.Invoke(FigureState.Setting);
        }

        /// <summary>
        /// フォルダ選択ダイアログの表示
        /// </summary>
        private string? OpenFolderEventHandler()
        {
            string? folderPath = null;
            // ToDo: フォルダ選択処理の実装をお願いします。
            FolderBrowserDialog chosefolder = new FolderBrowserDialog();
            chosefolder.ShowDialog();
            folderPath = chosefolder.SelectedPath; 
            return folderPath;
        }

        #endregion Private Methods
    }
}