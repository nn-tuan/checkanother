using SatisAIManage.MVVMTool;
using System;
using System.Drawing;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Windows.Forms;
using System.Windows.Input;
using SatisAICtrl.Models;
using System.Windows.Interop;
using System.Numerics;
using System.Windows;

namespace SatisAIManage.ViewModels
{

    public class Rect
    {
        public int X1 { get; set; }
        public int Y1 { get; set; }
        public int X2 { get; set; }
        public int Y2 { get; set; }

        public Rect(int x1, int y1, int x2, int y2)
        {
            X1 = x1;
            Y1 = y1;
            X2 = x2;
            Y2 = y2;
        }
    }

    public class InspectionImageAreaViewModel : BindBase
    {

        [System.Runtime.InteropServices.DllImport("gdi32.dll")]
        public static extern bool DeleteObject(IntPtr hObject);


        #region Feilds
        private readonly SatisAICtrl.ISatisAICtrl? _ControlManager;
        #endregion Feilds

        #region Properties

        private SolidColorBrush ConfirmBrush = new SolidColorBrush(System.Windows.Media.Color.FromRgb(0x03, 0xca, 0xfc));
        private SolidColorBrush UnconfirmBrush = new SolidColorBrush(System.Windows.Media.Color.FromRgb(0xbe, 0xc4, 0x06));
        private SolidColorBrush CornerBrush = new SolidColorBrush(System.Windows.Media.Color.FromRgb(0x00, 0xff, 0x00));
        private SolidColorBrush TransparentBrush = new SolidColorBrush(System.Windows.Media.Color.FromArgb(0x00, 0x00, 0xff, 0x00));
        private SolidColorBrush FillBrush = new SolidColorBrush(System.Windows.Media.Color.FromArgb(0x10,0xff, 0xff, 0xff));
        private double opacity_movein = 0.5;
        private double opacity_moveout = 0.0;
        private int IsMoveIn = -1;
        public ItemOverlay? ItemOverlay { get; set; }
        public int ImageWidth { get; set; }
        public int ImageHeight { get; set; }

        public ObservableCollection<Rect> Boxes { get; set; }

        private BitmapSource? _inspectionImage = null;

        public BitmapSource? InspectionImage
        {
            get
            {
                return _inspectionImage;
            }
            set
            {
                _inspectionImage = value;
                RaisePropertyChanged();
            }
        }

        private double m_height;
        public double Height
        {
            get { return m_height; }
            set
            {
                m_height = value;
            }
        }
        private double m_width;
        public double Width
        {
            get { return m_width; }
            set
            {
                m_width = value;
            }
        }

        private Uri? _inspectionMovie = null;

        public Uri? InspectionMovie
        {
            get
            {
                return _inspectionMovie;
            }
            set
            {
                _inspectionMovie = value;
                RaisePropertyChanged();
            }
        }

        private string? _inspectionResult = null;

        public string? InspectionResult
        {
            get
            {
                return _inspectionResult;
            }
            set
            {
                _inspectionResult = value;
                RaisePropertyChanged();
            }
        }



        #region Command

        /* ===================================================================== */
        private ICommand? _mouseUp;
        public ICommand MouseUp
        {
            get
            {
                return _mouseUp ?? (_mouseUp = new RelayCommand<System.Windows.Controls.Canvas>(
                          x => {
                              DoStuffWhenMouseUp(x);
                          }));
            }
        }

        private void DoStuffWhenMouseUp(System.Windows.Controls.Canvas obj)
        {
            //System.Windows.Point currentPoint = Mouse.GetPosition(obj);
            //System.Windows.MessageBox.Show(currentPoint.X.ToString());
        }

        private ICommand? _mouseClick;
        public ICommand MouseClick
        {
            get
            {
                return _mouseClick ?? (_mouseClick = new RelayCommand<System.Windows.Controls.Canvas>(
                          x => {
                              DoStuffWhenMouseClicked(x);
                          }));
            }
        }
        private void DoStuffWhenMouseClicked(System.Windows.Controls.Canvas obj)
        {
            System.Windows.Point currentPoint = Mouse.GetPosition(obj);
            int boxIdx = -1;
            for (int i = 0; i < ItemOverlay!.RectItems.Count -1; i++)
            { 
                RectItem rect = ItemOverlay!.RectItems[i];
                if (rect.X <= currentPoint.X && rect.Y <= currentPoint.Y && (rect.X + rect.Width) >= currentPoint.X && (rect.Y + rect.Height) >= currentPoint.Y)
                {
                    boxIdx = ItemOverlay!.RectItems.IndexOf(rect);
                }
            }
            if (boxIdx != -1)
              _ControlManager!.TogglePcbPart(boxIdx);
            else
            {
                ////update pcb corner
                //int canvasW = (int)Width;
                //int canvasH = (int)Height;
                

                //float scaleX = ImageWidth / (float)canvasW;
                //float scaleY = ImageHeight / (float)canvasH;
                //float scale = Math.Max(scaleX, scaleY);

                //float renderW = ImageWidth / scale;
                //float renderH = ImageHeight / scale;
                //float diffX = (canvasW - renderW) / 2;
                //float diffY = (canvasH - renderH) / 2;

                //int x = (int)((currentPoint.X - diffX) * scale);
                //int y = (int)((currentPoint.Y - diffY) * scale);
                //_ControlManager!.AddCorner(x, y);
            }
        }



        /* ===================================================================== */
        private ICommand? _mouseMove;
        public ICommand MouseMove
        {
            get
            {
                return _mouseMove ?? (_mouseMove = new RelayCommand<System.Windows.Controls.Canvas>(
                          x => {
                              DoStuffWhenMouseMove(x);
                          }));
            }
        }
        private void DoStuffWhenMouseMove(System.Windows.Controls.Canvas obj)
        {
            System.Windows.Point currentPoint = Mouse.GetPosition(obj);
            int boxIdx = -1;
            for (int i = 0; i < ItemOverlay!.RectItems.Count - 1; i++)
            {
                RectItem rect = ItemOverlay!.RectItems[i];
                if (rect.X <= currentPoint.X && rect.Y <= currentPoint.Y && (rect.X + rect.Width) >= currentPoint.X && (rect.Y + rect.Height) >= currentPoint.Y)
                {
                    boxIdx = ItemOverlay!.RectItems.IndexOf(rect);
                }
            }
            if (boxIdx != -1)
            {
                if(IsMoveIn == -1)
                {
                    _ControlManager!.TogglePcbPartBackground(boxIdx);
                    IsMoveIn = boxIdx;
                }
            }    
            else
            {
                if (IsMoveIn != -1)
                {
                    _ControlManager!.TogglePcbPartBackground(IsMoveIn);
                    IsMoveIn = -1;
                }
            }
        }
        public DelegateCommand? SelectFolderCommand { get; }
        public DelegateCommand? MediaPlayCommand { get; }

        #endregion Command
        #endregion Properties

        #region Event
        /// <summary> 動画再生させるイベント </summary>
        public event Action? MediaPlayEvent;
        #endregion Event

        public InspectionImageAreaViewModel()
        {
        }

        public InspectionImageAreaViewModel(SatisAICtrl.ISatisAICtrl controlManager)
        {
            _ControlManager = controlManager;
            ItemOverlay = new ItemOverlay();

            Boxes = new ObservableCollection<Rect>();
            Boxes!.Add(new Rect(10, 10, 100, 50));

            controlManager.ViewImageOpenEvent += _Ctrl_ViewImageOpenEvent;
            controlManager.PcbUpdateEvent += _Ctrl_PcbUpdateEvent;
            controlManager.ViewMovieOpenEvent += _Ctrl_ViewMovieOpenEvent;
            controlManager.UpdateInspectionResultEvent += _Ctrl_UpdateInspectionResultEvent;
            MediaPlayCommand = new DelegateCommand(() => MediaPlayEventHandler(), () => true);
            //controlManager.NGPartItemsEvent += _NGPartItemEvent;

            //MouseEnterCommand = new DelegateCommand(obj => MouseEnterHandler(obj));
        }

        #region Private Methods

        private void MediaPlayEventHandler()
        {
            MediaPlayEvent?.Invoke();
        }

        private void _Ctrl_ViewImageOpenEvent(Uri imageUri)
        {
            if (!DispatcherHelper.CheckAccess())
            {
                DispatcherHelper.BeginInvoke((Action<Uri>)_Ctrl_ViewImageOpenEvent, imageUri);
            }
            else
            {
                //RecipeFilePath = path;
                InspectionImage = new BitmapImage(imageUri);
            }
        }

        private void _NGPartItemEvent(PartItem part)
        {
            System.Windows.Forms.MessageBox.Show("test");
        }

        private void _Ctrl_PcbUpdateEvent(PcbBoard board)
        {

            ItemOverlay!.Clear();
           
            int canvasW = (int)Width;
            int canvasH = (int)Height;
            ImageWidth = board.Width;
            ImageHeight = board.Height;

            float scaleX = ImageWidth / (float)canvasW;
            float scaleY = ImageHeight / (float)canvasH;
            float scale = Math.Max(scaleX, scaleY);

            float renderW = ImageWidth / scale;
            float renderH = ImageHeight / scale;
            float diffX = (canvasW - renderW) / 2;
            float diffY = (canvasH - renderH) / 2;

            // draw full canvas with transparent for mouse catching
            BoxTxtArrowItem ItemBg = new BoxTxtArrowItem()
            {
                X =0,
                Y = 0,
                TextItem = new TextItem
                {
                    Text = "",
                    Foreground = UnconfirmBrush,
                    FontSize = 20,
                    Padding = new System.Windows.Thickness(10),

                },
                RectItem = new RectItem()
                {
                    Width = 9999999,
                    Height = 9999999,
                    StrokeThickness = 3,
                    Fill = new SolidColorBrush(System.Windows.Media.Color.FromArgb(0x01, 0xff, 0x00, 0x00)),
                },
                ArrowItem = new ArrowItem()
                {
                    Stroke = new SolidColorBrush(System.Windows.Media.Color.FromArgb(0x00, 0xff, 0x00, 0x00)),
                    StrokeThickness = 3,
                    ArrowLength = 40,
                    Degree = 0
                },
            };

            /* ============================ Component arrangement ============================= */
            int No = 0;
            for (int index = 0; index < board.Parts.Count; index++)
            {
                No++;
                for (int next_index = index + 1; next_index < board.Parts.Count; next_index++)
                {
                    int x_index = (int)(diffX + board.Parts[index].Box!.Left / scale);
                    int y_index = (int)(diffY + board.Parts[index].Box!.Top / scale);

                    int x_next = (int)(diffX + board.Parts[next_index].Box!.Left / scale);
                    int y_next = (int)(diffY + board.Parts[next_index].Box!.Top / scale);

                    if (y_index <= y_next)
                    {
                        if (y_index == y_next)
                        {
                            if(x_index >= x_next)
                            {
                                PartItem buff_item = board.Parts[index];
                                board.Parts[index] = board.Parts[next_index];
                                board.Parts[next_index] = buff_item;
                            }    
                        }
                        else
                        {
                            PartItem buff_item = board.Parts[index];
                            board.Parts[index] = board.Parts[next_index];
                            board.Parts[next_index] = buff_item;
                        }
                    }
                }
                board.Parts[index].No = No;
            }
            /* ================================================================================ */

            foreach (PartItem part in board.Parts)
            {
                BoundingBox box = part.Box!;
                box.Rotation = part.Rotation;
                SolidColorBrush color = UnconfirmBrush;
                if (part.Confirm) 
                    color = ConfirmBrush;
                double opacity = opacity_moveout;
                if (part.IsMoveIn)
                    opacity = opacity_movein;
                int x = (int)(diffX + box.Left / scale);
                int y = (int)(diffY + box.Top / scale);
                int w = (int)(box.Width / scale);
                int h = (int)(box.Height / scale);
                BoxTxtArrowItem Item = new BoxTxtArrowItem()
                {
                    X = x,
                    Y = y,
                    TextItem = new TextItem
                    {
                        Text = part.No.ToString(),
                        Background = color,
                        Foreground = Brushes.Black,
                        FontSize = 14,
                        Padding = new System.Windows.Thickness(2),

                    },
                    RectItem = new RectItem()
                    {
                        Width = w,
                        Height = h,
                        Stroke = color,
                        StrokeThickness = 3,
                        Fill = FillBrush,
                        Background = Brushes.White,
                        Opacity = opacity
                    },
                    ArrowItem = new ArrowItem()
                    {
                        Stroke = color,
                        StrokeThickness = 3,
                        ArrowLength = 60,
                        Degree = box.Rotation
                    },
                };
                ItemOverlay.AddCreateItem(Item);
            }

            foreach (System.Drawing.Point pt in board.corners)
            {
                SolidColorBrush color = UnconfirmBrush;
                color = CornerBrush;
                int x = (int)(diffX + (pt.X - 25) / scale);
                int y = (int)(diffY + (pt.Y - 25) / scale);
                int w = (int)(50 / scale);
                int h = (int)(50 / scale);
                BoxTxtArrowItem Item = new BoxTxtArrowItem()
                {
                    X = x,
                    Y = y,
                    TextItem = new TextItem
                    {
                        Text = "pcb_anchor",
                        Foreground = TransparentBrush,
                        FontSize = 20,
                        Padding = new System.Windows.Thickness(10),

                    },
                    RectItem = new RectItem()
                    {
                        Width = w,
                        Height = h,
                        Stroke = color,
                        StrokeThickness = 3,
                        Fill = FillBrush,
                    },
                    ArrowItem = new ArrowItem()
                    {
                        Stroke = TransparentBrush,
                        StrokeThickness = 3,
                        ArrowLength = 40,
                        Degree = 0
                    },
                };
                ItemOverlay.AddCreateItem(Item);
            }

            ItemOverlay.AddCreateItem(ItemBg);
        }

        public void _Ctrl_ViewImageUpdateEvent(BitmapSource bitmap)
        {
            if (!DispatcherHelper.CheckAccess())
            {
                DispatcherHelper.BeginInvoke((Action<BitmapSource>)_Ctrl_ViewImageUpdateEvent, bitmap);
            }
            else
            {
                InspectionImage = bitmap;

            }
        }

        private void _Ctrl_ViewMovieOpenEvent(Uri imageObj)
        {
            if (!DispatcherHelper.CheckAccess())
            {
                DispatcherHelper.BeginInvoke((Action<Uri>)_Ctrl_ViewMovieOpenEvent, imageObj);
            }
            else
            {
                InspectionMovie = imageObj;
                MediaPlayEvent?.Invoke();
            }
        }

        private void _Ctrl_UpdateInspectionResultEvent(string result)
        {
            InspectionResult = result;
        }

        #endregion Private Methods
    }
}