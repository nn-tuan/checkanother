﻿using SatisAICtrl;
using SatisAIManage.MVVMTool;
using System;

namespace SatisAIManage.ViewModels
{
    public class PixelAdjustmentViewModel : BindBase
    {
        #region Fields
        private readonly ISatisAICtrl? _ControlManager;
        #endregion Fields

        #region Properties
        public SubButtonAreaViewModel? SubButtonAreaVM { get; set; }
        #endregion Properties

        #region Event
        /// <summary> 画面遷移時のイベント</summary>
        internal event Action<FigureState>? FigureChangeEvent;
        #endregion Event

        public PixelAdjustmentViewModel()
        {
        }

        public PixelAdjustmentViewModel(ISatisAICtrl controlManager)
        {
            _ControlManager = controlManager;
            SubButtonAreaVM = new SubButtonAreaViewModel(_ControlManager);
            SubButtonAreaVM.FigureChangeEvent += (state) =>
            {
                FigureChangeInvoke(state);
            };
        }

        #region Private Methods

        /// <summary>
        /// 画面遷移処理
        /// </summary>
        /// <param name="state">遷移先</param>
        private void FigureChangeInvoke(FigureState state)
        {
            FigureChangeEvent?.Invoke(state);
            //RaisePropertyChanged(nameof(MenuVisibility));
            //RaisePropertyChanged(nameof(ViewLow));
        }

        #endregion Private Methods
    }
}