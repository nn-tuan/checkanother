﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Markup;
using System.Globalization;
using System.IO;

namespace SatisAIManage
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        /// <summary>
        /// Get parent folder given upper level
        /// </summary>
        /// <param name="dir"></param>
        /// <param name="level">upper level</param>
        /// <returns></returns>
        public string GetParent(string dir, int level)
        {
            string currentDir = dir;
            for (int i = 0; i < level; i++)
            {
                DirectoryInfo? info = Directory.GetParent(currentDir);
                if (info == null) return currentDir; //lowest parent
                else currentDir = info.FullName;
            }
            return currentDir;
        }

        public bool IsSameFile(String path1, String path2)
        {
            return File.ReadAllBytes(path1).SequenceEqual(File.ReadAllBytes(path2));
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            // Replace old Dll
            string currDirectory = Directory.GetCurrentDirectory();
            string rootDir = GetParent(currDirectory, 5);
            string dllPath = rootDir + "\\x64\\Release\\SatisAICore.dll";
            string dllPath2 = GetParent(currDirectory, 4) + "\\x64\\Release\\SatisAICore.dll";
            if (File.Exists(dllPath2))
            {
                dllPath = dllPath2;
            }
            string currentDllPath = currDirectory + "\\SatisAICore.dll";
            bool isCopy = false;
            if (File.Exists(dllPath))
            {
                if (File.Exists(currentDllPath))
                {
                    // Check if two file are different
                    if (!IsSameFile(currentDllPath, dllPath))
                    {
                        File.Delete(currentDllPath);
                        isCopy = true;
                    }
                }
                else
                {
                    isCopy = true;
                }
                if (isCopy)
                    File.Copy(dllPath, currentDllPath);
            }
            base.OnStartup(e);
        }
    }
}
