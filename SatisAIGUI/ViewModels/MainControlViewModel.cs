using Microsoft.Win32;
using SatisAICtrl.Models;
using SatisAICtrl;
using SatisAIGUI.MVVMTool;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Media;



namespace SatisAIGUI.ViewModels
{
   

    public class MainControlViewModel : BindBase
    {
        #region Fields
        private readonly SatisAICtrl.SatisAICtrl controlManager;
        #endregion Fields

        #region Properties
        public ItemOverlay? ItemOverlay { get; set; }
        public ButtonAreaViewModel? ButtonAreaVM { get; set; }
        public ButtonControlViewModel? ButtonControlVM { get; set; }
        public InspectionImageAreaViewModel? InspectionImageAreaVM { get; set; }
        public TargetInfoAreaViewModel? TargetInfoAreaVM { get; set; }
        public OperationLogViewModel? OperationLogVM { get; set; }

        public string SourceVM { get; set; }

        private ObservableCollection<PartItem> _NGPartsList = new ObservableCollection<PartItem>();

        public ObservableCollection<PartItem> NGPartsList
        {
            get
            {
                return _NGPartsList;
            }
            set
            {
                _NGPartsList = value;
                RaisePropertyChanged();
            }
        }


        #region Binding
        private string? _inspectionResult = "Wait";

        public string? InspectionResult
        {
            get
            {
                return _inspectionResult;
            }
            set
            {
                _inspectionResult = value;
                RaisePropertyChanged();
            }
        }


        #endregion Binding
        #endregion Properties

        #region Event
        /// <summary> 結果取得時に見た目変化させるイベント </summary>
        public event Action<string>? UpdateResultDesignEvent;
        /// <summary> 閉じるボタン押下時のイベント </summary>
        public event Action? CloseAppEvent;
        /// <summary> 画面遷移時のイベント</summary>
        internal event Action<FigureState>? FigureChangeEvent;
        #endregion Event

        /// <summary>
        /// デザイナの参照用（使用しない）
        /// </summary>
        public MainControlViewModel()
        {
        }
        public MainControlViewModel(SatisAICtrl.SatisAICtrl managerObj)
        {
            /* ================================================== */
            SourceVM = "C:\\Users\\tuang8ee\\Pictures\\test.avi";
            ItemOverlay = new ItemOverlay();

            BoxTxtArrowItem Item = new BoxTxtArrowItem()
            {
                X = 300,
                Y = 100,
                TextItem = new TextItem
                {
                    Text = "BOX 1",
                    Foreground = Brushes.Blue,
                    FontSize = 100,
                    Padding = new System.Windows.Thickness(10),
                    
                },
                RectItem = new RectItem()
                {
                    Width = 100,
                    Height = 100,
                    Stroke = Brushes.Blue,
                    StrokeThickness = 4,
                },
                ArrowItem = new ArrowItem()
                {
                    Stroke = Brushes.Blue,
                    StrokeThickness = 4,
                    ArrowLength = 100,
                    Degree = -135
                },
            };

            ItemOverlay.AddCreateItem(Item);
            Item.X = 100;
            Item.Y = 100;
            Item.TextItem.Text = "test";
            ItemOverlay.AddCreateItem(Item);
            /* ================================================== */

            controlManager = managerObj;
            ButtonAreaVM = new ButtonAreaViewModel(managerObj);
            InspectionImageAreaVM = new InspectionImageAreaViewModel(managerObj);
            TargetInfoAreaVM = new TargetInfoAreaViewModel(managerObj);
            OperationLogVM = new OperationLogViewModel(managerObj);

            controlManager.UpdateInspectionResultEvent += _Ctrl_UpdateInspectionResultEvent;
            controlManager.CloseAppEvent += _Ctrl_CloseAppEvent;
            controlManager.NGPartItemsEvent += _Ctrl_NGPartItemsEvent;
            controlManager.ImageDataUpdateEvent += _Ctrl_BitmapDataUpdateEvent;


            ButtonAreaVM.FigureChangeEvent += (state) =>
            {
                FigureChangeInvoke(state);
            };
        }

        public void _Ctrl_BitmapDataUpdateEvent(byte[] data, int w, int h, int c)
        {
            InspectionImageAreaVM?._Ctrl_ViewImageUpdateEvent(Utils.BitmapUtil.GetBitmap(data,w,h,c));
        }

        private void _Ctrl_UpdateInspectionResultEvent(string result)
        {
            if (!DispatcherHelper.CheckAccess())
            {
                DispatcherHelper.BeginInvoke((Action<string>)_Ctrl_UpdateInspectionResultEvent, result);
            }
            else
            {
                InspectionResult = result;
                UpdateResultDesignEvent?.Invoke(result);
            }
        }

        private void _Ctrl_NGPartItemsEvent(List<PartItem> items)
        {
            NGPartsList = new ObservableCollection<PartItem>(items);
        }

        private void _Ctrl_CloseAppEvent()
        {
            CloseAppEvent?.Invoke();
        }

        /// <summary>
        /// 画面遷移処理
        /// </summary>
        /// <param name="state">遷移先</param>
        private void FigureChangeInvoke(FigureState state)
        {
            FigureChangeEvent?.Invoke(state);
            //RaisePropertyChanged(nameof(MenuVisibility));
            //RaisePropertyChanged(nameof(ViewLow));
        }


    }
}