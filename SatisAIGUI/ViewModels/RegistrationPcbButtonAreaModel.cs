﻿using Microsoft.Win32;
using SatisAICtrl;
using SatisAIGUI.MVVMTool;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SatisAIGUI.ViewModels
{
    public class RegistrationPcbButtonAreaModel : BindBase
    {
        private readonly ISatisAICtrl? _ControlManager;

        #region Fields

        // 機種変更 ボタンを押せるかどうかの状態変数
        private bool ModelChangeButtonCanExecute = true;

        // 閉じる ボタンを押せるかどうかの状態変数
        private bool CloseButtonCanExecute = true;

        #endregion Fields

        #region Properties

        #region Command
        public DelegateCommand? StartRegistrationPcbCommand { get; }
        public DelegateCommand? StopRegistrationPcbCommand { get; }
        public DelegateCommand? RegistrationPcbCommand { get; }
        public DelegateCommand? CloseRegistrationPcbCommand { get; }
        #endregion Command
        #endregion Properties

        #region Event
        /// <summary> 画面遷移時のイベント </summary>
        internal event Action<FigureState>? FigureChangeEvent;
        #endregion Event

        public RegistrationPcbButtonAreaModel()
        {
        }

        public RegistrationPcbButtonAreaModel(ISatisAICtrl controlManager)
        {
            _ControlManager = controlManager;
            ModelChangeButtonCanExecute = true;
            StartRegistrationPcbCommand   = new DelegateCommand(() => StartRegistrationPcbEventHandler(), () => { return ModelChangeButtonCanExecute; });
            StopRegistrationPcbCommand    = new DelegateCommand(() => StopRegistrationPcbEventHandler());
            RegistrationPcbCommand        = new DelegateCommand(() => RegistrationPcbEventHandler());
            CloseRegistrationPcbCommand   = new DelegateCommand(() => CloseEventHandler(), () => { return CloseButtonCanExecute; });
        }


        private void StartRegistrationPcbEventHandler()
        {

        }

        private void StopRegistrationPcbEventHandler()
        {

        }
        private void RegistrationPcbEventHandler()
        {

        }

        /// <summary>
        /// 閉じるボタン押下時のイベント
        /// </summary>
        private void CloseEventHandler()
        {
            // Closeイベント呼び出し
            FigureChangeEvent?.Invoke(FigureState.MainControl);
        }

        
    }
}