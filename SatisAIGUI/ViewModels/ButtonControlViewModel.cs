using Microsoft.Win32;
using SatisAICtrl;
using SatisAIGUI.MVVMTool;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SatisAIGUI.ViewModels
{
    public class ButtonControlViewModel : BindBase
    {
        private readonly ISatisAICtrl? _ControlManager;

        #region Fields

        // 機種変更 ボタンを押せるかどうかの状態変数
        private bool ModelChangeButtonCanExecute = true;

        // 閉じる ボタンを押せるかどうかの状態変数
        private bool CloseButtonCanExecute = true;

        #endregion Fields

        #region Properties

        #region Command
        public DelegateCommand? StartStopCam { get; }
        public DelegateCommand? StartStopInspect { get; }
        #endregion Command
        #endregion Properties

  
        public ButtonControlViewModel()
        {
        }

        public ButtonControlViewModel(ISatisAICtrl controlManager)
        {
            _ControlManager = controlManager;
            ModelChangeButtonCanExecute = true;
            StartStopCam = new DelegateCommand(() => StartStopCamEventHandler());
            StartStopInspect = new DelegateCommand(() => StartStopInspectEventHandler());
            
        }

   
  
        private void StartStopCamEventHandler()
        {
            _ControlManager?.StartStopCam(true);

        }

        private void StartStopInspectEventHandler()
        {
            _ControlManager?.StartStopInspect(true);
        }
    }
}