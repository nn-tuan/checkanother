using SatisAICtrl;
using SatisAIGUI.MVVMTool;
using System;

namespace SatisAIGUI.ViewModels
{
    /// <summary>
    /// 画面切り替え先のID
    /// </summary>
    internal enum FigureState
    {
        MainControl = 0,
        Setting,
        RegisterPart,
        RegisterPcb,
        PixelAdjustment,
        ReSetting,
    }

    public class MainFrameViewModel : BindBase
    {
        #region Fields
        private readonly ISatisAICtrl? _controlManager;
        private FigureState _CurrentFigureState = FigureState.MainControl;
        #endregion Fields

        #region Properties
        public MainControlViewModel? MainControlVM { get; }
        public SettingViewModel? SettingVM { get; }
        public RegistrationPartViewModel? PartRegistrationVM { get; }
        public RegistrationPcbViewModel? PcbRegistrationVM { get; }

        public PixelAdjustmentViewModel? PixelAdjustmentVM { get; }
        #endregion Properties

        #region Command
        public DelegateCommand? TopCommand { get; }

        public DelegateCommand? CancelCommand { get; }
        #endregion Command

        #region Event
        /// <summary> 画面切り替え時のイベント </summary>
        internal event Action<FigureState>? FigureChangeEvent;
        /// <summary> 閉じるボタン押下時のイベント </summary>
        public event Action? CloseAppEvent;
        #endregion Event

        /// <summary>
        /// Xaml Designer用（使用しない）
        /// </summary>
        public MainFrameViewModel()
        {
        }

        /// <summary>
        /// Main Constructor
        /// </summary>
        /// <param name="managerObj"></param>
        public MainFrameViewModel(SatisAICtrl.SatisAICtrl managerObj)
        {
            _controlManager = managerObj;

            // ここから参照するVMのインスタンス生成
            SettingVM = new SettingViewModel(managerObj);
            MainControlVM = new MainControlViewModel(managerObj);
            PartRegistrationVM = new RegistrationPartViewModel(managerObj);
            PcbRegistrationVM = new RegistrationPcbViewModel(managerObj);
            PixelAdjustmentVM = new PixelAdjustmentViewModel(managerObj);

            // コマンド
            TopCommand = new DelegateCommand(() => TopHandler());
            CancelCommand = new DelegateCommand(() => CancelHandler());

            MainControlVM.FigureChangeEvent += (state) =>
            {
                FigureChangeInvoke(state);
                //MotorControlBaseVM.SetMotorModeState(state);
            };
            SettingVM.FigureChangeEvent += (state) =>
            {
                FigureChangeInvoke(state);
            };

            PartRegistrationVM.FigureChangeEvent += (state) =>
            {
                FigureChangeInvoke(state);
            };
            PcbRegistrationVM.FigureChangeEvent += (state) =>
            {
                FigureChangeInvoke(state);
            };
            PixelAdjustmentVM.FigureChangeEvent += (state) =>
            {
                FigureChangeInvoke(state);
            };

            // イベント
            _controlManager.CloseAppEvent += _Ctrl_CloseAppEvent;
            
            // Update when UI loaded
            SettingVM.ReadCameraConfig();
        }


        private void TopHandler()
        {
            FigureChangeInvoke(FigureState.MainControl);
        }

        private void CancelHandler()
        {
            FigureChangeInvoke(FigureState.MainControl);
        }

        private void FigureChangeInvoke(FigureState state)
        {
            _CurrentFigureState = state;
            FigureChangeEvent?.Invoke(state);
            //RaisePropertyChanged(nameof(MenuVisibility));
            //RaisePropertyChanged(nameof(ViewLow));
        }

        /// <summary>
        /// MainControlの閉じるボタン押下時の処理
        /// </summary>
        private void _Ctrl_CloseAppEvent()
        {
            CloseAppEvent?.Invoke();
        }
    }
}