using SatisAIGUI.MVVMTool;
using System;
using System.Drawing;
using System.Windows;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Media.Imaging;


namespace SatisAIGUI.ViewModels
{

    public class Rect
    {
        public int X1 { get; set; }
        public int Y1 { get; set; }
        public int X2 { get; set; }
        public int Y2 { get; set; }

        public Rect(int x1, int y1, int x2, int y2)
        {
            X1 = x1;
            Y1 = y1;
            X2 = x2;
            Y2 = y2;
        }
    }

    public class InspectionImageAreaViewModel : BindBase
    {

        [System.Runtime.InteropServices.DllImport("gdi32.dll")]
        public static extern bool DeleteObject(IntPtr hObject);


        #region Feilds
        private readonly SatisAICtrl.ISatisAICtrl? _ControlManager;
        #endregion Feilds

        #region Properties
        public ObservableCollection<Rect> Boxes { get; set; }

        private BitmapSource? _inspectionImage = null;

        public BitmapSource? InspectionImage
        {
            get
            {
                return _inspectionImage;
            }
            set
            {
                _inspectionImage = value;
                RaisePropertyChanged();
            }
        }

        private Uri? _inspectionMovie = null;

        public Uri? InspectionMovie
        {
            get
            {
                return _inspectionMovie;
            }
            set
            {
                _inspectionMovie = value;
                RaisePropertyChanged();
            }
        }

        private string? _inspectionResult = null;

        public string? InspectionResult
        {
            get
            {
                return _inspectionResult;
            }
            set
            {
                _inspectionResult = value;
                RaisePropertyChanged();
            }
        }

        #region Command
        public DelegateCommand? SelectFolderCommand { get; }
        public DelegateCommand? MediaPlayCommand { get; }

        #endregion Command
        #endregion Properties

        #region Event
        /// <summary> 動画再生させるイベント </summary>
        public event Action? MediaPlayEvent;
        #endregion Event

        public InspectionImageAreaViewModel()
        {
        }

        public InspectionImageAreaViewModel(SatisAICtrl.ISatisAICtrl controlManager)
        {
            _ControlManager = controlManager;

            Boxes = new ObservableCollection<Rect>();
            Boxes!.Add(new Rect(10, 10, 100, 50));

            controlManager.ViewImageOpenEvent += _Ctrl_ViewImageOpenEvent;
            //controlManager.ViewImageUpdateEvent += _Ctrl_ViewImageUpdateEvent;
            controlManager.ViewMovieOpenEvent += _Ctrl_ViewMovieOpenEvent;
            controlManager.UpdateInspectionResultEvent += _Ctrl_UpdateInspectionResultEvent;
            MediaPlayCommand = new DelegateCommand(() => MediaPlayEventHandler(), () => true);

            //MouseEnterCommand = new DelegateCommand(obj => MouseEnterHandler(obj));
        }

        #region Private Methods

        private void MediaPlayEventHandler()
        {
            MediaPlayEvent?.Invoke();
        }

        private void _Ctrl_ViewImageOpenEvent(Uri imageUri)
        {
            if (!DispatcherHelper.CheckAccess())
            {
                DispatcherHelper.BeginInvoke((Action<Uri>)_Ctrl_ViewImageOpenEvent, imageUri);
            }
            else
            {
                //RecipeFilePath = path;
                InspectionImage = new BitmapImage(imageUri);
            }
        }

        public void _Ctrl_ViewImageUpdateEvent(BitmapSource bitmap)
        {
            if (!DispatcherHelper.CheckAccess())
            {
                DispatcherHelper.BeginInvoke((Action<BitmapSource>)_Ctrl_ViewImageUpdateEvent, bitmap);
            }
            else
            {
                InspectionImage = bitmap;

            }
        }

        private void _Ctrl_ViewMovieOpenEvent(Uri imageObj)
        {
            if (!DispatcherHelper.CheckAccess())
            {
                DispatcherHelper.BeginInvoke((Action<Uri>)_Ctrl_ViewMovieOpenEvent, imageObj);
            }
            else
            {
                InspectionMovie = imageObj;
                MediaPlayEvent?.Invoke();
            }
        }

        private void _Ctrl_UpdateInspectionResultEvent(string result)
        {
            InspectionResult = result;
        }

        #endregion Private Methods
    }
}