﻿using SatisAICtrl;
using SatisAIGUI.MVVMTool;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SatisAIGUI.ViewModels
{
    public class SubButtonAreaViewModel : BindBase
    {
        #region Fields
        private readonly ISatisAICtrl? _ControlManager;
        #endregion

        #region Properties
        public SubButtonAreaViewModel? SubButtonAreaVM { get; set; }

        #region Command
        public DelegateCommand? ConfirmCommand { get; }
        public DelegateCommand? CancelCommand { get; }
        #endregion
        #endregion

        #region Event
        /// <summary> 画面遷移時のイベント </summary>
        internal event Action<FigureState>? FigureChangeEvent;
        #endregion
        public bool ConfirmEvent = false; 
        public SubButtonAreaViewModel()
        {
        }

        public SubButtonAreaViewModel(ISatisAICtrl controlManager)
        {
            _ControlManager = controlManager;
            //SubButtonAreaVM = new SubButtonAreaViewModel(_ControlManager);
            ConfirmCommand = new DelegateCommand(() => ConfirmCommandEventHandler());
            CancelCommand = new DelegateCommand(() => CancelCommandEventHandler());
        }


        /// <summary>
        /// OKボタン押下時のイベント
        /// </summary>
        private void ConfirmCommandEventHandler()
        {
            // ToDo：設定を変更する
            ConfirmEvent = true;
            // 画面を遷移する
            FigureChangeEvent?.Invoke(FigureState.MainControl);
        }

        /// <summary>
        /// Cancelボタン押下時のイベント
        /// </summary>
        private void CancelCommandEventHandler()
        {
            // 画面を遷移する
            ConfirmEvent = false;
            FigureChangeEvent?.Invoke(FigureState.MainControl);
        }
    }
}
