using SatisAICtrl;
using SatisAIGUI.MVVMTool;
using System;
using System.IO;
using System.Data;
using System.Windows.Forms;

namespace SatisAIGUI.ViewModels
{
    public class RegistrationPartViewModel : BindBase
    {
        #region Fields
        private readonly ISatisAICtrl? _ControlManager;
        System.Windows.Threading.DispatcherTimer timer;
        #endregion Fields

        #region Properties
        public RegistrationPartButtonAreaModel? RegistrationPartButtonAreaVM { get; set; }
        public DelegateCommand? OutputPartBtnCommand { get; set; }
        public InspectionImageAreaViewModel? InspectionImageAreaVM { get; set; }

        public string OutputPartTxt { get; set; } = "";
        public string PartID { get; set; } = "";
        public string PartName { get; set; } = "";
        public int TIMEOUT = 60; //sec
        public OperationLogViewModel? OperationLogVM { get; set; }


        public string SourceVM { get; set; } = "";
        #endregion Properties

        #region Event
        /// <summary> 画面遷移時のイベント</summary>
        internal event Action<FigureState>? FigureChangeEvent;
        #endregion Event

        public RegistrationPartViewModel()
        {

        }

        public RegistrationPartViewModel(ISatisAICtrl controlManager)
        {
            _ControlManager = controlManager;
            OperationLogVM = new OperationLogViewModel(controlManager);

            RegistrationPartButtonAreaVM = new RegistrationPartButtonAreaModel(_ControlManager);
            InspectionImageAreaVM = new InspectionImageAreaViewModel(controlManager);

            OutputPartBtnCommand = new DelegateCommand(() => OutputPartRegistrationPart());


            controlManager.ImageDataUpdateEvent += _Ctrl_BitmapDataUpdateEvent;
            controlManager.StartPartRegisterEvent += _StartPartRegisterEvent;
            controlManager.StopPartRegisterEvent += _StopPartRegisterEvent;

            RegistrationPartButtonAreaVM.FigureChangeEvent += (state) =>
            {
                FigureChangeInvoke(state);
            };

        }

        #region Private Methods
        private string GetTime()
        {
            return DateTime.Now.ToString("hhmmss-ddMMyyyy");
        }
        private void _StartPartRegisterEvent()
        {

            if (OutputPartTxt.Length == 0)
            {
                OperationLogVM?.AddLog("Missing Output Path");
                return;
            }
            if (PartID.Length == 0)
            {
                OperationLogVM?.AddLog("Missing PartId");
                return;
            }
            if (PartName.Length == 0)
            {
                OperationLogVM?.AddLog("Missing PartName");
                return;
            }
            if (!Directory.Exists(OutputPartTxt)) Directory.CreateDirectory(OutputPartTxt);
            string videoPath = OutputPartTxt + "//" + PartName + "__" + PartID + "__" + GetTime() + ".mp4";
            bool val = _ControlManager!.StartRecord(videoPath);
            OperationLogVM?.AddLog("Record Started");
            if (!val)
                OperationLogVM?.AddLog("Record Started Failed");
            OperationLogVM?.AddLog("Path:"+ videoPath);

            timer = new System.Windows.Threading.DispatcherTimer();
            timer.Tick += new EventHandler(timer_Tick);
            timer.Interval = new TimeSpan(0, 0, TIMEOUT);
            timer.Start();
        
    }

    private void timer_Tick(object sender, EventArgs e)
    {
        timer.Stop();
        _StopPartRegisterEvent();
    }

    private void _StopPartRegisterEvent()
        {
            timer.Stop();
            _ControlManager?.StopRecord();
            OperationLogVM?.AddLog("Record Stopped");
        }
        /// <summary>
        /// 画面遷移処理
        /// </summary>
        /// <param name="state">遷移先</param>
        private void FigureChangeInvoke(FigureState state)
        {
            FigureChangeEvent?.Invoke(state);
            //RaisePropertyChanged(nameof(MenuVisibility));
            //RaisePropertyChanged(nameof(ViewLow));
        }
        private string OpenFolderEventHandler()
        {
            FolderBrowserDialog chosefolder = new FolderBrowserDialog();
            chosefolder.ShowDialog();
            return chosefolder.SelectedPath; 
        }


        private void _Ctrl_BitmapDataUpdateEvent(byte[] data, int w, int h, int c)
        {
            InspectionImageAreaVM?._Ctrl_ViewImageUpdateEvent(Utils.BitmapUtil.GetBitmap(data, w, h, c));
        }
        private void OutputPartRegistrationPart()
        {
            /* =============== Chose save Folder =============== */
            OutputPartTxt = OpenFolderEventHandler();
            // TO DO: Handle save folder output

            /* ======= ReShow RegistrationPartView Windown ======== */
            FigureChangeEvent?.Invoke(FigureState.RegisterPart);
        }
        #endregion Private Methods
    }
}
