using SatisAICtrl;
using SatisAIGUI.MVVMTool;
using System;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using System.Threading;
using System.Globalization;

namespace SatisAIGUI.ViewModels
{
    public class SettingViewModel : BindBase
    {
        #region Fields
        private readonly ISatisAICtrl? _ControlManager;
        #endregion Fields

        #region Properties
        public SubButtonAreaViewModel? SubButtonAreaVM { get; set; }
        public string ResultOutputDirPath { get; set; }

        public string APPSETTING = "AppSetting",
                     SETTING = "Setting",
                     RESULTPATH = "ResultPath",
                     LANGCODE = "Language",
                     APPMODE = "AppMode",
                     GPU_ID = "GpuID",
                     IMAGEOUTPUTTYPE = "ImageOutputType",
                     NONE = "None",
                     NG = "NG Only",
                     ALL = "ALL";

        public string XmlPath = "";


        public SelectFolderButtonViewModel? SelectFolderButtonVM { get; set; }
        #endregion Properties

        #region Event
        /// <summary> 画面遷移時のイベント</summary>
        internal event Action<FigureState>? FigureChangeEvent;
        #endregion Event

        public bool NGonlyType { get; set; }
        public bool AllType { get; set; }
        public bool NoneType { get; set; }

        public SettingViewModel()
        {
            XmlPath = Directory.GetCurrentDirectory() + "\\Resources\\" + APPSETTING + ".xml";
        }

        public SettingViewModel(ISatisAICtrl controlManager)
        {
            XmlPath = Directory.GetCurrentDirectory() + "\\Resources\\" + APPSETTING + ".xml";
            string appPath = Directory.GetCurrentDirectory();

            _ControlManager = controlManager;
            SubButtonAreaVM = new SubButtonAreaViewModel(_ControlManager);
            SelectFolderButtonVM = new SelectFolderButtonViewModel(_ControlManager);

            NGonlyType = false;
            AllType = true;
            NoneType = false;

            /*
             * XMLファイルを読み取ります。
             * XMLファイルを作成します。
             */

            ResultOutputDirPath = GetXMLNode(RESULTPATH)!.InnerText;
            _ControlManager.GpuId = int.Parse(GetXMLNode(GPU_ID)!.InnerText);
            _ControlManager.ResultOutputDirPath = ResultOutputDirPath;



            string imgOutputType = GetXMLNode(IMAGEOUTPUTTYPE)!.InnerText;
            XmlNode? langNode = GetXMLNode(LANGCODE);

            string langCode = langNode?.InnerText ?? "en";
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(langCode);

            SetRadio(imgOutputType);

            SelectFolderButtonVM.FigureChangeEvent += (state) =>
            {
                if (SelectFolderButtonVM.ResultPath != "")
                {
                    ResultOutputDirPath = SelectFolderButtonVM.ResultPath;
                    _ControlManager.ResultOutputDirPath = ResultOutputDirPath;
                }
                FigureChangeInvoke(FigureState.ReSetting);
            };

            SubButtonAreaVM.FigureChangeEvent += (state) =>
            {
                if (SubButtonAreaVM.ConfirmEvent == true)
                {
                    SaveXML(RESULTPATH, ResultOutputDirPath);
                    if (NGonlyType == true)
                    {
                        SaveXML(IMAGEOUTPUTTYPE, NG);
                    }
                    else if (AllType == true)
                    {
                        SaveXML(IMAGEOUTPUTTYPE, ALL);
                    }
                    else if (NoneType == true)
                    {
                        SaveXML(IMAGEOUTPUTTYPE, NONE);
                    }
                }
                FigureChangeInvoke(state);
            };
        }


        public void SaveXML(string element, string data)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(XmlPath);

            XmlNodeList? nodeList = xmlDoc.SelectNodes(APPSETTING + "/" + SETTING);
            if (nodeList != null)
            {
                foreach (XmlNode xn in nodeList)
                {
                    var xmlElement = xn[element];
                    if (xmlElement != null)
                        xmlElement.InnerXml = data;
                    else
                    {
                        //TODO: handle missing element
                    }
                }
                xmlDoc.Save(XmlPath);
            }
            else
            {
                //TODO: handle missing element
            }
        }

        public XmlNode? GetXMLNode(string element_get)
        {
            XmlDocument xmlDoc = new XmlDocument();
            string appPath = Directory.GetCurrentDirectory();

            try
            {
                xmlDoc.Load(XmlPath);
            }
            catch
            {
                XmlDeclaration xmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", "UTF-8", null);
                XmlElement? root = xmlDoc.DocumentElement;
                xmlDoc.InsertBefore(xmlDeclaration, root);

                XmlElement AppSettingElement = xmlDoc.CreateElement("", APPSETTING, string.Empty);
                XmlElement SettingElement = xmlDoc.CreateElement(string.Empty, SETTING, string.Empty);
                XmlElement ResultPathElement = xmlDoc.CreateElement(string.Empty, RESULTPATH, string.Empty);
                XmlElement GpuIdElement = xmlDoc.CreateElement(string.Empty, GPU_ID, string.Empty);
                XmlElement AppModeElement = xmlDoc.CreateElement(string.Empty, APPMODE, string.Empty);
                XmlElement ImgTypeElement = xmlDoc.CreateElement(string.Empty, IMAGEOUTPUTTYPE, string.Empty);
                AppSettingElement.SetAttribute("ver", "1.00");
                xmlDoc.AppendChild(AppSettingElement);
                AppSettingElement.AppendChild(SettingElement);
                SettingElement.AppendChild(ResultPathElement);
                SettingElement.AppendChild(GpuIdElement);
                SettingElement.AppendChild(AppModeElement);
                SettingElement.AppendChild(ImgTypeElement);

                ResultPathElement.AppendChild(xmlDoc.CreateTextNode(""));
                ImgTypeElement.AppendChild(xmlDoc.CreateTextNode(""));
                GpuIdElement.AppendChild(xmlDoc.CreateTextNode("-1"));
                AppModeElement.AppendChild(xmlDoc.CreateTextNode("INSPECT"));// INSPECT DEMO_DIRECTION DEMO_INSPECTION
                xmlDoc.Save(XmlPath);
                xmlDoc.Load(XmlPath);
            }

            XmlNodeList? nodeList = xmlDoc.SelectNodes(APPSETTING + "/" + SETTING);
            if (nodeList != null)
                foreach (XmlNode xn in nodeList)
                {
                    return xn[element_get];
                }
            return null;
        }
        public void SetRadio(string imageType)
        {
            if (imageType == ALL)
            {
                NGonlyType = false;
                AllType = true;
                NoneType = false;
                _ControlManager!._LogType = LogType.All;
            }
            else if (imageType == NG)
            {
                NGonlyType = true;
                AllType = false;
                NoneType = false;
                _ControlManager!._LogType = LogType.NG;
            }
            else if (imageType == NONE)
            {
                NGonlyType = false;
                AllType = false;
                NoneType = true;
                _ControlManager!._LogType = LogType.None;
            }
            else
            {
                NGonlyType = false;
                AllType = false;
                NoneType = false;
            }
        }

        public void ReadCameraConfig()
        {
            //string ResultOutputDirPath = GetXMLData(RESULTPATH);
            XmlNode? cameraNode = GetXMLNode("Input");
            if (cameraNode != null)
            {
                string cameraType = "";
                string cameraPath = "";
                int cameraIndex = 0;
                cameraType = cameraNode.Attributes!["type"]!.Value;
                if (cameraType == "video") cameraPath = cameraNode.InnerText;
                else cameraIndex = int.Parse(cameraNode.Attributes!["index"]!.Value);

                Thread th = new Thread(() =>
                {
                    _ControlManager?.SetCamera(cameraType, cameraIndex, cameraPath);
                });
                th.IsBackground = true;
                th.Start();
            }
        }


        #region Private Methods

        /// <summary>
        /// 画面遷移処理
        /// </summary>
        /// <param name="state">遷移先</param>
        private void FigureChangeInvoke(FigureState state)
        {
            FigureChangeEvent?.Invoke(state);
            //RaisePropertyChanged(nameof(MenuVisibility));
            //RaisePropertyChanged(nameof(ViewLow));
        }

        #endregion Private Methods
    }
}