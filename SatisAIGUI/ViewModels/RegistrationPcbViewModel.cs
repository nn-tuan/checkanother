﻿using SatisAICtrl;
using SatisAIGUI.MVVMTool;
using System;
using System.Data;
using System.Windows.Forms;

namespace SatisAIGUI.ViewModels
{
    public class RegistrationPcbViewModel : BindBase
    {
        #region Fields
        private readonly ISatisAICtrl? _ControlManager;
        #endregion Fields

        #region Properties
        public RegistrationPcbButtonAreaModel? RegistrationPcbButtonAreaVM { get; set; }
        public DelegateCommand? OutputPartBtnCommand { get; set; }
        public InspectionImageAreaViewModel? InspectionImageAreaVM { get; set; }

        public string? OutputPartTxt { get; set; }
        public string? NewProjectName { get; set; }
        public string? NewBoardName { get; set; }
        public string? ModelNumber { get; set; }
        public int? RotationTol { get; set; } //Rotation

        public int? DistanceTol { get; set; } //Percentage

        public string SourceVM { get; set; } = "";
        #endregion Properties

        #region Event
        /// <summary> 画面遷移時のイベント</summary>
        internal event Action<FigureState>? FigureChangeEvent;
        #endregion Event

        public RegistrationPcbViewModel()
        {

        }

        public RegistrationPcbViewModel(SatisAICtrl.SatisAICtrl controlManager)
        {
            _ControlManager = controlManager;
            RegistrationPcbButtonAreaVM = new RegistrationPcbButtonAreaModel(_ControlManager);
            InspectionImageAreaVM = new InspectionImageAreaViewModel(controlManager);

            OutputPartBtnCommand = new DelegateCommand(() => OutputPartRegistrationPcb());

            /* ======== Mockup Data =========*/
            OutputPartTxt = "Output folder URL";
            NewProjectName = "NewProject";
            NewBoardName = "NewBoard";
            ModelNumber = "MODEL1";
            RotationTol = 30;
            DistanceTol = 30;
            controlManager.ImageDataUpdateEvent += _Ctrl_BitmapDataUpdateEvent;

            RegistrationPcbButtonAreaVM.FigureChangeEvent += (state) =>
             {
                FigureChangeInvoke(state);
            };
        }

        #region Private Methods

        /// <summary>
        /// 画面遷移処理
        /// </summary>
        /// <param name="state">遷移先</param>
        private void FigureChangeInvoke(FigureState state)
        {
            FigureChangeEvent?.Invoke(state);
            //RaisePropertyChanged(nameof(MenuVisibility));
            //RaisePropertyChanged(nameof(ViewLow));
        }
        private string? OpenFolderEventHandler()
        {
            string? folderPath = null;
            FolderBrowserDialog chosefolder = new FolderBrowserDialog();
            chosefolder.ShowDialog();
            folderPath = chosefolder.SelectedPath;
            return folderPath;
        }


        private void _Ctrl_BitmapDataUpdateEvent(byte[] data, int w, int h, int c)
        {
            InspectionImageAreaVM?._Ctrl_ViewImageUpdateEvent(Utils.BitmapUtil.GetBitmap(data, w, h, c));
        }
        private void OutputPartRegistrationPcb()
        {
            /* =============== Chose save Folder =============== */
            OutputPartTxt = OpenFolderEventHandler();
            // TO DO: Handle save folder output

            /* ======= ReShow RegistrationPcbView Windown ======== */
            FigureChangeEvent?.Invoke(FigureState.RegisterPcb);
        }
        #endregion Private Methods
    }
}
