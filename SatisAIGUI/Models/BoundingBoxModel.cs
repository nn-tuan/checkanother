﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaiplaceCreator.Models
{
    public class BoundingBoxModel
    {
        #region Properties
        int Top { get; set; } = 0;
        int Left { get; set; } = 0;
        int Width { get; set; } = 0;
        int Height { get; set; } = 0;
        string? Label { get; set; }
        int Angle { get; set; } = -1;
        int ArrowLength { get; set; } = 0;

        string Color { get; set; } = "#FF0000";
        #endregion

        public BoundingBoxModel(int top, int left, int width, int height)
        {
            Top = top;
            Left = left;
            Width = width;
            Height = height;
            ArrowLength = (int) (Math.Max(width, height) * 0.75);
        }

        public Point GetCenter()
        {
            return new Point(Left + Width / 2, Top + Height / 2);
        }
        public Point GetEndOfArrow()
        {
            double angleRadian = -Angle * 3.1415f / 180.0f;
            int dy = (int)(ArrowLength * Math.Sin(angleRadian));
            int dx = (int)(ArrowLength * Math.Cos(angleRadian));
            return new Point(Left + dx, Top + dy);
        }
    }
}
