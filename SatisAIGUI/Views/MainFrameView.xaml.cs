﻿using SatisAIGUI.ViewModels;
using System.Windows;
using System.Windows.Controls;

namespace SatisAIGUI.Views
{
    /// <summary>
    /// MainFrameView.xaml の相互作用ロジック
    /// </summary>
    public partial class MainFrameView : UserControl
    {
        private readonly UserControl? _ChildControl;

        public MainFrameView()
        {
            InitializeComponent();
            DataContextChanged += MainFrameView_DataContextChanged;
        }

        /// <summary>
        /// 原則1回しか呼び出しされないはず。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainFrameView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue is MainFrameViewModel mfvm)
            {
                // 初期値
                var mainCtrl = new MainControlView
                {
                    DataContext = mfvm.MainControlVM
                };
                ChildContent.Content = mainCtrl;
                mfvm.FigureChangeEvent += Mfvm_FigureChangeEvent;
            }
        }

        /// <summary>
        /// 画面変更
        /// </summary>
        /// <param name="obj"></param>
        private void Mfvm_FigureChangeEvent(FigureState obj)
        {
            if (DataContext is MainFrameViewModel mfvm)
            {
                // 画面変更時のVの生成 & VMとの結合処理を実行
                switch (obj)
                {
                    case FigureState.MainControl:
                        var mainCtrl = new MainControlView
                        {
                            DataContext = mfvm.MainControlVM
                        };
                        ChildContent.Content = mainCtrl;
                        break;

                    case FigureState.Setting:
                        mfvm!.SettingVM!.ResultOutputDirPath = mfvm!.SettingVM!.GetXMLNode(mfvm.SettingVM.RESULTPATH)!.InnerText;
                        string imgType = mfvm!.SettingVM!.GetXMLNode(mfvm.SettingVM.IMAGEOUTPUTTYPE)!.InnerText;
                        mfvm.SettingVM.SetRadio(imgType);
                        var setting = new SettingView
                        {
                            DataContext = mfvm.SettingVM
                        };
                        ChildContent.Content = setting;
                        break;

                    case FigureState.PixelAdjustment:
                        var pixelAdjustment = new PixelAdjustmentView
                        {
                            DataContext = mfvm.PixelAdjustmentVM
                        };
                        ChildContent.Content = pixelAdjustment;
                        break;
                    case FigureState.ReSetting:
                        var remain = new MainControlView
                        {
                            DataContext = mfvm.MainControlVM
                        };
                        ChildContent.Content = remain;

                        var resetting = new SettingView
                        {
                            DataContext = mfvm.SettingVM
                        };
                        ChildContent.Content = resetting;
                        break;

                    default:  
                        break;
                }
            }
        }
    }
}