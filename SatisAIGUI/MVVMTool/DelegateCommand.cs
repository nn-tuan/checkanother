using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Input;

namespace SatisAIGUI.MVVMTool
{
    /// <summary>
    /// Delegate Command
    /// </summary>
    public class DelegateCommand : ICommand
    {
        private static readonly Action EmptyExecute = () => { };
        private static readonly Func<bool> EmptyCanExecute = () => true;

        private Action execute;
        private Func<bool> canExecute;

        public DelegateCommand()
        {
            this.execute = EmptyExecute;
            this.canExecute = EmptyCanExecute;
        }

        public DelegateCommand(Action execute, Func<bool>? canExecute = null)
        {
            this.execute = execute ?? EmptyExecute;
            this.canExecute = canExecute ?? EmptyCanExecute;
        }

        public void Execute()
        {
            this.execute();
        }

        public bool CanExecute()
        {
            return this.canExecute();
        }

        public event EventHandler? CanExecuteChanged;

        public void RaiseCanExecuteChanged()
        {
            this.CanExecuteChanged?.Invoke(this, EventArgs.Empty);
        }

        bool ICommand.CanExecute(object? parameter)
        {
            return this.CanExecute();
        }

        void ICommand.Execute(object? parameter)
        {
            this.Execute();
        }
    }

    public class DelegateCommand<T> : ICommand
    {
        private static readonly Action<T> EmptyExecute = (t) => { };
        private static readonly Func<bool> EmptyCanExecute = () => true;

        private Action<T> execute;
        private Func<bool> canExecute;

        public DelegateCommand(Action<T> execute, Func<bool>? canExecute = null)
        {
            this.execute = execute ?? EmptyExecute;
            this.canExecute = canExecute ?? EmptyCanExecute;
        }

        public void Execute(T parameter)
        {
            this.execute(parameter);
        }

        public bool CanExecute()
        {
            return this.canExecute();
        }

        bool ICommand.CanExecute(object? parameter)
        {
            return this.CanExecute();
        }

        public event EventHandler? CanExecuteChanged;

        public void RaiseCanExecuteChanged()
        {
            this.CanExecuteChanged?.Invoke(this, EventArgs.Empty);
        }

        void ICommand.Execute(object? parameter)
        {
            if (parameter is T)
            {
                var arg = (T)parameter;
                this.Execute(arg);
            }
            else
                throw new ArgumentException("型が異なります。");
        }
    }

    public interface IAsyncCommand : ICommand
    {
        Task ExecuteAsync(object parameter);
    }

    public abstract class AsyncDelegateCommandBase : IAsyncCommand
    {
        public abstract bool CanExecute(object? parameter);

        public abstract Task ExecuteAsync(object? parameter);

        public async void Execute(object? parameter)
        {
            await ExecuteAsync(parameter);
        }

        public event EventHandler? CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        protected void RaiseCanExecuteChanged()
        {
            CommandManager.InvalidateRequerySuggested();
        }
    }

#if false
    public class AsyncCommand : AsyncDelegateCommandBase
    {
        private readonly Func<Task> _command;
        public AsyncCommand(Func<Task> command)
        {
            _command = command;
        }
        public
    }
#endif

    public static class CommandExetensions
    {
        public static void RaiseCanExecuteChanged(this ICommand self)
        {
            var delegateCommand = self as DelegateCommand;
            if (delegateCommand == null)
            {
                return;
            }

            delegateCommand.RaiseCanExecuteChanged();
        }

        public static void RaiseCanExecuteChanged(this IEnumerable<ICommand> self)
        {
            foreach (var command in self)
            {
                command.RaiseCanExecuteChanged();
            }
        }

        public static void RaiseCanExecuteChanged(params ICommand[] commands)
        {
            commands.RaiseCanExecuteChanged();
        }
    }
}