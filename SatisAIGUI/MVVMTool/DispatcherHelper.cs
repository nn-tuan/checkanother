using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace SatisAIGUI.MVVMTool
{
    /// <summary>
    /// 同期支援ツール
    /// </summary>
    public static class DispatcherHelper
    {
        private static Dispatcher? _dispatcher;

        public static void SetDispatcher(Dispatcher dispatcher)
        {
            _dispatcher = dispatcher;
        }

        public static bool CheckAccess()
        {
            #pragma warning disable CS8602 // null 参照の可能性があるものの逆参照です。
            return _dispatcher.CheckAccess();
            #pragma warning restore CS8602 // null 参照の可能性があるものの逆参照です。
        }

        public static object? Invoke(Delegate method)
        {
            return _dispatcher?.Invoke(method);
        }

        public static object? Invoke(Delegate method, params object[] args)
        {
            return _dispatcher?.Invoke(method, args);
        }

        public static void BeginInvoke(Delegate method)
        {
            _dispatcher?.BeginInvoke(method);
        }

        public static void BeginInvoke(Delegate method, params object[] args)
        {
            _dispatcher?.BeginInvoke(method, args);
        }

    }
}
